package com.example.crownapplication.bl.db.dao;

import com.airbnb.lottie.L;
import com.example.crownapplication.bl.db.model.Bb;
import com.example.crownapplication.bl.db.model.Lk;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class LkDao extends BaseDaoCrud<Lk, Integer> {
    private static LkDao lkDao;

    public static LkDao getLkDao(){
        if (lkDao == null){
            lkDao = new LkDao();
        }
        return lkDao;
    }

    public Lk getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }

    public List<Lk> getByJk(String jk) throws SQLException{
        QueryBuilder<Lk, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Lk.JK, jk);
        return getDao().query(qb.prepare());
    }

    public List<Lk> getByUsiaJk (int usia, String jk) throws SQLException{
        QueryBuilder<Lk, Integer> qb =  getDao().queryBuilder();
        qb.where().eq(Bb.USIA, usia)
                .and().eq(Lk.JK, jk);
        return getDao().query(qb.prepare());
    }
}
