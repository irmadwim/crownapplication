package com.example.crownapplication.bl.network.api;

import android.content.Context;
import android.util.Log;

import com.example.crownapplication.bl.db.dao.AnakDao;
import com.example.crownapplication.bl.db.dao.AspekDao;
import com.example.crownapplication.bl.db.dao.BbDao;
import com.example.crownapplication.bl.db.dao.DataGiziDao;
import com.example.crownapplication.bl.db.dao.HasilGiziDao;
import com.example.crownapplication.bl.db.dao.ImtDao;
import com.example.crownapplication.bl.db.dao.LkDao;
import com.example.crownapplication.bl.db.dao.PertanyaanDdstDao;
import com.example.crownapplication.bl.db.dao.RekomendasiGiziDao;
import com.example.crownapplication.bl.db.dao.ReportAkhirDao;
import com.example.crownapplication.bl.db.dao.TbDao;
import com.example.crownapplication.bl.db.dao.UserDao;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.Aspek;
import com.example.crownapplication.bl.db.model.Bb;
import com.example.crownapplication.bl.db.model.DataGizi;
import com.example.crownapplication.bl.db.model.HasilGizi;
import com.example.crownapplication.bl.db.model.Imt;
import com.example.crownapplication.bl.db.model.Lk;
import com.example.crownapplication.bl.db.model.PertanyaanDdst;
import com.example.crownapplication.bl.db.model.RekomendasiGizi;
import com.example.crownapplication.bl.db.model.ReportAkhir;
import com.example.crownapplication.bl.db.model.Tb;
import com.example.crownapplication.bl.db.model.User;
import com.example.crownapplication.bl.network.model.Anaks;
import com.example.crownapplication.bl.network.model.Aspeks;
import com.example.crownapplication.bl.network.model.Bbs;
import com.example.crownapplication.bl.network.model.DataGizis;
import com.example.crownapplication.bl.network.model.HasilGizis;
import com.example.crownapplication.bl.network.model.Imts;
import com.example.crownapplication.bl.network.model.Lks;
import com.example.crownapplication.bl.network.model.PertanyaanDdsts;
import com.example.crownapplication.bl.network.model.RekomendasiGizis;
import com.example.crownapplication.bl.network.model.ReportAkhirs;
import com.example.crownapplication.bl.network.model.StandardBbs;
import com.example.crownapplication.bl.network.model.StandardTbs;
import com.example.crownapplication.bl.network.model.Tbs;
import com.example.crownapplication.bl.network.model.Users;
import com.example.crownapplication.ui.dialog.DialogBuilder;

import org.threeten.bp.LocalDate;
import org.threeten.bp.Period;

import java.sql.SQLException;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SyncWorker {
    private static SyncWorker syncWorker;

    public static SyncWorker getSyncWorker() {
        if (syncWorker == null) {
            syncWorker = new SyncWorker();
        }
        return syncWorker;
    }

    public void syncAnak(final Context context, Call<Anaks> anaksCall, final boolean isFirstRun){
        anaksCall.enqueue(new Callback<Anaks>() {
            @Override
            public void onResponse(Call<Anaks> call, Response<Anaks> response) {
                if (response.isSuccessful()){
                    Anaks anaks = response.body();
                    Log.i("ANAK_GET", response.message());
                    for (Anaks.Anak list: anaks.getAnak()) {
                        Anak anak = new Anak();
                        LocalDate birth = null;
                        birth = LocalDate.of(list.getTglLahirAnak().getYear() + 1900, list.getTglLahirAnak().getMonth() +1 , list.getTglLahirAnak().getDate());
                        int year =  Period.between(birth, LocalDate.now()).getYears();
                        int month = Period.between(birth, LocalDate.now()).getMonths();
                        int totalBulan = year * 12 + month;

                        anak.setId_anak(list.getIdAnak());
                        anak.setNama_anak(list.getNamaAnak());
                        anak.setTgl_lahir_anak(list.getTglLahirAnak());
                        anak.setGender_anak(list.getGenderAnak());
                        anak.setId_user(list.getIdUser());
                        anak.setUsia(totalBulan);

                        try {
                            if (isFirstRun){
                                AnakDao.getAnakDao().add(anak);
                            }else {
                                AnakDao.getAnakDao().save(anak);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Anaks> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("ANAK_GET", t.getMessage());
            }
        });
    }

    public void syncUser(final Context context, Call<Users> usersCall, final boolean isFirstRun){
        usersCall.enqueue(new Callback<Users>() {
            @Override
            public void onResponse(Call<Users> call, Response<Users> response) {
                if (response.isSuccessful()){
                    Users users = response.body();
                    Log.i("USER_GET", response.message());
                    for (Users.User list : users.getUser()) {
                        User user = new User();
                        user.setId_user(list.getIdUser());
                        user.setNama(list.getNama());
                        user.setEmail(list.getEmail());
                        user.setPassword(list.getPassword());

                        try {
                            if (isFirstRun){
                                UserDao.getUserDao().add(user);
                            }else {
                                UserDao.getUserDao().save(user);
                            }
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Users> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("USER_GET", t.getMessage());
            }
        });
    }


    public void syncDataGizi(final Context context, Call<DataGizis> dataGizisCall, final boolean isFirstRun){
        dataGizisCall.enqueue(new Callback<DataGizis>() {
            @Override
            public void onResponse(Call<DataGizis> call, Response<DataGizis> response) {
                if (response.isSuccessful()){
                    Log.i("DATAGIZI_GET", response.message());
                    DataGizis list = response.body();
                    for (DataGizis.DataGizi dataGizi : list.getDatagizi()) {
                        DataGizi obj = new DataGizi();
                        obj.setId_gizi(dataGizi.getIdDataGizi());
                        obj.setId_anak(dataGizi.getIdAnak());
                        obj.setTanggal(dataGizi.getTanggal());
                        obj.setTb(dataGizi.getTbAnak());
                        obj.setBb(dataGizi.getBbAnak());
                        obj.setLk(dataGizi.getLkAnak());
                        obj.setImt(dataGizi.getImtAnak());

                        try {
                            if (isFirstRun){
                                DataGiziDao.getDatagiziDao().add(obj);
                            }else {
                                DataGiziDao.getDatagiziDao().save(obj);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<DataGizis> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("DATAGIZI_GET", t.getMessage());
            }
        });
    }

    public void syncHasilGizi(final Context context, Call<HasilGizis> hasilGizisCall, final boolean isFirstRun){
        hasilGizisCall.enqueue(new Callback<HasilGizis>() {
            @Override
            public void onResponse(Call<HasilGizis> call, Response<HasilGizis> response) {
                if (response.isSuccessful()){
                    Log.i("HASILGIZI_GET", response.message());
                    HasilGizis list = response.body();
                    for (HasilGizis.HasilGizi hasilGizi : list.getHasilgizi()) {
                        HasilGizi obj = new HasilGizi();
                        obj.setId_hasil_gizi(hasilGizi.getIdHasilGizi());
                        obj.setId_gizi(hasilGizi.getIdGizi());
                        obj.setTb_hasil(hasilGizi.getHasilTb());
                        obj.setBb_hasil(hasilGizi.getHasilBb());
                        obj.setLk_hasil(hasilGizi.getHasilLk());
                        obj.setImt_hasil(hasilGizi.getHasilImt());

                        try {
                            if (isFirstRun){
                                HasilGiziDao.getHasilGiziDao().add(obj);
                            }else {
                                HasilGiziDao.getHasilGiziDao().save(obj);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<HasilGizis> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("HASILGIZI_GET", t.getMessage());
            }
        });
    }

    public void syncPertanyaan(final Context context, Call<PertanyaanDdsts> pertanyaanDdstsCall, final boolean isFirstRun){
        pertanyaanDdstsCall.enqueue(new Callback<PertanyaanDdsts>() {
            @Override
            public void onResponse(Call<PertanyaanDdsts> call, Response<PertanyaanDdsts> response) {
                if (response.isSuccessful()){
                    Log.i("PERTANYAAN_GET", response.message());
                    PertanyaanDdsts list = response.body();
                    for (PertanyaanDdsts.PertanyaanDdst pertanyaanDdst : list.getPertanyaanddst()) {
                        PertanyaanDdst obj = new PertanyaanDdst();
                        obj.setId_pertanyaan(pertanyaanDdst.getIdPertanyaan());
                        obj.setId_aspek(pertanyaanDdst.getIdAspek());
                        obj.setUsia(pertanyaanDdst.getUsia());
                        obj.setPertanyaan(pertanyaanDdst.getPertanyaan());

                        try {
                            if (isFirstRun){
                                PertanyaanDdstDao.getPertanyaanddstDao().add(obj);
                            }else {
                                PertanyaanDdstDao.getPertanyaanddstDao().save(obj);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<PertanyaanDdsts> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("PERTANYAAN_GET", t.getMessage());
            }
        });
    }

    public void syncAspek(final Context context, Call<Aspeks> aspeksCall, final boolean isFirstRun){
        aspeksCall.enqueue(new Callback<Aspeks>() {
            @Override
            public void onResponse(Call<Aspeks> call, Response<Aspeks> response) {
                if (response.isSuccessful()){
                    Aspeks list = response.body();
                    Log.i("ASPEK_GET", response.message());
                    for (Aspeks.Aspek aspek : list.getAspek()) {
                        Aspek obj = new Aspek();
                        obj.setId_aspek(aspek.getIdAspek());
                        obj.setNama_aspek(aspek.getNamaAspek());

                        try {
                            if (isFirstRun){
                                AspekDao.getAspekDao().add(obj);
                            }else {
                                AspekDao.getAspekDao().save(obj);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Aspeks> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("ASPEK_GET", t.getMessage());
            }
        });
    }


    public void syncReportAkhir(final Context context, Call<ReportAkhirs> reportAkhirsCall, final boolean isFirstRun){
        reportAkhirsCall.enqueue(new Callback<ReportAkhirs>() {
            @Override
            public void onResponse(Call<ReportAkhirs> call, Response<ReportAkhirs> response) {
                if (response.isSuccessful()){
                    ReportAkhirs list = response.body();
                    Log.i("REPORTAKHIR_GET", response.message());
                    for (ReportAkhirs.ReportAkhir report : list.getReportakhir()){
                        ReportAkhir obj = new ReportAkhir();
                        obj.setId_reportAkhir(report.getIdReportAkhir());
                        obj.setAnak_id(report.getIdAnak());
                        obj.setHasil_akhir(report.getHasilAkhir());
                        //obj.setTanggal(report.getTanggal());

                        try {
                            if (isFirstRun){
                                ReportAkhirDao.getReportakhirDao().add(obj);
                            }else {
                                ReportAkhirDao.getReportakhirDao().save(obj);
                            }

                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }


                }
            }

            @Override
            public void onFailure(Call<ReportAkhirs> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("REPORTAKHIR_GET", t.getMessage());
            }
        });
    }

    public void syncRekomGizi(final Context context, Call<RekomendasiGizis> rekomGizisCall, final boolean isFirstRun){
        rekomGizisCall.enqueue(new Callback<RekomendasiGizis>() {
            @Override
            public void onResponse(Call<RekomendasiGizis> call, Response<RekomendasiGizis> response) {
                if (response.isSuccessful()){
                    RekomendasiGizis list = response.body();
                    Log.i("REKOMENDASIGIZI_GET", response.message());
                    for (RekomendasiGizis.RekomendasiGizi rekomGizi : list.getRekomendasigizi()){
                        RekomendasiGizi obj = new RekomendasiGizi();
                        obj.setId_rekom_gizi(rekomGizi.getIdRekomGizi());
                        obj.setUsia(rekomGizi.getUsia());
                        obj.setDeskripsi(rekomGizi.getDeskripsi());

                        try {
                            if (isFirstRun){
                                RekomendasiGiziDao.getRekomendasigiziDao().add(obj);
                            }else {
                                RekomendasiGiziDao.getRekomendasigiziDao().save(obj);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<RekomendasiGizis> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("REKOMENDASIGIZI_GET", t.getMessage());
            }
        });
    }
}
