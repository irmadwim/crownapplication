package com.example.crownapplication.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = Aspek.TBL_NAME)
public class Aspek {
    public static final String TBL_NAME = "aspek";
    public static final String ID = "id";
    public static final String NAMA_ASPEK = "nama_aspek";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = NAMA_ASPEK) private String nama_aspek;

    public Aspek() {
    }

    public int getId_aspek() {
        return id;
    }

    public void setId_aspek(int id) {
        this.id = id;
    }

    public String getNama_aspek() {
        return nama_aspek;
    }

    public void setNama_aspek(String nama_aspek) {
        this.nama_aspek = nama_aspek;
    }

}
