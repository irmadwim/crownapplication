package com.example.crownapplication.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Lk.TBL_NAME)
public class Lk {
    public static final String TBL_NAME = "lk";
    public static final String ID = "id";
    public static final String USIA = "usia";
    public static final String JK = "jk";
    public static final String M3SD = "m3sd";
    public static final String M2SD = "m2sd";
    public static final String M1SD = "m1sd";
    public static final String MEDIUM = "medium";
    public static final String P1SD = "p1sd";
    public static final String P2SD = "p2sd";
    public static final String P3SD = "p3sd";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = USIA) private int usia;
    @DatabaseField(columnName = JK) private String jk;
    @DatabaseField(columnName = M3SD) private float m3sd;
    @DatabaseField(columnName = M2SD) private float m2sd;
    @DatabaseField(columnName = M1SD) private float m1sd;
    @DatabaseField(columnName = MEDIUM) private float medium;
    @DatabaseField(columnName = P1SD) private float p1sd;
    @DatabaseField(columnName = P2SD) private float p2sd;
    @DatabaseField(columnName = P3SD) private float p3sd;

    public Lk() {
    }

    public int getId_Lk() {
        return id;
    }

    public void setId_lk(int id_lk) {
        this.id = id;
    }

    public int getUsia_anak() {
        return usia;
    }

    public void setUsia_anak(int usia) {
        this.usia = usia;
    }

    public String getJk() {
        return jk;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public float getM3sd() {
        return m3sd;
    }

    public void setM3sd(float m3sd) {
        this.m3sd = m3sd;
    }

    public float getM2sd() {
        return m2sd;
    }

    public void setM2sd(float m2sd) {
        this.m2sd = m2sd;
    }

    public float getM1sd() {
        return m1sd;
    }

    public void setM1sd(float m1sd) {
        this.m1sd = m1sd;
    }

    public float getMedium() {
        return medium;
    }

    public void setMedium(float medium) {
        this.medium = medium;
    }

    public float getP3sd() {
        return p3sd;
    }

    public void setP3sd(float p3sd) {
        this.p3sd = p3sd;
    }

    public float getP2sd() {
        return p2sd;
    }

    public void setP2sd(float p2sd) {
        this.p2sd = p2sd;
    }

    public float getP1sd() {
        return p1sd;
    }

    public void setP1sd(float p1sd) {
        this.p1sd = p1sd;
    }

}

