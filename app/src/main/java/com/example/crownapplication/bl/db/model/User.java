package com.example.crownapplication.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = User.TBL_NAME)
public class User {
    public static final String TBL_NAME = "user";
    public static final String ID = "id";
    public static final String NAMA = "nama";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = NAMA) private String nama;
    @DatabaseField(columnName = EMAIL) private String email;
    @DatabaseField(columnName = PASSWORD) private String password;

    public User() {
    }

    public int getId_user() {
        return id;
    }

    public void setId_user(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
