package com.example.crownapplication.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class Bbs {
    @SerializedName("bb")
    @Expose
    private List<Bb> bb = null;

    public List<Bb> getBb() {
        return bb;
    }

    public void setBb(List<Bb> bb) {
        this.bb = bb;
    }

    public class Bb {

        @SerializedName("id")
        @Expose
        private int idBb;
        @SerializedName("usia")
        @Expose
        private int usia;
        @SerializedName("jk")
        @Expose
        private String jk;
        @SerializedName("m3sd")
        @Expose
        private float m3sd;
        @SerializedName("m2sd")
        @Expose
        private float m2sd;
        @SerializedName("m1sd")
        @Expose
        private float m1sd;
        @SerializedName("medium")
        @Expose
        private float medium;
        @SerializedName("p1sd")
        @Expose
        private float p1sd;
        @SerializedName("p2sd")
        @Expose
        private float p2sd;
        @SerializedName("p3sd")
        @Expose
        private float p3sd;


        public int getIdBb() {
            return idBb;
        }

        public void setIdBb(int idBb) {
            this.idBb = idBb;
        }

        public int getUsia() {
            return usia;
        }

        public void setUsia(int usia) {
            this.usia = usia;
        }

        public String getJk() {
            return jk;
        }

        public void setJk(String jk) {
            this.jk = jk;
        }

        public float getM3sd() {
            return m3sd;
        }

        public void setM3sd(float m3sd) {
            this.m3sd = m3sd;
        }

        public float getM2sd() {
            return m2sd;
        }

        public void setM2sd(float m2sd) {
            this.m2sd = m2sd;
        }

        public float getM1sd() {
            return m1sd;
        }

        public void setM1sd(float m1sd) {
            this.m1sd = m1sd;
        }

        public float getMedium() {
            return medium;
        }

        public void setMedium(float medium) {
            this.medium = medium;
        }

        public float getP1sd() {
            return p1sd;
        }

        public void setP1sd(float p1sd) {
            this.p1sd = p1sd;
        }

        public float getP2sd() {
            return p2sd;
        }

        public void setP2sd(float p2sd) {
            this.p2sd = p2sd;
        }

        public float getP3sd() {
            return p3sd;
        }

        public void setP3sd(float p3sd) {
            this.p3sd = p3sd;
        }

    }
}

