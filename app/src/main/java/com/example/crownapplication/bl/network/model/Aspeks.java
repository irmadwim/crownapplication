package com.example.crownapplication.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Aspeks {
    @SerializedName("aspek")
    @Expose
    private List<Aspek> aspek = null;

    public List<Aspek> getAspek() {
        return aspek;
    }

    public void setAspek(List<Aspek> aspek) {
        this.aspek = aspek;
    }

    public class Aspek {

        @SerializedName("id")
        @Expose
        private int idAspek;
        @SerializedName("nama_aspek")
        @Expose
        private String namaAspek;

        public int getIdAspek() {
            return idAspek;
        }

        public void setIdAspek(int idAspek) {
            this.idAspek = idAspek;
        }

        public String getNamaAspek() {
            return namaAspek;
        }

        public void setNamaAspek(String namaAspek) {
            this.namaAspek = namaAspek;
        }


    }
}

