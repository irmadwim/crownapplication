package com.example.crownapplication.bl.db.dao;

import com.example.crownapplication.bl.db.model.Imt;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class ImtDao extends BaseDaoCrud<Imt, Integer> {
    private static ImtDao imtDao;

    public static ImtDao getImtDao(){
        if (imtDao == null){
            imtDao = new ImtDao();
        }
        return imtDao;
    }

    public Imt getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }

    public List<Imt> getByJk(String jk) throws SQLException{
        QueryBuilder<Imt, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Imt.JK, jk);
        return getDao().query(qb.prepare());
    }

    public List<Imt> getByUsiaJk (int usia, String jk) throws SQLException{
        QueryBuilder<Imt, Integer> qb =  getDao().queryBuilder();
        qb.where().eq(Imt.USIA, usia)
                .and().eq(Imt.JK, jk);
        return getDao().query(qb.prepare());
    }
}
