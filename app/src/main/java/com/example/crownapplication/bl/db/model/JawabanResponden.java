package com.example.crownapplication.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = JawabanResponden.TBL_NAME)
public class JawabanResponden {
    public static final String TBL_NAME = "jawaban_responden";
    public static final String ID = "id";
    public static final String ASPEK_ID = "aspek_id";
    public static final String ANAK_ID = "anak_id";
    public static final String REPORTED = "reported";
    public static final String JAWABAN_P = "jawaban_p";
    public static final String JAWABAN_F = "jawaban_f";
    public static final String JAWABAN_R = "jawaban_r";
    public static final String TANGGAL = "tanggal";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = ASPEK_ID) private int aspek_id;
    @DatabaseField(columnName = ANAK_ID) private int anak_id;
    @DatabaseField(columnName = REPORTED) private int reported;
    @DatabaseField(columnName = JAWABAN_P) private int jawaban_p;
    @DatabaseField(columnName = JAWABAN_F) private int jawaban_f;
    @DatabaseField(columnName = JAWABAN_R) private int jawaban_r;
    @DatabaseField(columnName = TANGGAL) private Date tanggal;

    public JawabanResponden() {
    }

    public int getId_jawaban() {
        return id;
    }

    public void setId_jawaban(int id) {
        this.id = id;
    }

    public int getId_aspek() {
        return aspek_id;
    }

    public void setId_aspek(int aspek_id) {
        this.aspek_id = aspek_id;
    }

    public int getId_anak() {
        return anak_id;
    }

    public void setId_anak(int anak_id) {
        this.anak_id = anak_id;
    }

    public int getReported() {
        return reported;
    }

    public void setReported(int reported) {
        this.reported = reported;
    }

    public int getJawaban_p() {
        return jawaban_p;
    }

    public void setJawaban_p(int jawaban_p) {
        this.jawaban_p = jawaban_p;
    }

    public int getJawaban_f() {
        return jawaban_f;
    }

    public void setJawaban_f(int jawaban_f) {
        this.jawaban_f = jawaban_f;
    }

    public int getJawaban_r() {
        return jawaban_r;
    }

    public void setJawaban_r(int jawaban_r) {
        this.jawaban_r = jawaban_r;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

}
