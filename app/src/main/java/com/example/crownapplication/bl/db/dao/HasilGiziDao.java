package com.example.crownapplication.bl.db.dao;

import com.example.crownapplication.bl.db.model.HasilGizi;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public class HasilGiziDao extends BaseDaoCrud<HasilGizi, Integer> {
    private static HasilGiziDao hasilGiziDao;

    public static HasilGiziDao getHasilGiziDao(){
        if (hasilGiziDao == null){
            hasilGiziDao = new HasilGiziDao();
        }
        return hasilGiziDao;
    }

    public HasilGizi getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }

    public List<HasilGizi> getByDataGizi(int gizi_id) throws SQLException{
        QueryBuilder<HasilGizi, Integer> qb = getDao().queryBuilder();
        qb.where().eq(HasilGizi.GIZI_ID, gizi_id);
        return getDao().query(qb.prepare());
    }

    public HasilGizi getLastIdByGizi(int gizi_id) throws SQLException{
        QueryBuilder<HasilGizi, Integer> qBuilder = getDao().queryBuilder();
        qBuilder.where().eq(HasilGizi.GIZI_ID, gizi_id);
        qBuilder.orderBy("ID", false);
        return getDao().queryForFirst(qBuilder.prepare());
    }

//    public List<HasilGizi> getByTgl(String tgl, int anak_id) throws SQLException{
//        QueryBuilder<HasilGizi, Integer> qb = getDao().queryBuilder();
//        qb.where().eq(HasilGizi.TANGGAL, tgl)
//                .and().eq(HasilGizi.ANAK_ID, anak_id);
//        return getDao().query(qb.prepare());
//    }

}
