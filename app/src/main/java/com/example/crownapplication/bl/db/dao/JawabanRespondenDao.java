package com.example.crownapplication.bl.db.dao;

import com.example.crownapplication.bl.db.model.JawabanResponden;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public class JawabanRespondenDao extends BaseDaoCrud<JawabanResponden, Integer> {
    private static JawabanRespondenDao jawabanRespondenDao;

    public static JawabanRespondenDao getJawabanrespondenDao(){
        if (jawabanRespondenDao == null){
            jawabanRespondenDao = new JawabanRespondenDao();
        }
        return jawabanRespondenDao;
    }

    public JawabanResponden getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }

//    public HasilGizi getByTgl(Date tgl, int anak_id) throws SQLException{
//        QueryBuilder<HasilGizi, Integer> qb = getDao().queryBuilder();
//        qb.where().eq(HasilGizi.TGL, tgl)
//                .and().eq(Report.ID_MURID, id_murid);
//        return getDao().queryForFirst(qb.prepare());
//    }

}

