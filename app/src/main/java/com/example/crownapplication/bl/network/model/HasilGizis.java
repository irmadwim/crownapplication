package com.example.crownapplication.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class HasilGizis {
    @SerializedName("hasilgizi")
    @Expose
    private List<HasilGizi> hasilgizi = null;

    public List<HasilGizi> getHasilgizi() {
        return hasilgizi;
    }

    public void setHasilgizi(List<HasilGizi> hasilgizi) {
        this.hasilgizi = hasilgizi;
    }

    public class HasilGizi {

        @SerializedName("id")
        @Expose
        private int idHasilGizi;
        @SerializedName("gizi_id")
        @Expose
        private int idGizi;

        @SerializedName("hasil_tb")
        @Expose
        private String hasilTb;
        @SerializedName("hasil_bb")
        @Expose
        private String hasilBb;
        @SerializedName("hasil_lk")
        @Expose
        private String hasilLk;
        @SerializedName("hasil_imt")
        @Expose
        private String hasilImt;

        public int getIdHasilGizi() {
            return idHasilGizi;
        }

        public void setIdHasilGizi(int idHasilGizi) {
            this.idHasilGizi = idHasilGizi;
        }

        public int getIdGizi() {
            return idGizi;
        }

        public void setIdGizi(int idGizi) {
            this.idGizi = idGizi;
        }

        public String getHasilTb() {
            return hasilTb;
        }

        public void setHasilTb(String hasilTb) {
            this.hasilTb = hasilTb;
        }

        public String getHasilBb() {
            return hasilBb;
        }

        public void setHasilBb(String hasilBb) {
            this.hasilBb = hasilBb;
        }

        public String getHasilLk() {
            return hasilLk;
        }

        public void setHasilLk(String hasilLk) {
            this.hasilLk = hasilLk;
        }

        public String getHasilImt() {
            return hasilImt;
        }

        public void setHasilImt(String hasilImt) {
            this.hasilImt = hasilImt;
        }


    }
}

