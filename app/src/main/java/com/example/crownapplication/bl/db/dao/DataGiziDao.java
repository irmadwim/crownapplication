package com.example.crownapplication.bl.db.dao;

import com.example.crownapplication.bl.db.model.DataGizi;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class DataGiziDao extends BaseDaoCrud<DataGizi, Integer> {
    private static DataGiziDao dataGiziDao;

    public static DataGiziDao getDatagiziDao(){
        if (dataGiziDao == null){
            dataGiziDao = new DataGiziDao();
        }
        return dataGiziDao;
    }

    public DataGizi getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }

    public List<DataGizi> getByAnak(int id) throws SQLException{
        QueryBuilder<DataGizi, Integer> qb = getDao().queryBuilder();
        qb.where().eq(DataGizi.ANAK_ID, id);
        return getDao().query(qb.prepare());
    }

    public List<DataGizi> getByTgl(String tgl, int anak_id) throws SQLException{
        QueryBuilder<DataGizi, Integer> qb = getDao().queryBuilder();
        qb.where().eq(DataGizi.TANGGAL, tgl)
                .and().eq(DataGizi.ANAK_ID, anak_id);
        return getDao().query(qb.prepare());
    }

    public DataGizi getLastIdByAnak(int anak_id) throws SQLException{
        QueryBuilder<DataGizi, Integer> qBuilder = getDao().queryBuilder();
        qBuilder.where().eq(DataGizi.ANAK_ID, anak_id);
        qBuilder.orderBy("ID", false);
        return getDao().queryForFirst(qBuilder.prepare());
    }
}
