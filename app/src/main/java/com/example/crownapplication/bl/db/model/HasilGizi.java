package com.example.crownapplication.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.sql.Date;
import java.sql.Timestamp;

@DatabaseTable(tableName = HasilGizi.TBL_NAME)
public class HasilGizi {
    public static final String TBL_NAME = "hasil_gizi";
    public static final String ID = "id";
    public static final String GIZI_ID = "gizi_id";
    public static final String HASIL_TB = "hasil_tb";
    public static final String HASIL_BB = "hasil_bb";
    public static final String HASIL_LK = "hasil_lk";
    public static final String HASIL_IMT = "hasil_imt";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = GIZI_ID) private int gizi_id;
    @DatabaseField(columnName = HASIL_TB) private String hasil_tb;
    @DatabaseField(columnName = HASIL_BB) private String hasil_bb;
    @DatabaseField(columnName = HASIL_LK) private String hasil_lk;
    @DatabaseField(columnName = HASIL_IMT) private String hasil_imt;

    public HasilGizi() {
    }

    public int getId_hasil_gizi() {
        return id;
    }

    public void setId_hasil_gizi(int id) {
        this.id = id;
    }

    public String getTb_hasil() {
        return hasil_tb;
    }

    public void setTb_hasil(String hasil_tb) {
        this.hasil_tb = hasil_tb;
    }

    public String getBb_hasil() {
        return hasil_bb;
    }

    public void setBb_hasil(String hasil_bb) {
        this.hasil_bb = hasil_bb;
    }

    public String getLk_hasil() {
        return hasil_lk;
    }

    public void setLk_hasil(String hasil_lk) {
        this.hasil_lk = hasil_lk;
    }

    public String getImt_hasil() {
        return hasil_imt;
    }

    public void setImt_hasil(String hasil_imt) {
        this.hasil_imt = hasil_imt;
    }

    public int getId_gizi() {
        return gizi_id;
    }

    public void setId_gizi(int gizi_id) {
        this.gizi_id = gizi_id;
    }



}
