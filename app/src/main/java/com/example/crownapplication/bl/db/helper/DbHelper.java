package com.example.crownapplication.bl.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.Aspek;
import com.example.crownapplication.bl.db.model.Bb;
import com.example.crownapplication.bl.db.model.DataGizi;
import com.example.crownapplication.bl.db.model.HasilGizi;
import com.example.crownapplication.bl.db.model.Imt;
import com.example.crownapplication.bl.db.model.JawabanResponden;
import com.example.crownapplication.bl.db.model.Lk;
import com.example.crownapplication.bl.db.model.PertanyaanDdst;
import com.example.crownapplication.bl.db.model.RekomendasiGizi;
import com.example.crownapplication.bl.db.model.ReportAkhir;
import com.example.crownapplication.bl.db.model.Tb;
import com.example.crownapplication.bl.db.model.User;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DbHelper extends OrmLiteSqliteOpenHelper {
    private static final int DBVER = 1;
    public static final String DBNAME = "crownapps.db";

    public DbHelper(Context ctx){
        super(ctx, DBNAME, null, DBVER);
    }
    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Anak.class);
            TableUtils.createTable(connectionSource, Aspek.class);
            TableUtils.createTable(connectionSource, DataGizi.class);
            TableUtils.createTable(connectionSource, HasilGizi.class);
            TableUtils.createTable(connectionSource, PertanyaanDdst.class);
            TableUtils.createTable(connectionSource, ReportAkhir.class);
            TableUtils.createTable(connectionSource, JawabanResponden.class);
            TableUtils.createTable(connectionSource, RekomendasiGizi.class);
            TableUtils.createTable(connectionSource, Bb.class);
            TableUtils.createTable(connectionSource, Lk.class);
            TableUtils.createTable(connectionSource, Tb.class);
            TableUtils.createTable(connectionSource, Imt.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {}

    @Override
    public ConnectionSource getConnectionSource() {
        return super.getConnectionSource();
    }

}
