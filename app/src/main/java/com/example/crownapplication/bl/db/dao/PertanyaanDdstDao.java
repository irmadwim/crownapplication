package com.example.crownapplication.bl.db.dao;

import com.example.crownapplication.bl.db.model.PertanyaanDdst;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class PertanyaanDdstDao extends BaseDaoCrud<PertanyaanDdst, Integer> {
    private static PertanyaanDdstDao pertanyaanDdstDao;

    public static PertanyaanDdstDao getPertanyaanddstDao(){
        if (pertanyaanDdstDao == null){
            pertanyaanDdstDao = new PertanyaanDdstDao();
        }
        return pertanyaanDdstDao;
    }

    public PertanyaanDdst getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }

    public List<PertanyaanDdst> getByAspek(int id) throws SQLException{
        QueryBuilder<PertanyaanDdst, Integer> qb = getDao().queryBuilder();
        qb.where().eq(PertanyaanDdst.ASPEK_ID, id);
        return getDao().query(qb.prepare());
    }

    public List<PertanyaanDdst> getByUsia(int aspek_id, int usia) throws SQLException{
        QueryBuilder<PertanyaanDdst, Integer> qb = getDao().queryBuilder();
        qb.where().eq(PertanyaanDdst.ASPEK_ID, aspek_id)
                .and().eq(PertanyaanDdst.USIA, usia);
        return getDao().query(qb.prepare());
    }
}
