package com.example.crownapplication.bl.db.dao;

import androidx.lifecycle.LiveData;

import com.example.crownapplication.bl.db.model.Anak;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

import retrofit2.http.Query;

public class AnakDao extends BaseDaoCrud<Anak, Integer> {
    private static AnakDao anakDao;

    public static AnakDao getAnakDao() {
        if (anakDao == null) {
            anakDao = new AnakDao();
        }
        return anakDao;
    }

    public Anak getByID(int id) throws SQLException {
        return getDao().queryForId(id);
    }

    public List<Anak> getByOrtu(int id) throws SQLException {
        QueryBuilder<Anak, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Anak.USER_ID, id);
        return getDao().query(qb.prepare());
    }

    public Anak getFirstId() throws SQLException{
        QueryBuilder<Anak, Integer> qBuilder = getDao().queryBuilder();
        qBuilder.orderBy("ID", true);
        return getDao().queryForFirst(qBuilder.prepare());
    }

    public Anak getFirstIdByOrtu(int user_id) throws SQLException{
        QueryBuilder<Anak, Integer> qBuilder = getDao().queryBuilder();
        qBuilder.where().eq(Anak.USER_ID, user_id);
        qBuilder.orderBy("ID", true);
        return getDao().queryForFirst(qBuilder.prepare());
    }

}