package com.example.crownapplication.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = RekomendasiGizi.TBL_NAME)
public class RekomendasiGizi {
    public static final String TBL_NAME = "rekomendasi_gizi";
    public static final String ID = "id";
    public static final String USIA = "usia";
    public static final String DESKRIPSI = "deskripsi";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = USIA) private int usia;
    @DatabaseField(columnName = DESKRIPSI) private String deskripsi;

    public RekomendasiGizi() {
    }

    public int getId_rekom_gizi() {
        return id;
    }

    public void setId_rekom_gizi(int id) {
        this.id = id;
    }

    public int getUsia() {
        return usia;
    }

    public void setUsia(int usia) {
        this.usia = usia;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

}
