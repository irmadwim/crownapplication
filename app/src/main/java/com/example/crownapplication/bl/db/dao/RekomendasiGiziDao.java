package com.example.crownapplication.bl.db.dao;

import com.example.crownapplication.bl.db.model.RekomendasiGizi;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class RekomendasiGiziDao extends BaseDaoCrud<RekomendasiGizi, Integer> {
    private static RekomendasiGiziDao rekomendasiGiziDao;

    public static RekomendasiGiziDao getRekomendasigiziDao(){
        if (rekomendasiGiziDao == null){
            rekomendasiGiziDao = new RekomendasiGiziDao();
        }
        return rekomendasiGiziDao;
    }

    public RekomendasiGizi getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }
}
