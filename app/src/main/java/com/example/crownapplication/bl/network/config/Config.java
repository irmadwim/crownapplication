package com.example.crownapplication.bl.network.config;

public class Config {
    public static final String BASE_URL = "http://irma.tutorteknik.com/";
    //public static final String BASE_URL = "http://192.168.1.7:8000/";

    public static final String API_LOGIN_USER = BASE_URL + "api/v1/login";
    public static final String API_REGISTER_USER = BASE_URL + "api/v1/register";
    public static final String API_READ_USER = BASE_URL + "api/v1/readuser";
    public static final String API_UPDATE_USER = BASE_URL + "api/v1/updateuser";
    public static final String API_UPDATE_PASSWORD = BASE_URL + "api/v1/updatepass";
    public static final String API_DELETE_USER = BASE_URL + "api/v1/deleteuser";

    public static final String API_READ_ANAK = BASE_URL + "api/v1/readanak";
    public static final String API_ADD_ANAK = BASE_URL + "api/v1/addanak";
    public static final String API_UPDATE_ANAK = BASE_URL + "api/v1/updateanak";
    public static final String API_DELETE_ANAK = BASE_URL + "api/v1/deleteanak";

    public static final String API_READ_ASPEK = BASE_URL + "api/v1/readaspek";

    public static final String API_READ_PERTANYAAN = BASE_URL +"api/v1/readpertanyaan";
    public static final String API_GETID_PERTANYAAN = BASE_URL + "api/v1/showpertanyaan/{id}";

    public static final String API_GET_BB = BASE_URL + "api/v1/getbb";
    public static final String API_GET_TB = BASE_URL + "api/v1/gettb";
    public static final String API_GET_LK = BASE_URL + "api/v1/getlk";
    public static final String API_GET_IMT = BASE_URL + "api/v1/getimt";

    public static final String API_GET_DATA_ANAK = BASE_URL + "api/v1/getdataanak";
    public static final String API_GET_DATA_BB = BASE_URL + "api/v1/getdatabb";
    public static final String API_GET_DATA_TB = BASE_URL + "api/v1/getdatatb";
    public static final String API_GET_DATA_LK = BASE_URL + "api/v1/getdatalk";
    public static final String API_GET_DATA_IMT = BASE_URL + "api/v1/getdataimt";

    public static final String API_GET_PERTANYAAN = BASE_URL + "api/v1/getpertanyaan";
    public static final String API_GET_JAWABAN = BASE_URL + "api/v1/getjawaban";
    public static final String API_GET_REPORT = BASE_URL + "api/v1/getreport";

    public static final String API_READ_BB = BASE_URL + "api/v1/readbb";
    public static final String API_READ_TB = BASE_URL + "api/v1/readtb";
    public static final String API_READ_LK = BASE_URL + "api/v1/readlk";
    public static final String API_READ_IMT = BASE_URL + "api/v1/readimt";

    public static final String API_READ_REKOMGIZI = BASE_URL + "api/v1/readrekomgizi";

    public static final String API_READ_DATAGIZI = BASE_URL + "api/v1/readgizi";
    public static final String API_ADD_DATAGIZI = BASE_URL + "api/v1/addgizi";
    public static final String API_UPDATE_DATAGIZI = BASE_URL + "api/v1/updategizi";
    public static final String API_DELETE_DATAGIZI = BASE_URL + "api/v1/deletegizi";

    public static final String API_READ_HASIL = BASE_URL + "api/v1/readhasil";
    public static final String API_ADD_HASIL = BASE_URL + "api/v1/addhasil";
    public static final String API_UPDATE_HASIL = BASE_URL + "api/v1/updatehasil";
    public static final String API_DELETE_HASIL = BASE_URL + "api/v1/deletehasil";

    public static final String API_READ_JAWABAN = BASE_URL + "api/v1/readjawaban";
    public static final String API_ADD_JAWABAN = BASE_URL + "api/v1/addjawaban";
    public static final String API_UPDATE_JAWABAN = BASE_URL + "api/v1/updatejawaban";
    public static final String API_DELETE_JAWABAN = BASE_URL + "api/v1/deletejawaban";

    public static final String API_UPDATE_REPORTED = BASE_URL + "api/v1/updatereported";

    public static final String API_READ_REPORT = BASE_URL + "api/v1/readreport";
    public static final String API_UPDATE_REPORT = BASE_URL + "api/v1/updatereport";
    public static final String API_ADD_REPORT = BASE_URL + "api/v1/addreport";
    public static final String API_DELETE_REPORT = BASE_URL + "api/v1/deletereport";


}
