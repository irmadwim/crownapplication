package com.example.crownapplication.bl.db.dao;

import com.example.crownapplication.bl.db.model.ReportAkhir;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public class ReportAkhirDao extends BaseDaoCrud<ReportAkhir, Integer> {
    private static ReportAkhirDao reportAkhirDao;

    public static ReportAkhirDao getReportakhirDao(){
        if (reportAkhirDao == null){
            reportAkhirDao = new ReportAkhirDao();
        }
        return reportAkhirDao;
    }

    public ReportAkhir getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }

    public List<ReportAkhir> getByAnak(int anak_id) throws SQLException{
        QueryBuilder<ReportAkhir, Integer> qb = getDao().queryBuilder();
        qb.where().eq(ReportAkhir.ANAK_ID, anak_id);
        return getDao().query(qb.prepare());
    }

    public List<ReportAkhir> getByTgl(String tgl, int anak_id) throws SQLException{
        QueryBuilder<ReportAkhir, Integer> qb = getDao().queryBuilder();
        qb.where().eq(ReportAkhir.TANGGAL, tgl)
                .and().eq(ReportAkhir.ANAK_ID, anak_id);
        return getDao().query(qb.prepare());
    }

    public ReportAkhir getByLastID(int anak_id) throws SQLException{
        QueryBuilder<ReportAkhir, Integer> qBuilder = getDao().queryBuilder();
        qBuilder.where().eq(ReportAkhir.ANAK_ID, anak_id);
        qBuilder.orderBy("ID", false);
        return getDao().queryForFirst(qBuilder.prepare());
    }

}
