package com.example.crownapplication.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class DataReports {
    @SerializedName("id")
    @Expose
    private int idReport;
    @SerializedName("anak_id")
    @Expose
    private int idAnak;
    @SerializedName("hasil_akhir")
    @Expose
    private String hasilAkhir;
    @SerializedName("tanggal")
    @Expose
    private String tanggal;


    public int getIdReport() {
        return idReport;
    }

    public void setIdReport(int idReport) {
        this.idReport = idReport;
    }

    public int getIdAnak() {
        return idAnak;
    }

    public void setIdAnak(int idAnak) {
        this.idAnak = idAnak;
    }

    public String getHasilAkhir() {
        return hasilAkhir;
    }

    public void setHasilAkhir(String hasilAkhir) {
        this.hasilAkhir = hasilAkhir;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tglLahir) {
        this.tanggal= tanggal;
    }

}
