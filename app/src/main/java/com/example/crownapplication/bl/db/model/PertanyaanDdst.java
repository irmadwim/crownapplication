package com.example.crownapplication.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = PertanyaanDdst.TBL_NAME)
public class PertanyaanDdst {
    public static final String TBL_NAME = "pertanyaan_ddst";
    public static final String ID = "id";
    public static final String ASPEK_ID = "aspek_id";
    public static final String USIA = "usia";
    public static final String PERTANYAAN = "pertanyaan";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = ASPEK_ID) private int aspek_id;
    @DatabaseField(columnName = USIA) private int usia;
    @DatabaseField(columnName = PERTANYAAN) private String pertanyaan;

    public PertanyaanDdst() {
    }

    public int getId_pertanyaan() {
        return id;
    }

    public void setId_pertanyaan(int id) {
        this.id = id;
    }

    public int getId_aspek() {
        return aspek_id;
    }

    public void setId_aspek(int aspek_id) {
        this.aspek_id = aspek_id;
    }

    public int getUsia() {
        return usia;
    }

    public void setUsia(int usia) {
        this.usia = usia;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

}
