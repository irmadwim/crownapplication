package com.example.crownapplication.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class JawabanRespondens {
    @SerializedName("jawabanresponden")
    @Expose
    private List<JawabanResponden> jawabanresponden = null;

    public List<JawabanResponden> getJawabanresponden() {
        return jawabanresponden;
    }

    public void setJawabanResponden(List<JawabanResponden> jawabanresponden) {
        this.jawabanresponden = jawabanresponden;
    }

    public class JawabanResponden {

        @SerializedName("id")
        @Expose
        private int idJawaban;
        @SerializedName("anak_id")
        @Expose
        private int idAnak;
        @SerializedName("aspek_id")
        @Expose
        private int idAspek;
        @SerializedName("reported")
        @Expose
        private int reported;
        @SerializedName("jawaban_p")
        @Expose
        private int jawabanP;
        @SerializedName("jawaban_f")
        @Expose
        private int jawabanF;
        @SerializedName("jawaban_r")
        @Expose
        private int jawabanR;
        @SerializedName("tanggal")
        @Expose
        private Date tanggal;

        public int getIdJawaban() {
            return idJawaban;
        }

        public void setIdJawaban(int idJawaban) {
            this.idJawaban = idJawaban;
        }

        public int getIdAnak() {
            return idAnak;
        }

        public void setIdAnak(int idAnak) {
            this.idAnak = idAnak;
        }

        public int getIdAspek() {
            return idAspek;
        }

        public void setIdAspek(int idAspek) {
            this.idAspek = idAspek;
        }

        public int getReported() {
            return reported;
        }

        public void setReported(int reported) {
            this.reported = reported;
        }

        public int getJawabanP() {
            return jawabanP;
        }

        public void setJawabanP(int jawabanP) {
            this.jawabanP = jawabanP;
        }

        public int getJawabanF() {
            return jawabanF;
        }

        public void setJawabanF(int jawabanF) {
            this.jawabanF = jawabanF;
        }

        public int getJawabanR() {
            return jawabanR;
        }

        public void setJawabanR(int jawabanR) {
            this.jawabanR = jawabanR;
        }

        public Date getTanggal() {
            return tanggal;
        }

        public void setTanggal(Date tglLahir) {
            this.tanggal= tanggal;
        }



    }
}

