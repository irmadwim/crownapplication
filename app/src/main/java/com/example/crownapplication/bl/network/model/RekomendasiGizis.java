package com.example.crownapplication.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class RekomendasiGizis {
    @SerializedName("gizi")
    @Expose
    private List<RekomendasiGizi> rekomendasigizi = null;

    public List<RekomendasiGizi> getRekomendasigizi() {
        return rekomendasigizi;
    }

    public void setRekomendasigizi(List<RekomendasiGizi> rekomendasigizi) {
        this.rekomendasigizi = rekomendasigizi;
    }

    public class RekomendasiGizi {

        @SerializedName("id")
        @Expose
        private int idRekomGizi;
        @SerializedName("usia")
        @Expose
        private int usia;
        @SerializedName("deskripsi")
        @Expose
        private String deskripsi;


        public int getIdRekomGizi() {
            return idRekomGizi;
        }

        public void setIdRekomGizi(int idRekomGizi) {
            this.idRekomGizi = idRekomGizi;
        }

        public int getUsia() {
            return usia;
        }

        public void setUsia(int usia) {
            this.usia = usia;
        }

        public String getDeskripsi() {
            return deskripsi;
        }

        public void setDeskripsi(String deskripsi) {
            this.deskripsi = deskripsi;
        }


    }
}

