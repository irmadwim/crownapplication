package com.example.crownapplication.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = ReportAkhir.TBL_NAME)
public class ReportAkhir {
    public static final String TBL_NAME = "report_akhir";
    public static final String ID = "id";
    public static final String ANAK_ID = "anak_id";
    public static final String HASIL_AKHIR = "hasil_akhir";
    public static final String TANGGAL = "tanggal";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = ANAK_ID) private int anak_id;
    @DatabaseField(columnName = HASIL_AKHIR) private String hasil_akhir;
    @DatabaseField(columnName = TANGGAL) private String tanggal;

    public ReportAkhir() {
    }

    public int getId_reportAkhir() {
        return id;
    }

    public void setId_reportAkhir(int id) {
        this.id = id;
    }

    public int getAnak_id() {
        return anak_id;
    }

    public void setAnak_id(int anak_id) {
        this.anak_id = anak_id;
    }

    public String getHasil_akhir() {
        return hasil_akhir;
    }

    public void setHasil_akhir(String hasil_akhir) {
        this.hasil_akhir = hasil_akhir;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

}
