package com.example.crownapplication.bl.db.dao;

import com.example.crownapplication.bl.db.model.Tb;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class TbDao extends BaseDaoCrud<Tb, Integer> {
    private static TbDao tbDao;

    public static TbDao getTbDao(){
        if (tbDao == null){
            tbDao = new TbDao();
        }
        return tbDao;
    }

    public Tb getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }

    public List<Tb> getByJk(String jk) throws SQLException{
        QueryBuilder<Tb, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Tb.JK, jk);
        return getDao().query(qb.prepare());
    }

    public List<Tb> getByUsiaJk (int usia, String jk) throws SQLException{
        QueryBuilder<Tb, Integer> qb =  getDao().queryBuilder();
        qb.where().eq(Tb.USIA, usia)
                .and().eq(Tb.JK, jk);
        return getDao().query(qb.prepare());
    }
}
