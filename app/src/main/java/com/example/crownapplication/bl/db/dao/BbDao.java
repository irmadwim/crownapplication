package com.example.crownapplication.bl.db.dao;

import com.example.crownapplication.bl.db.model.Bb;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class BbDao extends BaseDaoCrud<Bb, Integer> {
    private static BbDao bbDao;

    public static BbDao getBbDao(){
        if (bbDao == null){
            bbDao = new BbDao();
        }
        return bbDao;
    }

    public Bb getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }

    public List<Bb> getByJk(String jk) throws SQLException{
        QueryBuilder<Bb, Integer> qb =  getDao().queryBuilder();
        qb.where().eq(Bb.JK, jk);
        return getDao().query(qb.prepare());
    }

    public List<Bb> getByUsiaJk (int usia, String jk) throws SQLException{
        QueryBuilder<Bb, Integer> qb =  getDao().queryBuilder();
        qb.where().eq(Bb.USIA, usia)
                .and().eq(Bb.JK, jk);
        return getDao().query(qb.prepare());
    }

}
