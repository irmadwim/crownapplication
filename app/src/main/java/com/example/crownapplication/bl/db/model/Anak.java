package com.example.crownapplication.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = Anak.TBL_NAME)
public class Anak {
    public static final String TBL_NAME = "anak";
    public static final String ID = "id";
    public static final String USER_ID = "user_id";
    public static final String NAMA_ANAK = "nama_anak";
    public static final String TGL_LAHIR_ANAK = "tgl_lahir_anak";
    public static final String GENDER_ANAK = "gender_anak";
    public static final String USIA = "usia";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = NAMA_ANAK) private String nama_anak;
    @DatabaseField(columnName = TGL_LAHIR_ANAK) private Date tgl_lahir_anak;
    @DatabaseField(columnName = GENDER_ANAK) private String gender_anak;
    @DatabaseField(columnName = USER_ID) private int user_id;
    @DatabaseField(columnName = USIA) private int usia;

    public Anak() {
    }

    public int getId_anak() {
        return id;
    }

    public void setId_anak(int id) {
        this.id = id;
    }

    public String getNama_anak() {
        return nama_anak;
    }

    public void setNama_anak(String nama_anak) {
        this.nama_anak = nama_anak;
    }

    public Date getTgl_lahir_anak() {
        return tgl_lahir_anak;
    }

    public void setTgl_lahir_anak(Date tgl_lahir_anak) {
        this.tgl_lahir_anak = tgl_lahir_anak;
    }

    public String getGender_anak() {
        return gender_anak;
    }

    public void setGender_anak(String gender_anak) {
        this.gender_anak = gender_anak;
    }

    public int getId_user() {
        return user_id;
    }

    public void setId_user(int user_id) {
        this.user_id = user_id;
    }

    public int getUsia() {
        return usia;
    }

    public void setUsia(int usia) {
        this.usia = usia;
    }
}
