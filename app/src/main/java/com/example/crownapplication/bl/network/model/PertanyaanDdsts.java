package com.example.crownapplication.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class PertanyaanDdsts {
    @SerializedName("pertanyaanddst")
    @Expose
    private List<PertanyaanDdst> pertanyaanddst = null;

    public List<PertanyaanDdst> getPertanyaanddst() {
        return pertanyaanddst;
    }

    public void setPertanyaanddst(List<PertanyaanDdst> pertanyaanddst) {
        this.pertanyaanddst = pertanyaanddst;
    }

    public class PertanyaanDdst {

        @SerializedName("id")
        @Expose
        private int idPertanyaan;
        @SerializedName("aspek_id")
        @Expose
        private int idAspek;
        @SerializedName("usia")
        @Expose
        private int usia;
        @SerializedName("pertanyaan")
        @Expose
        private String pertanyaan;


        public int getIdPertanyaan() {
            return idPertanyaan;
        }

        public void setIdPertanyaan(int idPertanyaan) {
            this.idPertanyaan = idPertanyaan;
        }

        public int getIdAspek() {
            return idAspek;
        }

        public void setIdAspek(int idAspek) {
            this.idAspek = idAspek;
        }

        public int getUsia() {
            return usia;
        }

        public void setUsia(int usia) {
            this.usia = usia;
        }

        public String getPertanyaan() {
            return pertanyaan;
        }

        public void setPertanyaan(String pertanyaan) {
            this.pertanyaan = pertanyaan;
        }


    }
}

