package com.example.crownapplication.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class DataJawabans {

    @SerializedName("aspek_id")
    @Expose
    private int idAspek;
    @SerializedName("jawaban_p")
    @Expose
    private int jawabanP;
    @SerializedName("jawaban_f")
    @Expose
    private int jawabanF;
    @SerializedName("jawaban_r")
    @Expose
    private int jawabanR;


    public int getIdAspek() {
        return idAspek;
    }

    public void setIdAspek(int idAspek) {
        this.idAspek = idAspek;
    }

    public int getJawabanP() {
        return jawabanP;
    }

    public void setJawabanP(int jawabanP) {
        this.jawabanP = jawabanP;
    }

    public int getJawabanF() {
        return jawabanF;
    }

    public void setJawabanF(int jawabanF) {
        this.jawabanF = jawabanF;
    }

    public int getJawabanR() {
        return jawabanR;
    }

    public void setJawabanR(int jawabanR) {
        this.jawabanR = jawabanR;
    }

}
