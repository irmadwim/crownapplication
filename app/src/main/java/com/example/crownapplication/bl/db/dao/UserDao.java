package com.example.crownapplication.bl.db.dao;

import com.example.crownapplication.bl.db.model.User;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class UserDao extends BaseDaoCrud<User, Integer> {
    private static UserDao userDao;

    public static UserDao getUserDao(){
        if (userDao == null){
            userDao = new UserDao();
        }
        return userDao;
    }

    public User getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }
}
