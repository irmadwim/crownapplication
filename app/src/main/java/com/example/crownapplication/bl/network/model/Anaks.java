package com.example.crownapplication.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class Anaks {
    @SerializedName("anak")
    @Expose
    private List<Anak> anak = null;

    public List<Anak> getAnak() {
        return anak;
    }

    public void setAnak(List<Anak> anak) {
        this.anak = anak;
    }

    public class Anak {

        @SerializedName("id")
        @Expose
        private int idAnak;
        @SerializedName("nama_anak")
        @Expose
        private String namaAnak;
        @SerializedName("tgl_lahir_anak")
        @Expose
        private Date tglLahir;
        @SerializedName("gender_anak")
        @Expose
        private String genderAnak;
        @SerializedName("user_id")
        @Expose
        private int idUser;

        public int getIdAnak() {
            return idAnak;
        }

        public void setIdAnak(int idAnak) {
            this.idAnak = idAnak;
        }

        public String getNamaAnak() {
            return namaAnak;
        }

        public void setNamaAnak(String namaAnak) {
            this.namaAnak = namaAnak;
        }

        public Date getTglLahirAnak() {
            return tglLahir;
        }

        public void setTglLahirAnak(Date tglLahir) {
            this.tglLahir = tglLahir;
        }

        public String getGenderAnak() {
            return genderAnak;
        }

        public void setGenderAnak(String genderAnak) {
            this.genderAnak = genderAnak;
        }

        public int getIdUser() {
            return idUser;
        }

        public void setIdUser(int idUser) {
            this.idUser = idUser;
        }
    }
}

