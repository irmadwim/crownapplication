package com.example.crownapplication.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = DataGizi.TBL_NAME)
public class DataGizi {
    public static final String TBL_NAME = "data_gizi";
    public static final String ID = "id";
    public static final String ANAK_ID = "anak_id";
    public static final String TB_ANAK = "tb_anak";
    public static final String BB_ANAK = "bb_anak";
    public static final String LK_ANAK = "lk_anak";
    public static final String IMT_ANAK = "imt_anak";
    public static final String TANGGAL = "tanggal";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = ANAK_ID) private int anak_id;
    @DatabaseField(columnName = TB_ANAK) private float tb_anak;
    @DatabaseField(columnName = BB_ANAK) private float bb_anak;
    @DatabaseField(columnName = LK_ANAK) private float lk_anak;
    @DatabaseField(columnName = IMT_ANAK) private float imt_anak;
    @DatabaseField(columnName = TANGGAL) private Date tanggal;

    public DataGizi() {
    }

    public int getId_gizi() {
        return id;
    }

    public void setId_gizi(int id) {
        this.id = id;
    }

    public float getTb() {
        return tb_anak;
    }

    public void setTb(float tb_anak) {
        this.tb_anak = tb_anak;
    }

    public float getBb() {
        return bb_anak;
    }

    public void setBb(float bb_anak) {
        this.bb_anak = bb_anak;
    }

    public float getLk() {
        return lk_anak;
    }

    public void setLk(float lk_anak) {
        this.lk_anak = lk_anak;
    }

    public float getImt() {
        return imt_anak;
    }

    public void setImt(float imt_anak) {
        this.imt_anak = imt_anak;
    }

    public int getId_anak() {
        return anak_id;
    }

    public void setId_anak(int anak_id) {
        this.anak_id = anak_id;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

}
