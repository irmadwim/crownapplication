package com.example.crownapplication.bl.db.dao;


import com.example.crownapplication.bl.db.model.Aspek;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AspekDao extends BaseDaoCrud<Aspek, Integer> {
    private static AspekDao aspekDao;

    public static AspekDao getAspekDao(){
        if (aspekDao == null){
            aspekDao = new AspekDao();
        }
        return aspekDao;
    }

    public Aspek getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }


}

