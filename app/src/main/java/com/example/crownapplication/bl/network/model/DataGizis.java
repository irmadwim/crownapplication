package com.example.crownapplication.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class DataGizis {
    @SerializedName("datagizi")
    @Expose
    private List<DataGizi> datagizi = null;

    public List<DataGizi> getDatagizi() {
        return datagizi;
    }

    public void setDatagizi(List<DataGizi> datagizi) {
        this.datagizi = datagizi;
    }

    public class DataGizi {

        @SerializedName("id")
        @Expose
        private int idDataGizi;
        @SerializedName("anak_id")
        @Expose
        private int idAnak;
        @SerializedName("tb_anak")
        @Expose
        private float tbAnak;
        @SerializedName("bb_anak")
        @Expose
        private float bbAnak;
        @SerializedName("lk_anak")
        @Expose
        private float lkAnak;
        @SerializedName("imt_anak")
        @Expose
        private float imtAnak;
        @SerializedName("tanggal")
        @Expose
        private Date tanggal;

        public int getIdDataGizi() {
            return idDataGizi;
        }

        public void setIdDataGizi(int idDataGizi) {
            this.idDataGizi = idDataGizi;
        }

        public int getIdAnak() {
            return idAnak;
        }

        public void setIdAnak(int idAnak) {
            this.idAnak = idAnak;
        }

        public float getTbAnak() {
            return tbAnak;
        }

        public void setTbAnak(float tbAnak) {
            this.tbAnak = tbAnak;
        }

        public float getBbAnak() {
            return bbAnak;
        }

        public void setBbAnak(float bbAnak) {
            this.bbAnak = bbAnak;
        }

        public float getLkAnak() {
            return lkAnak;
        }

        public void setLkAnak(float lkAnak) {
            this.lkAnak = lkAnak;
        }

        public float getImtAnak() {
            return imtAnak;
        }

        public void setImtAnak(float imtAnak) {
            this.imtAnak = imtAnak;
        }

        //Ada kemungkinan ganti string, belum fix
        public Date getTanggal() {
            return tanggal;
        }

        public void setTanggal(Date tanggal) {
            this.tanggal = tanggal;
        }

    }
}

