package com.example.crownapplication.bl.network.api;

import com.example.crownapplication.bl.network.config.Config;
import com.example.crownapplication.bl.network.model.Anaks;
import com.example.crownapplication.bl.network.model.Aspeks;
import com.example.crownapplication.bl.network.model.BaseRespons;
import com.example.crownapplication.bl.network.model.Bbs;
import com.example.crownapplication.bl.network.model.DataAnaks;
import com.example.crownapplication.bl.network.model.DataBbs;
import com.example.crownapplication.bl.network.model.DataGizis;
import com.example.crownapplication.bl.network.model.DataImts;
import com.example.crownapplication.bl.network.model.DataJawabans;
import com.example.crownapplication.bl.network.model.DataLks;
import com.example.crownapplication.bl.network.model.DataPertanyaans;
import com.example.crownapplication.bl.network.model.DataReports;
import com.example.crownapplication.bl.network.model.DataTbs;
import com.example.crownapplication.bl.network.model.HasilGizis;
import com.example.crownapplication.bl.network.model.Imts;
import com.example.crownapplication.bl.network.model.JawabanRespondens;
import com.example.crownapplication.bl.network.model.Lks;
import com.example.crownapplication.bl.network.model.PertanyaanDdsts;
import com.example.crownapplication.bl.network.model.RekomendasiGizis;
import com.example.crownapplication.bl.network.model.ReportAkhirs;
import com.example.crownapplication.bl.network.model.StandardBbs;
import com.example.crownapplication.bl.network.model.StandardImts;
import com.example.crownapplication.bl.network.model.StandardLks;
import com.example.crownapplication.bl.network.model.StandardTbs;
import com.example.crownapplication.bl.network.model.Tbs;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.bl.network.model.Users;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Api {
    @FormUrlEncoded
    @POST(Config.API_LOGIN_USER)
    Call<UserOrtu> loginUser(
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST(Config.API_REGISTER_USER)
    Call<UserOrtu> registerUser(
            @Field("nama") String nama,
            @Field("email") String email,
            @Field("password") String password
    );


    @GET(Config.API_READ_USER)
    Call<Users> getUser();

    @GET(Config.API_READ_ANAK)
    Call<Anaks> getAnak();

    @GET(Config.API_READ_ASPEK)
    Call<Aspeks> getAspek();

//    @GET(Config.API_READ_PERTANYAAN)
//    Call<PertanyaanDdsts> getPertanyaan();

    @GET(Config.API_READ_REKOMGIZI)
    Call<RekomendasiGizis> getRekomGizi();

    @GET(Config.API_READ_DATAGIZI)
    Call<DataGizis> getDataGizi();

    @GET(Config.API_READ_HASIL)
    Call<HasilGizis> getHasilGizi();

    @GET(Config.API_READ_REPORT)
    Call<ReportAkhirs> getReport();

    @GET(Config.API_READ_TB)
    Call<Tbs> getTb();

    @GET(Config.API_READ_BB)
    Call<Bbs> getBb();

    @GET(Config.API_READ_LK)
    Call<Lks> getLk();

    @GET(Config.API_READ_IMT)
    Call<Imts> getImt();

    @GET(Config.API_READ_JAWABAN)
    Call<JawabanRespondens> getJawaban();

    @FormUrlEncoded
    @POST(Config.API_GET_REPORT)
    Call<ReportAkhirs> getReport(
            @Field("anak_id")      int anak_id,
            @Field("tanggal")      String tanggal
    );

    @FormUrlEncoded
    @POST(Config.API_GET_PERTANYAAN)
    Call<PertanyaanDdsts> getDataPertanyaan(
            @Field("aspek_id")  int aspek_id,
            @Field("usia")      int usia
    );

    @FormUrlEncoded
    @POST(Config.API_GET_DATA_ANAK)
    Call<DataAnaks> getDataAnak(
            @Field("user_id")  int user_id
    );


    @FormUrlEncoded
    @POST(Config.API_GET_JAWABAN)
    Call<List<DataJawabans>> getDataJawaban(
            @Field("anak_id")      int anak_id,
            @Field("tanggal")      String tanggal
    );

    @FormUrlEncoded
    @POST(Config.API_GET_BB)
    Call<StandardBbs> getBbStandard(
            @Field("usia")      int usia,
            @Field("jk")        String jk
    );

    @FormUrlEncoded
    @POST(Config.API_GET_TB)
    Call<StandardTbs> getTbStandard(
            @Field("usia")      int usia,
            @Field("jk")        String jk
    );

    @FormUrlEncoded
    @POST(Config.API_GET_LK)
    Call<StandardLks> getLkStandard(
            @Field("usia")      int usia,
            @Field("jk")        String jk
    );

    @FormUrlEncoded
    @POST(Config.API_GET_IMT)
    Call<StandardImts> getImtStandard(
            @Field("usia")    int usia,
            @Field("jk")        String jk
    );

    @FormUrlEncoded
    @POST(Config.API_GET_DATA_BB)
    Call<List<DataBbs>> getDataBb(
            @Field("jk")   String jk
    );


    @FormUrlEncoded
    @POST(Config.API_GET_DATA_TB)
    Call<List<DataTbs>> getDataTb(
            @Field("jk")   String jk
    );

    @FormUrlEncoded
    @POST(Config.API_GET_DATA_LK)
    Call<List<DataLks>> getDataLk(
            @Field("jk")   String jk
    );

    @FormUrlEncoded
    @POST(Config.API_GET_DATA_IMT)
    Call<List<DataImts>> getDataImt(
            @Field("jk")   String jk
    );


    @FormUrlEncoded
    @POST(Config.API_ADD_ANAK)
    Call<BaseRespons> addAnak(
            @Field("nama_anak")      String nama_anak,
            @Field("tgl_lahir_anak") String tgl_lahir_anak,
            @Field("gender_anak")    String gender_anak,
            @Field("user_id")        int user_id
    );

    @FormUrlEncoded
    @POST(Config.API_ADD_DATAGIZI)
    Call<BaseRespons> addDataGizi(
            @Field("tanggal")      String tanggal,
            @Field("tb_anak")      String tb_anak,
            @Field("bb_anak")      String bb_anak,
            @Field("lk_anak")      String lk_anak,
            @Field("imt_anak")      String imt_anak,
            @Field("anak_id")       int anak_id
    );
//
    @FormUrlEncoded
    @POST(Config.API_ADD_HASIL)
    Call<BaseRespons> addHasilGizi(
            @Field("gizi_id") int gizi_id,
            @Field("hasil_tb") String hasil_tb,
            @Field("hasil_bb") String hasil_bb,
            @Field("hasil_lk") String hasil_lk,
            @Field("hasil_imt") String hasil_imt
    );

    @FormUrlEncoded
    @POST(Config.API_ADD_JAWABAN)
    Call<BaseRespons> addJawaban(
            @Field("aspek_id") int aspek_id,
            @Field("anak_id") int anak_id,
            @Field("jawaban_p") int jawaban_p,
            @Field("jawaban_f") int jawaban_f,
            @Field("jawaban_r") int jawaban_r
    );

    @FormUrlEncoded
    @POST(Config.API_ADD_REPORT)
    Call<BaseRespons> addReport(
            @Field("anak_id") int anak_id,
            @Field("hasil_akhir") String hasil_akhir
    );


//    @FormUrlEncoded
//    @POST(Config.API_ADD_DATAGIZI)
//    Call<BaseRespons> addDataGizi(
//            @Field("id_murid") int id_murid,
//            @Field("id_guru") int id_guru,
//            @Field("id_kegiatan") int id_kegiatan,
//            @Field("keterangan") String keterangan,
//            @Field("waktu") String waktu ,
//            @Field("id_tppa") int id_tppa
//    );

    @FormUrlEncoded
    @POST(Config.API_UPDATE_ANAK)
    Call<BaseRespons> updateAnak(
            @Field("id") int id,
            @Field("user_id") int user_id,
            @Field("nama_anak") String nama_anak,
            @Field("tgl_lahir_anak") String tgl_lahir_anak,
            @Field("gender_anak") String gender_anak
    );

    @FormUrlEncoded
    @POST(Config.API_UPDATE_USER)
    Call<BaseRespons> updateUser(
            @Field("id") int id,
            @Field("nama") String nama,
            @Field("email") String email
    );

    @FormUrlEncoded
    @POST(Config.API_UPDATE_REPORTED)
    Call<BaseRespons> updateReported(
            @Field("anak_id") int anak_id
    );

    @FormUrlEncoded
    @POST(Config.API_GETID_PERTANYAAN)
    Call<PertanyaanDdsts> getPertanyaan(
            @Field("aspek_id") int aspek_id,
            @Field("usia") int usia
    );

}

