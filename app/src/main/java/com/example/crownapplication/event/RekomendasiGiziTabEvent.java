package com.example.crownapplication.event;

public class RekomendasiGiziTabEvent {
    public final Integer idTab;

    public RekomendasiGiziTabEvent(Integer idTab) {
        this.idTab = idTab;
    }
}
