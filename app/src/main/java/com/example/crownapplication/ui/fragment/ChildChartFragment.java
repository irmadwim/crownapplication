package com.example.crownapplication.ui.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.helper.Db;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.Bb;
import com.example.crownapplication.bl.db.model.DataGizi;
import com.example.crownapplication.bl.db.model.HasilGizi;
import com.example.crownapplication.bl.db.model.Imt;
import com.example.crownapplication.bl.db.model.Lk;
import com.example.crownapplication.bl.db.model.Tb;
import com.example.crownapplication.bl.db.model.User;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.api.SyncWorker;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.Anaks;
import com.example.crownapplication.bl.network.model.BaseRespons;
import com.example.crownapplication.bl.network.model.Bbs;
import com.example.crownapplication.bl.network.model.DataBbs;
import com.example.crownapplication.bl.network.model.DataImts;
import com.example.crownapplication.bl.network.model.DataLks;
import com.example.crownapplication.bl.network.model.DataTbs;
import com.example.crownapplication.bl.network.model.StandardBbs;
import com.example.crownapplication.bl.network.model.StandardImts;
import com.example.crownapplication.bl.network.model.StandardLks;
import com.example.crownapplication.bl.network.model.StandardTbs;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.activity.BMIChartActivity;
import com.example.crownapplication.ui.activity.DetailChildActivity;
import com.example.crownapplication.ui.activity.GiziActivity;
import com.example.crownapplication.ui.activity.HeadCircumChartActivity;
import com.example.crownapplication.ui.activity.HeightChartActivity;
import com.example.crownapplication.ui.activity.WeightChartActivity;
import com.example.crownapplication.ui.adapter.AnakAdapter;
import com.example.crownapplication.ui.adapter.DataGiziAdapter;
import com.example.crownapplication.ui.adapter.DialogAdapter;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.presenter.AnakPresenter;
import com.example.crownapplication.ui.presenter.BbPresenter;
import com.example.crownapplication.ui.presenter.DataGiziPresenter;
import com.example.crownapplication.ui.presenter.HasilGiziPresenter;
import com.example.crownapplication.ui.presenter.ImtPresenter;
import com.example.crownapplication.ui.presenter.LkPresenter;
import com.example.crownapplication.ui.presenter.TbPresenter;
import com.example.crownapplication.ui.presenter.UserPresenter;
import com.example.crownapplication.ui.util.PrefUtil;
import com.example.crownapplication.ui.view.AnakView;
import com.example.crownapplication.ui.view.BbView;
import com.example.crownapplication.ui.view.DataGiziView;
import com.example.crownapplication.ui.view.HasilGiziView;
import com.example.crownapplication.ui.view.ImtView;
import com.example.crownapplication.ui.view.LkView;
import com.example.crownapplication.ui.view.TbView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChildChartFragment extends Fragment implements AnakView, DataGiziView, HasilGiziView,
        LkView, BbView, ImtView, TbView {

    private Api mApi;
    private int id_user;
    private View view;
    public int id_anak;
    private UserPresenter userPresenter;
    private AnakPresenter anakPresenter;
    private AnakAdapter anakAdapter;
    private DialogAdapter dialogAdapter;
    private MaterialDialog dialog;
    private DataGiziPresenter dataGiziPresenter;
    private Bb bb;
    private BbPresenter bbPresenter;
    private TbPresenter tbPresenter;
    private LkPresenter lkPresenter;
    private ImtPresenter imtPresenter;
    private DataGizi dataGizi;
    private HasilGizi hasilGizi;
    private HasilGiziPresenter hasilGiziPresenter;
    private DataGiziAdapter dataGiziAdapter;
    private User user;
    private Anak anak;
    private Context context;
    private UserOrtu userOrtu;
    private RecyclerView recyclerView;
    private Bbs bbs;

    private String statusBb;
    private String statusTb;
    private String statusLk;
    private String statusImt;


    private List<Anak> anakList = new ArrayList<>();
    private List<DataGizi> dataGiziList = new ArrayList<>();
    //private List<DataBbs.DataBb> bbList = new ArrayList<DataBbs.DataBb>();
    private List<Tb> tbList = new ArrayList<>();
    private List<Lk> lkList = new ArrayList<>();
    private List<Imt> imtList = new ArrayList<>();

    private HashMap<String, Float> standardbb = new HashMap<>();
    private HashMap<String, Float> standardtb = new HashMap<>();
    private HashMap<String, Float> standardlk = new HashMap<>();
    private HashMap<String, Float> standardimt = new HashMap<>();


    @BindView(R.id.tv_namaanak_chart)
    public TextView tvNama;
    @BindView(R.id.tv_usia_chart)
    public TextView tvUsia;
    @BindView(R.id.tv_ketberat)
    TextView tvBb;
    @BindView(R.id.tv_kettinggi)
    TextView tvTb;
    @BindView(R.id.tv_ketlingkepala)
    TextView tvLk;
    @BindView(R.id.tv_ketimt)
    TextView tvImt;
    @BindView(R.id.tv_hasilberat)
    TextView tvKetBb;
    @BindView(R.id.tv_hasiltinggi)
    TextView tvKetTb;
    @BindView(R.id.tv_hasillingkepala)
    TextView tvKetLk;
    @BindView(R.id.tv_hasilimt)
    TextView tvKetImt;
    @BindView(R.id.tv_tgl)
    TextView tvTgl;
    @BindView(R.id.im_anak_chart)
    public ImageView ivAvatar;
    @BindView(R.id.chart_berat)
    LineChart chartBerat;
    @BindView(R.id.chart_tinggi)
    LineChart chartTinggi;
    @BindView(R.id.chart_lingkepala)
    LineChart chartKepala;
    @BindView(R.id.chart_imt)
    LineChart chartImt;
    boolean isStandartExist;

    public ChildChartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       view = inflater.inflate(R.layout.fragment_child_chart, container, false);

        ButterKnife.bind(this, view);
        Db.getInstance().init(getActivity());
        AndroidThreeTen.init(getActivity());
        mApi = RetrofitBuilder.builder(getActivity()).create(Api.class);
        isStandartExist = false;
        initData();
        getStandardBb();
        getDataBb();
        getDataTb();
        getDataLk();
        getDataImt();
//
        initView();

        return view;
    }


    private void initData() {
        userOrtu = PrefUtil.getUser(getActivity(), PrefUtil.USER_SESSION);
        anakPresenter = new AnakPresenter(getContext(), this);
        anak = anakPresenter.getFirstIdByOrtu(userOrtu.getIdUser());
        id_anak = anak.getId_anak();

        dataGiziPresenter = new DataGiziPresenter(getContext(), this);
        dataGizi = dataGiziPresenter.getLastIdByAnak(anak.getId_anak());

        anakList = anakPresenter.getByOrtu(userOrtu.getIdUser());
        dialogAdapter =  new DialogAdapter(getContext(), anakList);

        hasilGiziPresenter = new HasilGiziPresenter(getContext(), this);
        hasilGizi = hasilGiziPresenter.getLastIdByGizi(dataGizi.getId_gizi());

        dataGiziList = dataGiziPresenter.getByAnak(anak.getId_anak());


        //bbList = bbPresenter.getByJk(anak.getGender_anak());

    }

    private void getStandardBb() {
        mApi.getBbStandard(anak.getUsia(), anak.getGender_anak())
                .enqueue(new Callback<StandardBbs>() {
                    @Override
                    public void onResponse(Call<StandardBbs> call, Response<StandardBbs> response) {
                        if (response.isSuccessful()) {
                            StandardBbs bb = response.body();
                            Log.i("STANDARDBB_GET", response.message());
                            standardbb.put("m3sd", bb.getM3sd());
                            standardbb.put("m2sd", bb.getM2sd());
                            standardbb.put("m1sd", bb.getM1sd());
                            standardbb.put("medium", bb.getMedium());
                            standardbb.put("p1sd", bb.getP1sd());
                            standardbb.put("p2sd", bb.getP2sd());
                            standardbb.put("p3sd", bb.getP3sd());

                            statusBb = chainProcessBb();
                            tvKetBb.setText(chainProcessBb());

                            getStandardTb();
                        }
                    }

                    @Override
                    public void onFailure(Call<StandardBbs> call, Throwable t) {
                        DialogBuilder.showErrorDialog(context, t.getMessage());
                        Log.i("STANDARDBB_GET", t.getMessage());
                    }
                });
    }

    private void getStandardTb() {
        mApi.getTbStandard(anak.getUsia(), anak.getGender_anak())
                .enqueue(new Callback<StandardTbs>() {
            @Override
            public void onResponse(Call<StandardTbs> call, Response<StandardTbs> response) {
                if (response.isSuccessful()){
                    StandardTbs tb = response.body();
                    Log.i("STANDARDTB_GET", response.message());
                    standardtb.put("m3sd", tb.getM3sd());
                    standardtb.put("m2sd", tb.getM2sd());
                    standardtb.put("m1sd", tb.getM1sd());
                    standardtb.put("medium", tb.getMedium());
                    standardtb.put("p1sd", tb.getP1sd());
                    standardtb.put("p2sd", tb.getP2sd());
                    standardtb.put("p3sd", tb.getP3sd());

                    statusTb = chainProcessTb();
                    tvKetTb.setText(chainProcessTb());

                    getStandardLk();

                }
            }

            @Override
            public void onFailure(Call<StandardTbs> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("STANDARDTB_GET", t.getMessage());
            }
        });
    }

    private void getStandardLk() {
        mApi.getLkStandard(anak.getUsia(), anak.getGender_anak())
                .enqueue(new Callback<StandardLks>() {
            @Override
            public void onResponse(Call<StandardLks> call, Response<StandardLks> response) {
                if (response.isSuccessful()){
                    StandardLks lk = response.body();
                    Log.i("STANDARDLK_GET", response.message());
                    standardlk.put("m3sd", lk.getM3sd());
                    standardlk.put("m2sd", lk.getM2sd());
                    standardlk.put("m1sd", lk.getM1sd());
                    standardlk.put("medium", lk.getMedium());
                    standardlk.put("p1sd", lk.getP1sd());
                    standardlk.put("p2sd", lk.getP2sd());
                    standardlk.put("p3sd", lk.getP3sd());

                    statusLk = chainProcessLk();
                    tvKetLk.setText(chainProcessLk());

                    getStandardImt();
                }
            }

            @Override
            public void onFailure(Call<StandardLks> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("STANDARDLK_GET", t.getMessage());
            }
        });
    }

    private void getStandardImt() {
        mApi.getImtStandard(anak.getUsia(), anak.getGender_anak())
                .enqueue(new Callback<StandardImts>() {
            @Override
            public void onResponse(Call<StandardImts> call, Response<StandardImts> response) {
                if (response.isSuccessful()){
                    StandardImts imt = response.body();
                    Log.i("STANDARDIMT_GET", response.message());
                    standardimt.put("m3sd", imt.getM3sd());
                    standardimt.put("m2sd", imt.getM2sd());
                    standardimt.put("m1sd", imt.getM1sd());
                    standardimt.put("medium", imt.getMedium());
                    standardimt.put("p1sd", imt.getP1sd());
                    standardimt.put("p2sd", imt.getP2sd());
                    standardimt.put("p3sd", imt.getP3sd());

                    statusImt = chainProcessImt();
                    tvKetImt.setText(chainProcessImt());
                    isStandartExist = true;
                    addData();
                }
            }

            @Override
            public void onFailure(Call<StandardImts> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("STANDARDIMT_GET", t.getMessage());
            }
        });
    }


    @Nullable
    private String chainProcessBb() {
        if (standardbb.get("m3sd") != null && dataGizi.getBb() < standardbb.get("m3sd")) {
            return "Gizi Buruk";
        } else if (standardbb.get("m3sd") != null && dataGizi.getBb() >= standardbb.get("m3sd") && dataGizi.getBb() < standardbb.get("m2sd")) {
            return "Gizi Kurang";
        } else if (standardbb.get("m3sd") != null && dataGizi.getBb() >= standardbb.get("m2sd") && dataGizi.getBb() <= standardbb.get("p2sd")) {
            return "Gizi Baik";
        } else {
            return "Gizi Lebih";
        }
    }

    @Nullable
    private String chainProcessTb(){
        if (dataGizi.getTb() < standardtb.get("m3sd")){
            return "Sangat Pendek";
        }else if (dataGizi.getTb() >= standardtb.get("m3sd") && dataGizi.getTb() < standardtb.get("m2sd")){
            return "Pendek";
        }else if (dataGizi.getTb() >= standardtb.get("m2sd") && dataGizi.getTb() <= standardtb.get("p2sd")) {
            return "Normal";
        }else {
            return "Tinggi";
        }
    }

    @Nullable
    private String chainProcessLk(){
        if (dataGizi.getLk() < standardlk.get("m2sd")){
            return "Mikrosefalus";
        }else if (dataGizi.getLk() >= standardlk.get("m2sd") && dataGizi.getLk() <= standardlk.get("p2sd")){
            return "Normal";
        }else {
            return "Makrosefalus";
        }
    }

    @Nullable
    private String chainProcessImt(){
        if (dataGizi.getImt() < standardimt.get("m3sd")){
            return "Sangat Kurus";
        }else if (dataGizi.getImt() >= standardimt.get("m3sd") && dataGizi.getImt() < standardimt.get("m2sd")){
            return "Kurus";
        }else if (dataGizi.getImt() >= standardimt.get("m2sd") && dataGizi.getImt() <= standardimt.get("p1sd")) {
            return "Normal";
        }else if (dataGizi.getImt() > standardimt.get("p1sd") && dataGizi.getImt() <= standardimt.get("p2sd")){
            return "Gemuk";
        }else {
            return "Obesitas";
        }
    }

    private void addData() {
        mApi.addHasilGizi(dataGizi.getId_gizi(), statusTb, statusBb,
                statusLk, statusImt)
                .enqueue(new Callback<BaseRespons>() {
                    @Override
                    public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                        BaseRespons baseRespons = response.body();
                        SyncWorker.getSyncWorker().syncHasilGizi(context, mApi.getHasilGizi(), false);
                    }

                    @Override
                    public void onFailure(Call<BaseRespons> call, Throwable t) {

                    }
                });
    }

    private void getDataBb(){
        mApi.getDataBb(anak.getGender_anak())
                .enqueue(new Callback<List<DataBbs>>() {
                    @Override
                    public void onResponse(Call<List<DataBbs>> call, Response<List<DataBbs>> response) {
                        if (response.isSuccessful()){
                            List<DataBbs> dataBbs = response.body();
                            Log.i("DATABB_GET", response.message());
                            setLineChartBb(response.body(), dataGiziList);
                            //setLineChartGiziBb(dataGiziList);

                        }
                    }

                    @Override
                    public void onFailure(Call<List<DataBbs>> call, Throwable t) {
                        DialogBuilder.showErrorDialog(context, t.getMessage());
                        Log.i("DATABB_GET", t.getMessage());
                    }
                });
    }

    private void setLineChartBb(List<DataBbs> dataBbs, List<DataGizi> dataGiziBb){

        chartBerat.getLegend().setWordWrapEnabled(true);
        chartBerat.getLegend().setEnabled(true);
        chartBerat.setDrawGridBackground(false);
        chartBerat.getDescription().setEnabled(false);
        chartBerat.setDrawBorders(false);

        chartBerat.getAxisLeft().setEnabled(false);
        chartBerat.getAxisRight().setDrawAxisLine(false);
        chartBerat.getAxisRight().setDrawGridLines(false);
        chartBerat.getXAxis().setDrawAxisLine(false);
        chartBerat.getXAxis().setDrawGridLines(false);

        // enable touch gestures
        chartBerat.setTouchEnabled(true);

        // enable scaling and dragging
        chartBerat.setDragEnabled(true);
        chartBerat.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        chartBerat.setPinchZoom(false);

        ArrayList<String> xaxis = new ArrayList<>();
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        //ArrayList<ILineDataSet> dataSet2s = new ArrayList<>();

        dataSets.add(makeLineBb(dataBbs,"m3sd",Color.BLUE));
        dataSets.add(makeLineBb(dataBbs,"m2sd",Color.RED));
        dataSets.add(makeLineBb(dataBbs,"m1sd",Color.YELLOW));
        dataSets.add(makeLineBb(dataBbs,"medium",Color.GREEN));
        dataSets.add(makeLineBb(dataBbs,"p1sd",Color.YELLOW));
        dataSets.add(makeLineBb(dataBbs,"p2sd",Color.RED));
        dataSets.add(makeLineBb(dataBbs,"p3sd",Color.BLUE));

        dataSets.add(makeLineBbGizi(dataGiziBb,"bb_anak",Color.BLACK));

        chartBerat.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        chartBerat.getXAxis().setGranularity(1f);
        chartBerat.getAxisLeft().setGranularity(1f);

        LineData datas = new LineData(dataSets);
        //LineData datas2 = new LineData(dataSet2s);
        chartBerat.setData(datas);
        //chartBerat.setData(datas2);
        chartBerat.invalidate();

    }

    private ILineDataSet makeLineBb(List<DataBbs> dataBbs, String s, Integer color) {
        ArrayList<Entry> values = new ArrayList<>();
        for (int j=0;j<dataBbs.size();j++) {
            if (s.equals("m3sd")) {
                values.add(new Entry((float) dataBbs.get(j).getUsia(), (float) dataBbs.get(j).getM3sd()));
            } else if (s.equals("m2sd")) {
                values.add(new Entry((float) dataBbs.get(j).getUsia(), (float) dataBbs.get(j).getM2sd()));
            } else if (s.equals("m1sd")) {
                values.add(new Entry((float) dataBbs.get(j).getUsia(), (float) dataBbs.get(j).getM1sd()));
            } else if (s.equals("medium")) {
                values.add(new Entry((float) dataBbs.get(j).getUsia(), (float) dataBbs.get(j).getMedium()));
            } else if (s.equals("p1sd")) {
                values.add(new Entry((float) dataBbs.get(j).getUsia(), (float) dataBbs.get(j).getP1sd()));
            } else if (s.equals("p2sd")) {
                values.add(new Entry((float) dataBbs.get(j).getUsia(), (float) dataBbs.get(j).getP2sd()));
            } else if (s.equals("p3sd")) {
                values.add(new Entry((float) dataBbs.get(j).getUsia(), (float) dataBbs.get(j).getP3sd()));
            }
        }

        LineDataSet lineDataSet = new LineDataSet(values, s);
//        lineDataSet.setDrawValues(false);
        lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);

        // setting tampilan
        lineDataSet.setHighlightEnabled(true);
        lineDataSet.setLineWidth(1.5f);
//        lineDataSet.setCircleRadius(2f);
//            lineDataSet.setGradientColor(getGradientBackground().get(i).getStartColor(), getGradientBackground().get(i).getEndColor());
//        lineDataSet.setCircleColor(color);
        lineDataSet.setColor(color);
        lineDataSet.setDrawCircles(false);
//            lineDataSet.setDrawHighlightIndicators(true);
//            lineDataSet.setHighLightColor(Color.RED);

        return lineDataSet;
    }

    private ILineDataSet makeLineBbGizi(List<DataGizi> dataBb, String s, Integer color) {
        ArrayList<Entry> values2 = new ArrayList<>();
            for (int j=0;j<dataBb.size();j++) {
                if (s.equals("bb_anak")) {
                    values2.add(new Entry((int) anak.getUsia(), (float) dataBb.get(j).getBb()));
                }
            }

        LineDataSet lineDataSet2 = new LineDataSet(values2, s);
//        lineDataSet.setDrawValues(false);
        lineDataSet2.setAxisDependency(YAxis.AxisDependency.LEFT);

        // setting tampilan
        lineDataSet2.setHighlightEnabled(true);
        lineDataSet2.setLineWidth(1.5f);
        lineDataSet2.setCircleRadius(5f);
//            lineDataSet.setGradientColor(getGradientBackground().get(i).getStartColor(), getGradientBackground().get(i).getEndColor());
        lineDataSet2.setCircleColor(color);
        lineDataSet2.setColor(color);
        lineDataSet2.setDrawCircles(true);
//            lineDataSet.setDrawHighlightIndicators(true);
//            lineDataSet.setHighLightColor(Color.RED);

        return lineDataSet2;
    }

    private void getDataTb(){
        mApi.getDataTb(anak.getGender_anak())
                .enqueue(new Callback<List<DataTbs>>() {
                    @Override
                    public void onResponse(Call<List<DataTbs>> call, Response<List<DataTbs>> response) {
                        if (response.isSuccessful()){
                            List<DataTbs> dataTbs = response.body();
                            Log.i("DATATB_GET", response.message());
                            setLineChartTb(response.body(), dataGiziList);

                        }
                    }

                    @Override
                    public void onFailure(Call<List<DataTbs>> call, Throwable t) {
                        DialogBuilder.showErrorDialog(context, t.getMessage());
                        Log.i("DATATB_GET", t.getMessage());
                    }
                });


    }

    private void setLineChartTb(List<DataTbs> dataTbs, List<DataGizi> dataGiziTb){

        chartTinggi.getLegend().setWordWrapEnabled(true);
        chartTinggi.getLegend().setEnabled(true);
        chartTinggi.setDrawGridBackground(false);
        chartTinggi.getDescription().setEnabled(false);
        chartTinggi.setDrawBorders(false);

        chartTinggi.getAxisLeft().setEnabled(false);
        chartTinggi.getAxisRight().setDrawAxisLine(false);
        chartTinggi.getAxisRight().setDrawGridLines(false);
        chartTinggi.getXAxis().setDrawAxisLine(false);
        chartTinggi.getXAxis().setDrawGridLines(false);

        // enable touch gestures
        chartTinggi.setTouchEnabled(true);

        // enable scaling and dragging
        chartTinggi.setDragEnabled(true);
        chartTinggi.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        chartTinggi.setPinchZoom(false);

        ArrayList<String> xaxis = new ArrayList<>();
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        //ArrayList<ILineDataSet> dataSet2s = new ArrayList<>();

        dataSets.add(makeLineTb(dataTbs,"m3sd",Color.BLUE));
        dataSets.add(makeLineTb(dataTbs,"m2sd",Color.RED));
        dataSets.add(makeLineTb(dataTbs,"m1sd",Color.YELLOW));
        dataSets.add(makeLineTb(dataTbs,"medium",Color.GREEN));
        dataSets.add(makeLineTb(dataTbs,"p1sd",Color.YELLOW));
        dataSets.add(makeLineTb(dataTbs,"p2sd",Color.RED));
        dataSets.add(makeLineTb(dataTbs,"p3sd",Color.BLUE));

        dataSets.add(makeLineTbGizi(dataGiziTb,"tb_anak",Color.BLACK));

        chartTinggi.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        chartTinggi.getXAxis().setGranularity(1f);
        chartTinggi.getAxisLeft().setGranularity(1f);

        LineData datas = new LineData(dataSets);
        //LineData datas2 = new LineData(dataSet2s);
        chartTinggi.setData(datas);
        //chartTinggi.setData(datas2);
        chartTinggi.invalidate();

    }

    private ILineDataSet makeLineTb(List<DataTbs> dataTbs, String s, Integer color) {
        ArrayList<Entry> values = new ArrayList<>();
        for (int j=0;j<dataTbs.size();j++) {
            if (s.equals("m3sd")) {
                values.add(new Entry((float) dataTbs.get(j).getUsia(), (float) dataTbs.get(j).getM3sd()));
            } else if (s.equals("m2sd")) {
                values.add(new Entry((float) dataTbs.get(j).getUsia(), (float) dataTbs.get(j).getM2sd()));
            } else if (s.equals("m1sd")) {
                values.add(new Entry((float) dataTbs.get(j).getUsia(), (float) dataTbs.get(j).getM1sd()));
            } else if (s.equals("medium")) {
                values.add(new Entry((float) dataTbs.get(j).getUsia(), (float) dataTbs.get(j).getMedium()));
            } else if (s.equals("p1sd")) {
                values.add(new Entry((float) dataTbs.get(j).getUsia(), (float) dataTbs.get(j).getP1sd()));
            } else if (s.equals("p2sd")) {
                values.add(new Entry((float) dataTbs.get(j).getUsia(), (float) dataTbs.get(j).getP2sd()));
            } else if (s.equals("p3sd")) {
                values.add(new Entry((float) dataTbs.get(j).getUsia(), (float) dataTbs.get(j).getP3sd()));
            }
        }

        LineDataSet lineDataSet = new LineDataSet(values, s);
//        lineDataSet.setDrawValues(false);
        lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);

        // setting tampilan
        lineDataSet.setHighlightEnabled(true);
        lineDataSet.setLineWidth(1.5f);
//        lineDataSet.setCircleRadius(2f);
//            lineDataSet.setGradientColor(getGradientBackground().get(i).getStartColor(), getGradientBackground().get(i).getEndColor());
//        lineDataSet.setCircleColor(color);
        lineDataSet.setColor(color);
        lineDataSet.setDrawCircles(false);
//            lineDataSet.setDrawHighlightIndicators(true);
//            lineDataSet.setHighLightColor(Color.RED);

        return lineDataSet;
    }

    private ILineDataSet makeLineTbGizi(List<DataGizi> dataTb, String s, Integer color) {
        ArrayList<Entry> values2 = new ArrayList<>();
        for (int j=0;j<dataTb.size();j++) {
            if (s.equals("tb_anak")) {
                values2.add(new Entry((int) anak.getUsia(), (float) dataTb.get(j).getTb()));
            }
        }

        LineDataSet lineDataSet2 = new LineDataSet(values2, s);
//        lineDataSet.setDrawValues(false);
        lineDataSet2.setAxisDependency(YAxis.AxisDependency.LEFT);

        // setting tampilan
        lineDataSet2.setHighlightEnabled(true);
        lineDataSet2.setLineWidth(1.5f);
        lineDataSet2.setCircleRadius(5f);
//            lineDataSet.setGradientColor(getGradientBackground().get(i).getStartColor(), getGradientBackground().get(i).getEndColor());
        lineDataSet2.setCircleColor(color);
        lineDataSet2.setColor(color);
        lineDataSet2.setDrawCircles(true);
//            lineDataSet.setDrawHighlightIndicators(true);
//            lineDataSet.setHighLightColor(Color.RED);

        return lineDataSet2;
    }



    private void getDataLk(){
        mApi.getDataLk(anak.getGender_anak())
                .enqueue(new Callback<List<DataLks>>() {
                    @Override
                    public void onResponse(Call<List<DataLks>> call, Response<List<DataLks>> response) {
                        if (response.isSuccessful()){
                            List<DataLks> dataLks = response.body();
                            Log.i("DATALK_GET", response.message());
                            setLineChartLk(response.body(), dataGiziList);

                        }
                    }

                    @Override
                    public void onFailure(Call<List<DataLks>> call, Throwable t) {
                        DialogBuilder.showErrorDialog(context, t.getMessage());
                        Log.i("DATALK_GET", t.getMessage());
                    }
                });


    }

    private void setLineChartLk(List<DataLks> dataLks, List<DataGizi> dataGiziLk){

        chartKepala.getLegend().setWordWrapEnabled(true);
        chartKepala.getLegend().setEnabled(true);
        chartKepala.setDrawGridBackground(false);
        chartKepala.getDescription().setEnabled(false);
        chartKepala.setDrawBorders(false);

        chartKepala.getAxisLeft().setEnabled(false);
        chartKepala.getAxisRight().setDrawAxisLine(false);
        chartKepala.getAxisRight().setDrawGridLines(false);
        chartKepala.getXAxis().setDrawAxisLine(false);
        chartKepala.getXAxis().setDrawGridLines(false);

        // enable touch gestures
        chartKepala.setTouchEnabled(true);

        // enable scaling and dragging
        chartKepala.setDragEnabled(true);
        chartKepala.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        chartKepala.setPinchZoom(false);

        ArrayList<String> xaxis = new ArrayList<>();
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        //ArrayList<ILineDataSet> dataSet2s = new ArrayList<>();

        dataSets.add(makeLineLk(dataLks,"m3sd",Color.BLUE));
        dataSets.add(makeLineLk(dataLks,"m2sd",Color.RED));
        dataSets.add(makeLineLk(dataLks,"m1sd",Color.YELLOW));
        dataSets.add(makeLineLk(dataLks,"medium",Color.GREEN));
        dataSets.add(makeLineLk(dataLks,"p1sd",Color.YELLOW));
        dataSets.add(makeLineLk(dataLks,"p2sd",Color.RED));
        dataSets.add(makeLineLk(dataLks,"p3sd",Color.BLUE));

        dataSets.add(makeLineLkGizi(dataGiziLk,"lk_anak",Color.BLACK));

        chartKepala.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        chartKepala.getXAxis().setGranularity(1f);
        chartKepala.getAxisLeft().setGranularity(1f);

        LineData datas = new LineData(dataSets);
        //LineData datas2 = new LineData(dataSet2s);
        chartKepala.setData(datas);
        //chartKepala.setData(datas2);
        chartKepala.invalidate();

    }

    private ILineDataSet makeLineLk(List<DataLks> dataLks, String s, Integer color) {
        ArrayList<Entry> values = new ArrayList<>();
        for (int j=0;j<dataLks.size();j++) {
            if (s.equals("m3sd")) {
                values.add(new Entry((float) dataLks.get(j).getUsia(), (float) dataLks.get(j).getM3sd()));
            } else if (s.equals("m2sd")) {
                values.add(new Entry((float) dataLks.get(j).getUsia(), (float) dataLks.get(j).getM2sd()));
            } else if (s.equals("m1sd")) {
                values.add(new Entry((float) dataLks.get(j).getUsia(), (float) dataLks.get(j).getM1sd()));
            } else if (s.equals("medium")) {
                values.add(new Entry((float) dataLks.get(j).getUsia(), (float) dataLks.get(j).getMedium()));
            } else if (s.equals("p1sd")) {
                values.add(new Entry((float) dataLks.get(j).getUsia(), (float) dataLks.get(j).getP1sd()));
            } else if (s.equals("p2sd")) {
                values.add(new Entry((float) dataLks.get(j).getUsia(), (float) dataLks.get(j).getP2sd()));
            } else if (s.equals("p3sd")) {
                values.add(new Entry((float) dataLks.get(j).getUsia(), (float) dataLks.get(j).getP3sd()));
            }
        }

        LineDataSet lineDataSet = new LineDataSet(values, s);
//        lineDataSet.setDrawValues(false);
        lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);

        // setting tampilan
        lineDataSet.setHighlightEnabled(true);
        lineDataSet.setLineWidth(1.5f);
//        lineDataSet.setCircleRadius(2f);
//            lineDataSet.setGradientColor(getGradientBackground().get(i).getStartColor(), getGradientBackground().get(i).getEndColor());
//        lineDataSet.setCircleColor(color);
        lineDataSet.setColor(color);
        lineDataSet.setDrawCircles(false);
//            lineDataSet.setDrawHighlightIndicators(true);
//            lineDataSet.setHighLightColor(Color.RED);

        return lineDataSet;
    }

    private ILineDataSet makeLineLkGizi(List<DataGizi> dataLk, String s, Integer color) {
        ArrayList<Entry> values2 = new ArrayList<>();
        for (int j=0;j<dataLk.size();j++) {
            if (s.equals("lk_anak")) {
                values2.add(new Entry((int) anak.getUsia(), (float) dataLk.get(j).getLk()));
            }
        }

        LineDataSet lineDataSet2 = new LineDataSet(values2, s);
//        lineDataSet.setDrawValues(false);
        lineDataSet2.setAxisDependency(YAxis.AxisDependency.LEFT);

        // setting tampilan
        lineDataSet2.setHighlightEnabled(true);
        lineDataSet2.setLineWidth(1.5f);
        lineDataSet2.setCircleRadius(5f);
//            lineDataSet.setGradientColor(getGradientBackground().get(i).getStartColor(), getGradientBackground().get(i).getEndColor());
        lineDataSet2.setCircleColor(color);
        lineDataSet2.setColor(color);
        lineDataSet2.setDrawCircles(true);
//            lineDataSet.setDrawHighlightIndicators(true);
//            lineDataSet.setHighLightColor(Color.RED);

        return lineDataSet2;
    }



    private void getDataImt(){
        mApi.getDataImt(anak.getGender_anak())
                .enqueue(new Callback<List<DataImts>>() {
                    @Override
                    public void onResponse(Call<List<DataImts>> call, Response<List<DataImts>> response) {
                        if (response.isSuccessful()){
                            List<DataImts> dataLks = response.body();
                            Log.i("DATAIMT_GET", response.message());
                            setLineChartImt(response.body(), dataGiziList);

                        }
                    }

                    @Override
                    public void onFailure(Call<List<DataImts>> call, Throwable t) {
                        DialogBuilder.showErrorDialog(context, t.getMessage());
                        Log.i("DATAIMT_GET", t.getMessage());
                    }
                });


    }

    private void setLineChartImt(List<DataImts> dataImts, List<DataGizi> dataGiziImt){

        chartImt.getLegend().setWordWrapEnabled(true);
        chartImt.getLegend().setEnabled(true);
        chartImt.setDrawGridBackground(false);
        chartImt.getDescription().setEnabled(false);
        chartImt.setDrawBorders(false);

        chartImt.getAxisLeft().setEnabled(false);
        chartImt.getAxisRight().setDrawAxisLine(false);
        chartImt.getAxisRight().setDrawGridLines(false);
        chartImt.getXAxis().setDrawAxisLine(false);
        chartImt.getXAxis().setDrawGridLines(false);

        // enable touch gestures
        chartImt.setTouchEnabled(true);

        // enable scaling and dragging
        chartImt.setDragEnabled(true);
        chartImt.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        chartImt.setPinchZoom(false);

        ArrayList<String> xaxis = new ArrayList<>();
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        //ArrayList<ILineDataSet> dataSet2s = new ArrayList<>();

        dataSets.add(makeLineImt(dataImts,"m3sd",Color.BLUE));
        dataSets.add(makeLineImt(dataImts,"m2sd",Color.RED));
        dataSets.add(makeLineImt(dataImts,"m1sd",Color.YELLOW));
        dataSets.add(makeLineImt(dataImts,"medium",Color.GREEN));
        dataSets.add(makeLineImt(dataImts,"p1sd",Color.YELLOW));
        dataSets.add(makeLineImt(dataImts,"p2sd",Color.RED));
        dataSets.add(makeLineImt(dataImts,"p3sd",Color.BLUE));

        dataSets.add(makeLineImtGizi(dataGiziImt,"imt_anak",Color.BLACK));

        chartImt.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        chartImt.getXAxis().setGranularity(1f);
        chartImt.getAxisLeft().setGranularity(1f);

        LineData datas = new LineData(dataSets);
        //LineData datas2 = new LineData(dataSet2s);
        chartImt.setData(datas);
        //chartImt.setData(datas2);
        chartImt.invalidate();

    }

    private ILineDataSet makeLineImt(List<DataImts> dataImts, String s, Integer color) {
        ArrayList<Entry> values = new ArrayList<>();
        for (int j=0;j<dataImts.size();j++) {
            if (s.equals("m3sd")) {
                values.add(new Entry((float) dataImts.get(j).getUsia(), (float) dataImts.get(j).getM3sd()));
            } else if (s.equals("m2sd")) {
                values.add(new Entry((float) dataImts.get(j).getUsia(), (float) dataImts.get(j).getM2sd()));
            } else if (s.equals("m1sd")) {
                values.add(new Entry((float) dataImts.get(j).getUsia(), (float) dataImts.get(j).getM1sd()));
            } else if (s.equals("medium")) {
                values.add(new Entry((float) dataImts.get(j).getUsia(), (float) dataImts.get(j).getMedium()));
            } else if (s.equals("p1sd")) {
                values.add(new Entry((float) dataImts.get(j).getUsia(), (float) dataImts.get(j).getP1sd()));
            } else if (s.equals("p2sd")) {
                values.add(new Entry((float) dataImts.get(j).getUsia(), (float) dataImts.get(j).getP2sd()));
            } else if (s.equals("p3sd")) {
                values.add(new Entry((float) dataImts.get(j).getUsia(), (float) dataImts.get(j).getP3sd()));
            }
        }

        LineDataSet lineDataSet = new LineDataSet(values, s);
//        lineDataSet.setDrawValues(false);
        lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);

        // setting tampilan
        lineDataSet.setHighlightEnabled(true);
        lineDataSet.setLineWidth(1.5f);
//        lineDataSet.setCircleRadius(2f);
//            lineDataSet.setGradientColor(getGradientBackground().get(i).getStartColor(), getGradientBackground().get(i).getEndColor());
//        lineDataSet.setCircleColor(color);
        lineDataSet.setColor(color);
        lineDataSet.setDrawCircles(false);
//            lineDataSet.setDrawHighlightIndicators(true);
//            lineDataSet.setHighLightColor(Color.RED);

        return lineDataSet;
    }

    private ILineDataSet makeLineImtGizi(List<DataGizi> dataImt, String s, Integer color) {
        ArrayList<Entry> values2 = new ArrayList<>();
        for (int j=0;j<dataImt.size();j++) {
            if (s.equals("imt_anak")) {
                values2.add(new Entry((int) anak.getUsia(), (float) dataImt.get(j).getImt()));
            }
        }

        LineDataSet lineDataSet2 = new LineDataSet(values2, s);
//        lineDataSet.setDrawValues(false);
        lineDataSet2.setAxisDependency(YAxis.AxisDependency.LEFT);

        // setting tampilan
        lineDataSet2.setHighlightEnabled(true);
        lineDataSet2.setLineWidth(1.5f);
        lineDataSet2.setCircleRadius(5f);
//            lineDataSet.setGradientColor(getGradientBackground().get(i).getStartColor(), getGradientBackground().get(i).getEndColor());
        lineDataSet2.setCircleColor(color);
        lineDataSet2.setColor(color);
        lineDataSet2.setDrawCircles(true);
//            lineDataSet.setDrawHighlightIndicators(true);
//            lineDataSet.setHighLightColor(Color.RED);

        return lineDataSet2;
    }



    private void initView() {
        tvNama.setText(String.valueOf(anak.getNama_anak()));

        if (anak.getUsia() >= 12)
            tvUsia.setText(anak.getUsia()/12 + " tahun " + anak.getUsia()%12 + " bulan");
        else
            tvUsia.setText(anak.getUsia() + " bulan");

        // Set Avatar sesuai jenis kelamin dan umur
        if (anak.getGender_anak().equals("L")){
            ivAvatar.setImageResource(R.drawable.ic_baby_boy);
        }else {
            ivAvatar.setImageResource(R.drawable.ic_baby_girl);

        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        tvTgl.setText(sdf.format(dataGizi.getTanggal()));
        tvBb.setText(String.valueOf(dataGizi.getBb()));
        tvTb.setText(String.valueOf(dataGizi.getTb()));
        tvLk.setText(String.valueOf(dataGizi.getLk()));
        tvImt.setText(String.valueOf(dataGizi.getImt()));

    }


    @OnClick(R.id.btn_history) void goHistory(){
        Intent intent = new Intent(getContext(), DetailChildActivity.class);
        intent.putExtra("id", anak.getId_anak());
        startActivity (intent);
    }


    @OnClick(R.id.im_detail_chart) void goDialog(){
        AlertDialog.Builder mDialog = new AlertDialog.Builder(view.getContext())
                .setTitle("Pilih anak");
        LayoutInflater inflater = getLayoutInflater();
        View convertView = inflater.inflate(R.layout.fragment_list_anak_dialog, null);
        RecyclerView list = convertView.findViewById(R.id.rv_list_anak_dialog);
        DialogAdapter dialogAdapter = new DialogAdapter(convertView.getContext(), R.layout.item_dialog, anakList);
        list.setLayoutManager(new LinearLayoutManager(convertView.getContext()));
        list.setAdapter(dialogAdapter);
        dialogAdapter.setOnItemClickListener(new DialogAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                id_anak = anakList.get(position).getId_anak();
                tvNama.setText(anakList.get(position).getNama_anak());
                tvUsia.setText(anakList.get(position).getUsia() + " bulan");
                if (anakList.get(position).getGender_anak().equals("L")){
                    ivAvatar.setImageResource(R.drawable.ic_baby_boy);
                }else {
                    ivAvatar.setImageResource(R.drawable.ic_baby_girl);
                }
            }
        });
        mDialog.setView(convertView);
        mDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                anak = anakPresenter.getByID(id_anak);
                dataGizi = dataGiziPresenter.getLastIdByAnak(id_anak);
//                SyncWorker.getSyncWorker().syncDataGizi(getContext(), mApi.getDataGizi(), false);
//                loadGizi(dataGiziPresenter.getLastIdByAnak(id_anak));

                getStandardBb();
                addData();

                dataGiziList = dataGiziPresenter.getByAnak(id_anak);
                getDataBb();
                getDataTb();
                getDataLk();
                getDataImt();

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                tvTgl.setText(sdf.format(dataGizi.getTanggal()));
                tvBb.setText(String.valueOf(dataGizi.getBb()));
                tvTb.setText(String.valueOf(dataGizi.getTb()));
                tvLk.setText(String.valueOf(dataGizi.getLk()));
                tvImt.setText(String.valueOf(dataGizi.getImt()));


            }
        });
        mDialog.show();

    }


    @OnClick(R.id.card_chart_berat) void goWeight(){
        Intent intent = new Intent(getContext(), WeightChartActivity.class);
        intent.putExtra("id", anak.getId_anak());
        startActivity (intent);
    }

    @OnClick(R.id.card_chart_tinggi) void goHeight(){
        Intent intent = new Intent(getContext(), HeightChartActivity.class);
        intent.putExtra("id", anak.getId_anak());
        startActivity (intent);
    }

    @OnClick(R.id.card_chart_lingkepala) void goHeadCircum(){
        Intent intent = new Intent(getContext(), HeadCircumChartActivity.class);
        intent.putExtra("id", anak.getId_anak());
        startActivity (intent);
    }

    @OnClick(R.id.card_chart_imt) void goBmi(){
        Intent intent = new Intent(getContext(), BMIChartActivity.class);
        intent.putExtra("id", anak.getId_anak());
        startActivity (intent);
    }

    @OnClick(R.id.btn_gizi) void goNutrition(){
        Intent intent = new Intent(getContext(), GiziActivity.class);
        intent.putExtra("id", anak.getId_anak());
        startActivity (intent);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (anak != null && isStandartExist){
            //anak = anakPresenter.getByID(id_anak);
            dataGizi = dataGiziPresenter.getLastIdByAnak(anak.getId_anak());
//                SyncWorker.getSyncWorker().syncDataGizi(getContext(), mApi.getDataGizi(), false);
//                loadGizi(dataGiziPresenter.getLastIdByAnak(id_anak));

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            tvTgl.setText(sdf.format(dataGizi.getTanggal()));
            tvBb.setText(String.valueOf(dataGizi.getBb()));
            tvTb.setText(String.valueOf(dataGizi.getTb()));
            tvLk.setText(String.valueOf(dataGizi.getLk()));
            tvImt.setText(String.valueOf(dataGizi.getImt()));

            getStandardBb();
            addData();

            dataGiziList = dataGiziPresenter.getByAnak(anak.getId_anak());
            getDataBb();
            getDataTb();
            getDataLk();
            getDataImt();

        }

    }

    @Override
    public void showLoading() {
        dialog = DialogBuilder.showLoadingDialog(getContext(), "Updating data", "Please Wait", false);

    }

    @Override
    public void hideLoading() {
        dialog.dismiss();

    }

    @Override
    public void loadHasilGizi(List<HasilGizi> hasilGiziList) {

    }

    @Override
    public void loadTb(List<Tb> tbList) {

    }

    @Override
    public void loadLk(List<Lk> lkList) {

    }

    @Override
    public void loadImt(List<Imt> imtList) {

    }

    @Override
    public void loadBb(List<Bb> bbList) {

    }

    @Override
    public void loadDataGizi(List<DataGizi> dataGiziList) {

    }

    @Override
    public void loadGizi(DataGizi dataGizi) {


    }


    @Override
    public void loadAnak(List<Anak> anakList) {

    }
}
