package com.example.crownapplication.ui.util;

import android.widget.EditText;

import static android.text.TextUtils.isEmpty;

public class Util {

    public static boolean isEempty(EditText text){
        CharSequence str = text.getText().toString();
        return isEmpty(str);
    }
}
