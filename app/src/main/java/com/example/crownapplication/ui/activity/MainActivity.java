package com.example.crownapplication.ui.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.dao.AnakDao;
import com.example.crownapplication.bl.db.dao.AspekDao;
import com.example.crownapplication.bl.db.dao.DataGiziDao;
import com.example.crownapplication.bl.db.dao.HasilGiziDao;
import com.example.crownapplication.bl.db.dao.PertanyaanDdstDao;
import com.example.crownapplication.bl.db.dao.RekomendasiGiziDao;
import com.example.crownapplication.bl.db.dao.ReportAkhirDao;
import com.example.crownapplication.bl.db.dao.UserDao;
import com.example.crownapplication.bl.db.helper.Db;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.Aspek;
import com.example.crownapplication.bl.db.model.DataGizi;
import com.example.crownapplication.bl.db.model.HasilGizi;
import com.example.crownapplication.bl.db.model.PertanyaanDdst;
import com.example.crownapplication.bl.db.model.RekomendasiGizi;
import com.example.crownapplication.bl.db.model.ReportAkhir;
import com.example.crownapplication.bl.db.model.User;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.api.SyncWorker;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.fragment.AccountFragment;
import com.example.crownapplication.ui.fragment.ChildChartFragment;
import com.example.crownapplication.ui.fragment.DevelopmentFragment;
import com.example.crownapplication.ui.fragment.HomePageFragment;
import com.example.crownapplication.ui.fragment.NutritionFragment;
import com.example.crownapplication.R;
import com.example.crownapplication.ui.util.PrefUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.sql.SQLException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity{

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNav;

    private Api mApi;
    private boolean isFirstRun = true;
    private UserOrtu userOrtu;
    Fragment selectedFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Db.getInstance().init(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        userOrtu = PrefUtil.getUser(this, PrefUtil.USER_SESSION);
        new DoCloudSync(this).execute();
        initialCheck();
        init();

    }
    private void init() {
        Integer mode = getIntent().getIntExtra("mode", 0);

        bottomNav.setOnNavigationItemSelectedListener(navListener);

        if (mode == 0) {
            bottomNav.setSelectedItemId(R.id.navigation_home);
        }else if (mode == 1){
            bottomNav.setSelectedItemId(R.id.navigation_charts);
        }else if (mode == 2){
            bottomNav.setSelectedItemId(R.id.navigation_development);
        }else if (mode == 3) {
            bottomNav.setSelectedItemId(R.id.navigation_nutrition);
        }else if (mode == 4) {
            bottomNav.setSelectedItemId(R.id.navigation_account);
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;

            switch (item.getItemId()){
                case R.id.navigation_home:
                    selectedFragment = new HomePageFragment();
                    break;
                case R.id.navigation_charts:
                    selectedFragment = new ChildChartFragment();
                    break;
                case R.id.navigation_development:
                    selectedFragment = new DevelopmentFragment();
                    break;
                case R.id.navigation_nutrition:
                    selectedFragment = new NutritionFragment();
                    break;
                case R.id.navigation_account:
                    selectedFragment = new AccountFragment();
                    break;
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.framelayoutmain,selectedFragment).commit();
            return true;
        }
    };


    private class DoCloudSync extends AsyncTask<Void, Void, Void> {
        private MaterialDialog dialog;
        private final Context ctx;

        private DoCloudSync(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = DialogBuilder.showLoadingDialog(ctx, "Updating Data", "Please Wait", false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                SyncWorker.getSyncWorker().syncAnak(ctx, mApi.getAnak(), isFirstRun);
                SyncWorker.getSyncWorker().syncUser(ctx, mApi.getUser(), isFirstRun);
                SyncWorker.getSyncWorker().syncRekomGizi(ctx, mApi.getRekomGizi(), isFirstRun);
                SyncWorker.getSyncWorker().syncDataGizi(ctx, mApi.getDataGizi(), isFirstRun);
                SyncWorker.getSyncWorker().syncHasilGizi(ctx, mApi.getHasilGizi(), isFirstRun);
//                SyncWorker.getSyncWorker().syncPertanyaan(ctx, mApi.getPertanyaan(), isFirstRun);
                SyncWorker.getSyncWorker().syncAspek(ctx, mApi.getAspek(), isFirstRun);
                SyncWorker.getSyncWorker().syncReportAkhir(ctx, mApi.getReport(), isFirstRun);
                //SyncWorker.getSyncWorker().syncTppa(ctx, mApi.getTppa(), isFirstRun);
                if(isFirstRun) Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
        }
    }

    private void initialCheck(){
        try {
            List<Anak> anakCheck = AnakDao.getAnakDao().read();
            List<User> userCheck = UserDao.getUserDao().read();
            List<RekomendasiGizi> rekomgiziCheck = RekomendasiGiziDao.getRekomendasigiziDao().read();
            List<DataGizi> datagiziCheck = DataGiziDao.getDatagiziDao().read();
            List<HasilGizi> hasilGiziCheck = HasilGiziDao.getHasilGiziDao().read();
            List<Aspek> aspekCheck = AspekDao.getAspekDao().read();
            List<ReportAkhir> reportakhirCheck = ReportAkhirDao.getReportakhirDao().read();
//            List<PertanyaanDdst> pertanyaanCheck = PertanyaanDdstDao.getPertanyaanddstDao().read();
            //List<Tppa> tppaCheck = TppaDao.getTppaDao().read();
            isFirstRun = (anakCheck.isEmpty()|| userCheck.isEmpty() || rekomgiziCheck.isEmpty() ||
                    datagiziCheck.isEmpty() || aspekCheck.isEmpty() || reportakhirCheck.isEmpty() ||
                    hasilGiziCheck.isEmpty() || false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Log.i("ISFIRSTRUN", String.valueOf(isFirstRun));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (selectedFragment != null) {
            selectedFragment.onResume();
        }
    }

}

