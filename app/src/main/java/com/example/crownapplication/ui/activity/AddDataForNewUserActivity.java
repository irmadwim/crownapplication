package com.example.crownapplication.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.User;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.api.SyncWorker;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.BaseRespons;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.presenter.AnakPresenter;
import com.example.crownapplication.ui.presenter.UserPresenter;
import com.example.crownapplication.ui.util.PrefUtil;
import com.example.crownapplication.ui.view.UserView;
import com.google.android.material.textfield.TextInputEditText;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDataForNewUserActivity extends AppCompatActivity implements UserView {

    Calendar calendar;
    int year;
    int month;
    int date;
    private String selected_jk;
    private int id_anak, id_user;
    private AnakPresenter anakPresenter;
    private UserPresenter userPresenter;
    private User user;
    private UserOrtu userOrtu;
    private Anak anak;
    private Api mApi;
    private MaterialDialog mDialog;
    private Context ctx;

    @BindView(R.id.tedit_name_addnew)
    TextInputEditText etNama;
    @BindView(R.id.tedit_tgllahir_addnew)
    TextInputEditText etTgl;
    @BindView(R.id.sp_daftarJk_addnew)
    Spinner spJk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_data_for_new_user);

        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        initData();
        //initViews();
    }

    private void initData(){
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        id_user = getIntent().getIntExtra("id_user", 0);
        userOrtu = PrefUtil.getUser(this, PrefUtil.USER_SESSION);
        //userPresenter = new UserPresenter(getApplicationContext(), this);
        //user = userPresenter.getByID(userOrtu.getIdUser());
        //user = userPresenter.getByID(id_user);
    }

    @OnItemSelected(R.id.sp_daftarJk_addnew) void jkSelected(int position){
        selected_jk = (String) spJk.getAdapter().getItem(position);
    }

    @OnItemSelected(value = R.id.sp_daftarJk_addnew, callback = OnItemSelected.Callback.NOTHING_SELECTED)
    void onNothingSelected() {

    }

    @OnClick(R.id.tedit_tgllahir_addnew) void showTimeDialog(){
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        date = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddDataForNewUserActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        etTgl.setText(String.format("%02d/%02d/%02d", year, month+1, dayOfMonth));
                    }

                }, year, month, date);

        datePickerDialog.show();
    }


    @OnClick(R.id.btn_lanjut_addnew) void onAdd(){
        MaterialDialog dialog = DialogBuilder.showLoadingDialog(AddDataForNewUserActivity.this, "Add Data", "Please Wait", false);
        mApi.addAnak(etNama.getText().toString(),etTgl.getText().toString(),
                selected_jk, userOrtu.getIdUser())
                .enqueue(new Callback<BaseRespons>() {
                    @Override
                    public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                        BaseRespons baseRespons = response.body();
                        String message = "";
                        if (baseRespons!=null){
                            if(!baseRespons.getError()){
                                message = "Data berhasil ditambahkan";
                                //SyncWorker.getSyncWorker().syncAnak(getApplicationContext(), mApi.getAnak(), false);
                            }
                        }
                        if (response.code() == 400){
                            message = "Invalid parameter!";
                        }
                        if (response.code() == 502){
                            message = "Gagal menyimpan ke database.";
                        }
                        Toast.makeText(AddDataForNewUserActivity.this, message, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        Intent intent = new Intent(AddDataForNewUserActivity.this, AddMeasureNewUserActivity.class);
                        //intent.putExtra("id", anak.getId_anak());
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<BaseRespons> call, Throwable t) {

                    }
                });
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadUser(List<User> userList) {

    }
}
