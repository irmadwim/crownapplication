package com.example.crownapplication.ui.view;

import com.example.crownapplication.bl.db.model.Aspek;

import java.util.List;

public interface AspekView {
    void showLoading();
    void hideLoading();
    void loadAspek(List<Aspek> aspekList);
}
