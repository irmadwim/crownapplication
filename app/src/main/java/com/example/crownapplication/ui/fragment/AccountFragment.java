package com.example.crownapplication.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.helper.Db;
import com.example.crownapplication.bl.db.model.User;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.activity.EditAccountActivity;
import com.example.crownapplication.ui.activity.SignInActivity;
import com.example.crownapplication.ui.presenter.UserPresenter;
import com.example.crownapplication.ui.util.PrefUtil;
import com.example.crownapplication.ui.view.UserView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AccountFragment extends Fragment implements UserView {

    private UserPresenter userPresenter;
    private User user;
    private UserOrtu userOrtu;
    private Api mApi;

    @BindView(R.id.tv_nama_profil)
    TextView tvNama;
    @BindView(R.id.tv_email_profil) TextView tvEmail;
    @BindView(R.id.profile_picture)
    ImageView ivAvatar;

    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this,view);
        Db.getInstance().init(getActivity());
        AndroidThreeTen.init(getActivity());
        mApi = RetrofitBuilder.builder(getActivity()).create(Api.class);
        initData();
        initViews();
        return view;
    }

    private void initData() {
        userOrtu = PrefUtil.getUser(getActivity(), PrefUtil.USER_SESSION);
        userPresenter = new UserPresenter(getActivity().getApplicationContext(), this);
        user = userPresenter.getByID(userOrtu.getIdUser());
    }

    private void initViews(){
        tvNama.setText(String.valueOf(user.getNama()));
        tvEmail.setText(String.valueOf(user.getEmail()));
        ivAvatar.setImageResource(R.drawable.profile);

    }

    @OnClick(R.id.btn_edit_profile) void goEdit(){
        Intent intent = new Intent(getContext(), EditAccountActivity.class);
        intent.putExtra("id", userOrtu.getIdUser());
        startActivity (intent);
        //startActivity(new Intent(getContext(), EditProfileActivity.class));
    }


    @OnClick(R.id.btn_logout) void doLogout(){
        PrefUtil.clear(getActivity());
        startActivity(new Intent(getContext(), SignInActivity.class));
        getActivity().finish();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadUser(List<User> userList) {

    }
}
