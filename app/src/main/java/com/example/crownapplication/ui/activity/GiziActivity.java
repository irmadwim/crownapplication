package com.example.crownapplication.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.DataGizi;
import com.example.crownapplication.bl.db.model.HasilGizi;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.ui.adapter.DataGiziAdapter;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.presenter.AnakPresenter;
import com.example.crownapplication.ui.presenter.DataGiziPresenter;
import com.example.crownapplication.ui.presenter.HasilGiziPresenter;
import com.example.crownapplication.ui.presenter.UserPresenter;
import com.example.crownapplication.ui.util.PrefUtil;
import com.example.crownapplication.ui.view.AnakView;
import com.example.crownapplication.ui.view.DataGiziView;
import com.example.crownapplication.ui.view.HasilGiziView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GiziActivity extends AppCompatActivity implements AnakView, DataGiziView, HasilGiziView {

    private Api mApi;
    private UserPresenter userPresenter;
    private AnakPresenter anakPresenter;
    private int id_anak;
    private String statusTinggi, statusBerat, statusLk, statusImt;
    private Dialog dialog;
    private Anak anak;
    private DataGiziPresenter dataGiziPresenter;
    private DataGizi dataGizi;
    private HasilGiziPresenter hasilGiziPresenter;
    private HasilGizi hasilGizi;
    private MaterialDialog mDialog;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_usia_gizi) TextView tvUsia;
    @BindView(R.id.tv_namaanak_gizi) TextView tvNama;
    @BindView(R.id.tv_ketberat_gizi)
    TextView tvBb;
    @BindView(R.id.tv_kettinggi_gizi)
    TextView tvTb;
    @BindView(R.id.tv_ketlk_gizi)
    TextView tvLk;
    @BindView(R.id.tv_ketimt_gizi)
    TextView tvImt;
    @BindView(R.id.tv_hasilberat_gizi)
    TextView tvKetBb;
    @BindView(R.id.tv_hasiltinggi_gizi)
    TextView tvKetTb;
    @BindView(R.id.tv_hasillk_gizi)
    TextView tvKetLk;
    @BindView(R.id.tv_hasilimt_gizi)
    TextView tvKetImt;
    @BindView(R.id.im_anak_gizi)
    ImageView ivAvatar;
    @BindView(R.id.tv_protein)
    TextView tvProtein;
    @BindView(R.id.tv_lemak)
    TextView tvLemak;
    @BindView(R.id.tv_karbohidrat)
    TextView tvKarbohidrat;
    @BindView(R.id.tv_energi)
    TextView tvEnergi;
    @BindView(R.id.tv_air)
    TextView tvAir;
    @BindView(R.id.tv_serat)
    TextView tvSerat;
    @BindView(R.id.tv_vita)
    TextView tvVitA;
    @BindView(R.id.tv_vitd)
    TextView tvVitD;
    @BindView(R.id.tv_vite)
    TextView tvVitE;
    @BindView(R.id.tv_vitk)
    TextView tvVitK;
    @BindView(R.id.tv_kalsium)
    TextView tvKalsium;
    @BindView(R.id.tv_kalium)
    TextView tvKalium;
    @BindView(R.id.tv_fosfor)
    TextView tvFosfor;
    @BindView(R.id.tv_besi)
    TextView tvBesi;
    @BindView(R.id.tv_magnesium)
    TextView tvMagnesium;
    @BindView(R.id.tv_natrium)
    TextView tvNatrium;
    @BindView(R.id.tv_keterangan_gizi)
    TextView tvKet;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gizi);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        initData();
        initViews();
    }

    private void initData(){
        id_anak = getIntent().getIntExtra("id", 0);
        anakPresenter = new AnakPresenter(getApplicationContext(), this);
        anak = anakPresenter.getByID(id_anak);

        dataGiziPresenter = new DataGiziPresenter(getApplicationContext(), this);
        dataGizi = dataGiziPresenter.getLastIdByAnak(anak.getId_anak());

        hasilGiziPresenter = new HasilGiziPresenter(getApplicationContext(), this);
        hasilGizi = hasilGiziPresenter.getLastIdByGizi(dataGizi.getId_gizi());
    }

    private void initViews() {
        toolbar.setTitle("Rekomendasi Gizi");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvNama.setText(String.valueOf(anak.getNama_anak()));

        if (anak.getUsia() >= 12)
            tvUsia.setText(anak.getUsia() / 12 + " tahun " + anak.getUsia() % 12 + " bulan");
        else
            tvUsia.setText(anak.getUsia() + " bulan");

        // Set Avatar sesuai jenis kelamin dan umur
        if (anak.getGender_anak().equals("L")) {
            ivAvatar.setImageResource(R.drawable.ic_baby_boy);
        } else {
            ivAvatar.setImageResource(R.drawable.ic_baby_girl);

        }

        tvTb.setText(String.valueOf(dataGizi.getTb()));
        tvBb.setText(String.valueOf(dataGizi.getBb()));
        tvLk.setText(String.valueOf(dataGizi.getLk()));
        tvImt.setText(String.valueOf(dataGizi.getImt()));

        tvKetTb.setText(String.valueOf(hasilGizi.getTb_hasil()));
        tvKetBb.setText(String.valueOf(hasilGizi.getBb_hasil()));
        tvKetLk.setText(String.valueOf(hasilGizi.getLk_hasil()));
        tvKetImt.setText(String.valueOf(hasilGizi.getImt_hasil()));

        if (hasilGizi.getTb_hasil().equals("Sangat Pendek")) {
            tvKetTb.setTextColor(Color.RED);
        } else if (hasilGizi.getTb_hasil().equals("Pendek")) {
            tvKetTb.setTextColor(Color.parseColor("#ff7c17"));
        } else if (hasilGizi.getTb_hasil().equals("Normal")) {
            tvKetTb.setTextColor(Color.GREEN);
        } else {
            tvKetTb.setTextColor(Color.parseColor("#ff7c17"));
        }

        if (hasilGizi.getBb_hasil().equals("Gizi Buruk")) {
            tvKetBb.setTextColor(Color.RED);
        } else if (hasilGizi.getBb_hasil().equals("Gizi Kurang")) {
            tvKetBb.setTextColor(Color.parseColor("#ff7c17"));
        } else if (hasilGizi.getBb_hasil().equals("Gizi Baik")) {
            tvKetBb.setTextColor(Color.GREEN);
        } else {
            tvKetBb.setTextColor(Color.parseColor("#ff7c17"));
        }

        if (hasilGizi.getLk_hasil().equals("Mikrosefalus")) {
            tvKetLk.setTextColor(Color.RED);
        } else if (hasilGizi.getLk_hasil().equals("Normal")) {
            tvKetLk.setTextColor(Color.GREEN);
        } else {
            tvKetLk.setTextColor(Color.RED);
        }

        if (hasilGizi.getImt_hasil().equals("Sangat Kurus")) {
            tvKetImt.setTextColor(Color.RED);
        } else if (hasilGizi.getImt_hasil().equals("Kurus")) {
            tvKetImt.setTextColor(Color.parseColor("#ff7c17"));
        } else if (hasilGizi.getImt_hasil().equals("Normal")) {
            tvKetImt.setTextColor(Color.GREEN);
        }else if (hasilGizi.getImt_hasil().equals("Gemuk")){
            tvKetImt.setTextColor(Color.parseColor("#ff7c17"));
        } else {
            tvKetImt.setTextColor(Color.RED);
        }

        if (anak.getUsia() <= 6) {
            tvEnergi.setText("Energi : 550 kkal");
            tvProtein.setText("Protein : 12 gram");
            tvKarbohidrat.setText("Karbohidrat : 58 gram");
            tvLemak.setText("Lemak : 34 gram");
            tvAir.setVisibility(View.GONE);
            tvSerat.setVisibility(View.GONE);
            tvVitA.setText("Vitamin A : 375 mcg");
            tvVitD.setText("Vitamin D : 5 mcg");
            tvVitE.setText("Vitamin E : 4 mg");
            tvVitK.setText("Vitamin K : 5 mcg");
            tvKalsium.setText("Kalsium : 200 mg");
            tvFosfor.setText("Fosfor : 100 mg");
            tvMagnesium.setText("Magnesium : 30 mg");
            tvNatrium.setText("Natrium : 120 mg");
            tvKalium.setText("Kalium : 500 mg");
            tvBesi.setVisibility(View.GONE);
        } else if (anak.getUsia() <= 11) {
            tvEnergi.setText("Energi : 725 kkal");
            tvProtein.setText("Protein : 18 gram");
            tvKarbohidrat.setText("Karbohidrat : 82 gram");
            tvLemak.setText("Lemak : 36 gram");
            tvAir.setText("Air : 800 ml");
            tvSerat.setText("Serat : 10 gram");
            tvVitA.setText("Vitamin A : 400 mcg");
            tvVitD.setText("Vitamin D : 5 mcg");
            tvVitE.setText("Vitamin E : 5 mg");
            tvVitK.setText("Vitamin K : 10 mcg");
            tvKalsium.setText("Kalsium : 250 mg");
            tvFosfor.setText("Fosfor : 250 mg");
            tvMagnesium.setText("Magnesium : 55 mg");
            tvNatrium.setText("Natrium : 200 mg");
            tvKalium.setText("Kalium : 700 mg");
            tvBesi.setText("Zat Besi : 7 mg");
        } else if (anak.getUsia() <= 36) {
            tvEnergi.setText("Energi : 1125 kkal");
            tvProtein.setText("Protein : 26 gram");
            tvKarbohidrat.setText("Karbohidrat : 155 gram");
            tvLemak.setText("Lemak : 44 gram");
            tvAir.setText("Air : 1200 ml");
            tvSerat.setText("Serat : 16 gram");
            tvVitA.setText("Vitamin A : 400 mcg");
            tvVitD.setText("Vitamin D : 15 mcg");
            tvVitE.setText("Vitamin E : 6 mg");
            tvVitK.setText("Vitamin K : 15 mcg");
            tvKalsium.setText("Kalsium : 650 mg");
            tvFosfor.setText("Fosfor : 500 mg");
            tvMagnesium.setText("Magnesium : 60 mg");
            tvNatrium.setText("Natrium : 1000 mg");
            tvKalium.setText("Kalium : 3000 mg");
            tvBesi.setText("Zat Besi : 8 mg");
        } else {
            tvEnergi.setText("Energi : 1600 kkal");
            tvProtein.setText("Protein : 35 gram");
            tvKarbohidrat.setText("Karbohidrat : 220 gram");
            tvLemak.setText("Lemak : 62 gram");
            tvAir.setText("Air : 1500 ml");
            tvSerat.setText("Serat : 22 gram");
            tvVitA.setText("Vitamin A : 375 mcg");
            tvVitD.setText("Vitamin D : 15 mcg");
            tvVitE.setText("Vitamin E : 7 mg");
            tvVitK.setText("Vitamin K : 20 mcg");
            tvKalsium.setText("Kalsium : 1000 mg");
            tvFosfor.setText("Fosfor : 500 mg");
            tvMagnesium.setText("Magnesium : 95 mg");
            tvNatrium.setText("Natrium : 1200 mg");
            tvKalium.setText("Kalium : 3800 mg");
            tvBesi.setText("Zat Besi : 9 mg");
        }

        if (hasilGizi.getImt_hasil().equals("Sangat Kurus") || hasilGizi.getImt_hasil().equals("Kurus")) {
            if (anak.getUsia() <= 6){
                tvKet.setText("Berilah ASI secara rutin setiap 2 jam sekali, apabila tidak ada " +
                        "perubahan segera periksakan ke dokter");
            } else if (anak.getUsia() <= 11){
                tvKet.setText("Berilah ASI secara rutin, berikan makanan dengan porsi kecil namun dengan " +
                        "frekuensi sering, tingkatkan pemberian cairan, energi, serta protein secara bertahap, apabila " +
                        "keadaan tidak segera membaik segera periksakan ke dokter");
            } else if (anak.getUsia() <= 36){
                tvKet.setText("Tingkatkan pemberian energi dan protein secara bertahap, berikan buah - buahan dan sayur " +
                        "setidaknya 5 porsi sehari serta berikan makanan dengan porsi kecil dengan frekuensi sering, bila" +
                        "keadaan tidak membaik segera periksakan ke dokter");
            } else {
                tvKet.setText("Tingkatkan pemberian energi dan protein secara bertahap, berikan buah - buahan dan sayur " +
                        "setidaknya 5 porsi sehari serta berikan makanan dengan porsi kecil dengan frekuensi sering, bila" +
                        "keadaan tidak membaik segera periksakan ke dokter");
            }
        }else if (hasilGizi.getImt_hasil().equals("Gemuk") || hasilGizi.getImt_hasil().equals("Obesitas")){
            if (anak.getUsia() <= 6){
                tvKet.setText("Asupan kalori tidak perlu dikurangi, tetap berikan ASI secara rutin " +
                        "apabila tidak ada perubahan segera periksakan ke dokter");
            } else if (anak.getUsia() <= 11){
                tvKet.setText("Asupan kalori tidak perlu dikurangi, tetap berikan ASI secara rutin " +
                        "apabila tidak ada perubahan segera periksakan ke dokter");
            } else if (anak.getUsia() <= 36){
                tvKet.setText("Pola makan harus terjadwal, waktu makan tidak boleh lebih dari 30 menit " +
                        "dalam satu sesi dengan komposisi makanan karbohidrat 50-60%, lemak 30%, protein 15-20%");
            } else {
                tvKet.setText("Memangkas kalori sebesar 200-300 kkal dari asupan kalori harian sampai " +
                        "sesuai dengan kebutuhan, kurangi makanan tinggi lemak, tinggi gula, dan kalori seperti kue, roti," +
                        "permen, dsb");
            }
        } else {
            if (anak.getUsia() <= 12){
                tvKet.setText("Status gizi anak Anda NORMAL, berikan gizi sesuai dengan takaran gizi yang " +
                        "tercantum di atas, dan berikan ASI secara rutin");
            } else {
                tvKet.setText("Status gizi anak Anda NORMAL, berikan gizi sesuai dengan takaran gizi yang " +
                        "tercantum di atas");
            }

        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void showLoading() {
        mDialog = DialogBuilder.showLoadingDialog(getApplicationContext(), "Updating Data", "Please Wait", false);

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadHasilGizi(List<HasilGizi> hasilGiziList) {

    }

    @Override
    public void loadDataGizi(List<DataGizi> dataGiziList) {

    }

    @Override
    public void loadGizi(DataGizi dataGizi) {

    }

    @Override
    public void loadAnak(List<Anak> anakList) {

    }
}
