package com.example.crownapplication.ui.view;

import com.example.crownapplication.bl.db.model.Tb;

import java.util.List;

public interface TbView {
    void showLoading();
    void hideLoading();
    void loadTb(List<Tb> tbList);
}
