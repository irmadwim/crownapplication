package com.example.crownapplication.ui.presenter;

import android.content.Context;

import com.example.crownapplication.bl.db.dao.HasilGiziDao;
import com.example.crownapplication.bl.db.model.HasilGizi;
import com.example.crownapplication.ui.view.HasilGiziView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HasilGiziPresenter {

    private HasilGiziView hasilGiziView;

    public HasilGiziPresenter(Context context, HasilGiziView hasilGiziView) {
        this.hasilGiziView = hasilGiziView;
        AndroidThreeTen.init(context);
    }

    public void load(){
        hasilGiziView.showLoading();
        List<HasilGizi> list = new ArrayList<>();
        try {
            list = HasilGiziDao.getHasilGiziDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        hasilGiziView.loadHasilGizi(list);
        hasilGiziView.hideLoading();
    }


    public List<HasilGizi> getByDataGizi(int gizi){
        List<HasilGizi> list = new ArrayList<>();
        try {
            list = HasilGiziDao.getHasilGiziDao().getByDataGizi(gizi);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public HasilGizi getByID(int id){
        HasilGizi hasilGizi = null;
        try {
            hasilGizi = HasilGiziDao.getHasilGiziDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hasilGizi;
    }

    public HasilGizi getLastIdByGizi(int gizi){
        HasilGizi hasilGizi = null;
        try {
            hasilGizi = HasilGiziDao.getHasilGiziDao().getLastIdByGizi(gizi);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hasilGizi;
    }

}
