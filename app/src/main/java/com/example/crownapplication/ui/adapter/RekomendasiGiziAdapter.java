package com.example.crownapplication.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.RekomendasiGizi;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RekomendasiGiziAdapter extends RecyclerView.Adapter<RekomendasiGiziAdapter.ViewHolder> {
    Context context;
    List<RekomendasiGizi> list = new ArrayList<>();

    public RekomendasiGiziAdapter(Context context) {
        this.context = context;
        AndroidThreeTen.init(context);
    }

    public RekomendasiGiziAdapter(Context context, List<RekomendasiGizi> list) {
        this.context = context;
        this.list = list;
        AndroidThreeTen.init(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rekomgizi, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RekomendasiGizi rekomendasiGizi = list.get(position);

        holder.id = rekomendasiGizi.getId_rekom_gizi();
        holder.tvDeskripsi.setText(rekomendasiGizi.getDeskripsi());

        if (rekomendasiGizi.getUsia() <= 5) {
            holder.ivAvatar.setImageResource(R.drawable.asi);
        } else if (rekomendasiGizi.getUsia() <= 8) {
            holder.ivAvatar.setImageResource(R.drawable.mpasi7_8bln);
        } else if (rekomendasiGizi.getUsia() <= 12) {
            holder.ivAvatar.setImageResource(R.drawable.mpasi9_12bln);
        } else if (rekomendasiGizi.getUsia() <= 23) {
            holder.ivAvatar.setImageResource(R.drawable.mpasi13_23bln);
        } else if (rekomendasiGizi.getUsia() <= 36) {
            holder.ivAvatar.setImageResource(R.drawable.mpasi24_60bln);
        }else if (rekomendasiGizi.getUsia() <= 60) {
            holder.ivAvatar.setImageResource(R.drawable.mpasi3_6tahun);
        }

        if (rekomendasiGizi.getUsia() <= 5) {
            holder.tvKeterangan.setText("Berikan ASI yang pertama keluar sesaat setelah lahir yang berwarna " +
                    "kekuningan (kolostrum)");
        } else if (rekomendasiGizi.getUsia() <= 8) {
            holder.tvKeterangan.setText("Memasuki waktu MPASI, teruskan pemberian ASI sesering mungkin, selain itu " +
                    "berikan makanan selingan 1-2 kali sehari berupa buah atau biskuit");
        } else if (rekomendasiGizi.getUsia() <= 12) {
            holder.tvKeterangan.setText("Teruskan pemberian ASI, lalu berikan makanan selingan yang dapat dipegang " +
                    "anak, diberikan di antara waktu makan lengkap");
        } else if (rekomendasiGizi.getUsia() <= 23) {
            holder.tvKeterangan.setText("Teruskan pemberian ASI, berikan makanan selingan 1-2 kali sehari (buah, biskuit)");
        } else if (rekomendasiGizi.getUsia() <= 36) {
            holder.tvKeterangan.setText("Beri makanan selingan 2 kali sehari, jangan berikan makanan manis sebelum waktu makan, " +
                    "sebab dapat mengurangi nafsu makan anak");
        }else if (rekomendasiGizi.getUsia() <= 60) {
            holder.tvKeterangan.setText("Beri makanan selingan 2 kali sehari, jangan berikan makanan manis sebelum waktu makan, " +
                    "sebab dapat mengurangi nafsu makan anak");
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvDeskripsi)
        TextView tvDeskripsi;
        @BindView(R.id.tvKeterangan)
        TextView tvKeterangan;
        @BindView(R.id.ivRekomGizi)
        ImageView ivAvatar;
        int id;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


    public void generate(List<RekomendasiGizi> list) {
        clear();
        this.list = list;
        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }
}