package com.example.crownapplication.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.DataGizi;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DataGiziAdapter extends RecyclerView.Adapter<DataGiziAdapter.ViewHolder> {

    Context context;
    List<DataGizi> list = new ArrayList<>();

    public DataGiziAdapter(Context context) {
        this.context = context;
        AndroidThreeTen.init(context);
    }

    public DataGiziAdapter(Context context, List<DataGizi> list) {
        this.context = context;
        this.list = list;
        AndroidThreeTen.init(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_datagizi, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DataGizi dataGizi = list.get(position);
        holder.id = dataGizi.getId_gizi();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        holder.tvTgl.setText(sdf.format(dataGizi.getTanggal()));
        holder.tvBb.setText(String.valueOf(dataGizi.getBb()));
        holder.tvTb.setText(String.valueOf(dataGizi.getTb()));
        holder.tvLk.setText(String.valueOf(dataGizi.getLk()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tv_date)
        TextView tvTgl;
        @BindView(R.id.tv_weight)
        TextView tvBb;
        @BindView(R.id.tv_height)
        TextView tvTb;
        @BindView(R.id.tv_head)
        TextView tvLk;
        int id;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public void generate(List<DataGizi> list) {
        clear();
        this.list = list;
        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }

}
