package com.example.crownapplication.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.ui.fragment.ChildChartFragment;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DialogListAdapter extends RecyclerView.Adapter<DialogListAdapter.ViewHolder> {

    Context context;
    List<Anak> list = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private ChildChartFragment childChartFragment;

    public DialogListAdapter(Context context, int item_dialog, List<Anak> anakList) {
        this.context = context;
        this.list = anakList;
        AndroidThreeTen.init(context);
    }

    public DialogListAdapter(Context context, List<Anak> list) {
        this.context = context;
        this.list = list;
        AndroidThreeTen.init(context);
    }

    private OnItemClickListener listener;
    // Define the listener interface

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    @NonNull
    @Override
    public DialogListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_dialog, parent,false);
        return new DialogListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DialogListAdapter.ViewHolder holder, int position) {
        Anak anak = list.get(position);
        holder.id = anak.getId_anak();
        holder.tvNama.setText(anak.getNama_anak());
        holder.tvUsia.setText(anak.getUsia() + " bulan");
        holder.position = position;

        if (anak.getGender_anak().equals("L")){
            holder.ivAvatar.setImageResource(R.drawable.ic_baby_boy);
        }else {
            holder.ivAvatar.setImageResource(R.drawable.ic_baby_girl);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.tv_nama_anak_list)
        TextView tvNama;
        @BindView(R.id.avatar_anak_list)
        ImageView ivAvatar;
        @BindView(R.id.tv_usia_anak_list)
        TextView tvUsia;
        @BindView(R.id.cv_anak_list)
        CardView cvAnak;
        int id;
        int position;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(itemView, position);
                        }
                    }
                }
            });

        }

        @Override
        public void onClick(View v) {

        }
//        @OnClick(R.id.cv_anak_drawer) void onClick(){
//            ChildChartFragment.textView.setText("You have selected : "+list[getAdapterPosition()]);
//            ChildChartFragment.dialog.dismiss();
//            Intent intent = new Intent(context, ChildChartFragment.class);
//            intent.putExtra("id", id);
//            context.startActivity(intent);
//        }
    }

    public void generate(List<Anak> list) {
        clear();
        this.list = list;
        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }
}
