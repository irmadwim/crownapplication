package com.example.crownapplication.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.DataGizi;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.adapter.DataGiziAdapter;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.presenter.AnakPresenter;
import com.example.crownapplication.ui.presenter.DataGiziPresenter;
import com.example.crownapplication.ui.util.PrefUtil;
import com.example.crownapplication.ui.view.AnakView;
import com.example.crownapplication.ui.view.DataGiziView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailChildActivity extends AppCompatActivity implements AnakView, DataGiziView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_usia_detail) TextView tvUsia;
    @BindView(R.id.tv_nama_detail) TextView tvNama;
    @BindView(R.id.rv_detailchild)
    RecyclerView rvDetail;
    @BindView(R.id.im_detail_anak)
    ImageView ivAvatar;

    private List<DataGizi> giziList = new ArrayList<>();
    private int id_anak;
    private AnakPresenter anakPresenter;
    private Anak anak;
    private DataGiziPresenter dataGiziPresenter;
    private DataGizi dataGizi;
    private DataGiziAdapter dataGiziAdapter;
    private MaterialDialog mDialog;
    private Api mApi;
    private UserOrtu userOrtu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_child);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        userOrtu = PrefUtil.getUser(this, PrefUtil.USER_SESSION);
        initData();
        initViews();
    }

    private void initData(){
        id_anak = getIntent().getIntExtra("id", 0);
        anakPresenter = new AnakPresenter(getApplicationContext(), this);
        anak = anakPresenter.getByID(id_anak);

        dataGiziPresenter = new DataGiziPresenter(getApplicationContext(), this);
        giziList = dataGiziPresenter.getByAnak(id_anak);
        dataGiziAdapter =  new DataGiziAdapter(getApplicationContext(), giziList);

        rvDetail.setHasFixedSize(true);
        rvDetail.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    private void initViews(){
        toolbar.setTitle("Detail Anak");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvNama.setText(String.valueOf(anak.getNama_anak()));

        if (anak.getUsia() >= 12)
            tvUsia.setText(anak.getUsia()/12 + " tahun " + anak.getUsia()%12 + " bulan");
        else
            tvUsia.setText(anak.getUsia() + " bulan");

        // Set Avatar sesuai jenis kelamin dan umur
        if (anak.getGender_anak().equals("L")){
            ivAvatar.setImageResource(R.drawable.ic_baby_boy);
        }else {
            ivAvatar.setImageResource(R.drawable.ic_baby_girl);

        }
    }

    @OnClick(R.id.fab_tambahdetail_btn) void goAddMeasure(){
        Intent intent = new Intent(DetailChildActivity.this, AddMeasurementActivity.class);
        intent.putExtra("id", anak.getId_anak());
        startActivity (intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadDataGizi(dataGiziPresenter.getByAnak(anak.getId_anak()));
    }


    @Override
    public void showLoading() {
        mDialog = DialogBuilder.showLoadingDialog(getApplicationContext(), "Updating Data", "Please Wait", false);

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadDataGizi(List<DataGizi> dataGiziList) {
        dataGiziAdapter.generate(dataGiziList);
        dataGiziAdapter.notifyDataSetChanged();
        rvDetail.setAdapter(dataGiziAdapter);

    }

    @Override
    public void loadGizi(DataGizi dataGizi) {

    }

    @Override
    public void loadAnak(List<Anak> anakList) {

    }
}
