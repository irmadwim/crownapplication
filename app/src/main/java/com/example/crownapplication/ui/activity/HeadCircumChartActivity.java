package com.example.crownapplication.ui.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.DataGizi;
import com.example.crownapplication.bl.db.model.User;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.DataBbs;
import com.example.crownapplication.bl.network.model.DataLks;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.presenter.AnakPresenter;
import com.example.crownapplication.ui.presenter.BbPresenter;
import com.example.crownapplication.ui.presenter.DataGiziPresenter;
import com.example.crownapplication.ui.view.AnakView;
import com.example.crownapplication.ui.view.DataGiziView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HeadCircumChartActivity extends AppCompatActivity implements DataGiziView, AnakView {

    private int id_anak;
    private BbPresenter bbPresenter;
    private DataGiziPresenter dataGiziPresenter;
    private AnakPresenter anakPresenter;
    private User user;
    private UserOrtu userOrtu;
    private Anak anak;
    private DataGizi dataGizi;
    private Api mApi;
    private MaterialDialog mDialog;
    private Context context;

    @BindView(R.id.view_chart_head)
    LineChart chartKepala;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private List<DataGizi> dataGiziList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_head_circum_chart);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        initData();
        getDataLk();
        initViews();
    }

    private void initData() {

        id_anak = getIntent().getIntExtra("id", 0);
        anakPresenter = new AnakPresenter(getApplicationContext(), this);
        anak = anakPresenter.getByID(id_anak);

        dataGiziPresenter = new DataGiziPresenter(getApplicationContext(), this);
        //dataGizi = dataGiziPresenter.getLastIdByAnak(anak.getId_anak());

        //anakList = anakPresenter.getByOrtu(userOrtu.getIdUser());
        dataGiziList = dataGiziPresenter.getByAnak(id_anak);

        //bbList = bbPresenter.getByJk(anak.getGender_anak());

    }

    private void getDataLk(){
        mApi.getDataLk(anak.getGender_anak())
                .enqueue(new Callback<List<DataLks>>() {
                    @Override
                    public void onResponse(Call<List<DataLks>> call, Response<List<DataLks>> response) {
                        if (response.isSuccessful()){
                            List<DataLks> dataLks = response.body();
                            Log.i("DATALKCHART_GET", response.message());
                            setLineChartLk(response.body(), dataGiziList);

                        }
                    }

                    @Override
                    public void onFailure(Call<List<DataLks>> call, Throwable t) {
                        DialogBuilder.showErrorDialog(context, t.getMessage());
                        Log.i("DATALKCHART_GET", t.getMessage());
                    }
                });


    }

    private void setLineChartLk(List<DataLks> dataLks, List<DataGizi> dataGiziLk){

        chartKepala.getLegend().setWordWrapEnabled(true);
        chartKepala.getLegend().setEnabled(true);
        chartKepala.setDrawGridBackground(false);
        chartKepala.getDescription().setEnabled(false);
        chartKepala.setDrawBorders(false);

        chartKepala.getAxisLeft().setEnabled(false);
        chartKepala.getAxisRight().setDrawAxisLine(false);
        chartKepala.getAxisRight().setDrawGridLines(false);
        chartKepala.getXAxis().setDrawAxisLine(false);
        chartKepala.getXAxis().setDrawGridLines(false);

        // enable touch gestures
        chartKepala.setTouchEnabled(true);

        // enable scaling and dragging
        chartKepala.setDragEnabled(true);
        chartKepala.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        chartKepala.setPinchZoom(false);

        ArrayList<String> xaxis = new ArrayList<>();
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        //ArrayList<ILineDataSet> dataSet2s = new ArrayList<>();

        dataSets.add(makeLineLk(dataLks,"m3sd",Color.BLUE));
        dataSets.add(makeLineLk(dataLks,"m2sd",Color.RED));
        dataSets.add(makeLineLk(dataLks,"m1sd",Color.YELLOW));
        dataSets.add(makeLineLk(dataLks,"medium",Color.GREEN));
        dataSets.add(makeLineLk(dataLks,"p1sd",Color.YELLOW));
        dataSets.add(makeLineLk(dataLks,"p2sd",Color.RED));
        dataSets.add(makeLineLk(dataLks,"p3sd",Color.BLUE));

        dataSets.add(makeLineLkGizi(dataGiziLk,"lk_anak",Color.BLACK));

        chartKepala.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        chartKepala.getXAxis().setGranularity(1f);
        chartKepala.getAxisLeft().setGranularity(1f);

        LineData datas = new LineData(dataSets);
        //LineData datas2 = new LineData(dataSet2s);
        chartKepala.setData(datas);
        //chartKepala.setData(datas2);
        chartKepala.invalidate();

    }

    private ILineDataSet makeLineLk(List<DataLks> dataLks, String s, Integer color) {
        ArrayList<Entry> values = new ArrayList<>();
        for (int j=0;j<dataLks.size();j++) {
            if (s.equals("m3sd")) {
                values.add(new Entry((float) dataLks.get(j).getUsia(), (float) dataLks.get(j).getM3sd()));
            } else if (s.equals("m2sd")) {
                values.add(new Entry((float) dataLks.get(j).getUsia(), (float) dataLks.get(j).getM2sd()));
            } else if (s.equals("m1sd")) {
                values.add(new Entry((float) dataLks.get(j).getUsia(), (float) dataLks.get(j).getM1sd()));
            } else if (s.equals("medium")) {
                values.add(new Entry((float) dataLks.get(j).getUsia(), (float) dataLks.get(j).getMedium()));
            } else if (s.equals("p1sd")) {
                values.add(new Entry((float) dataLks.get(j).getUsia(), (float) dataLks.get(j).getP1sd()));
            } else if (s.equals("p2sd")) {
                values.add(new Entry((float) dataLks.get(j).getUsia(), (float) dataLks.get(j).getP2sd()));
            } else if (s.equals("p3sd")) {
                values.add(new Entry((float) dataLks.get(j).getUsia(), (float) dataLks.get(j).getP3sd()));
            }
        }

        LineDataSet lineDataSet = new LineDataSet(values, s);
//        lineDataSet.setDrawValues(false);
        lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);

        // setting tampilan
        lineDataSet.setHighlightEnabled(true);
        lineDataSet.setLineWidth(1.5f);
//        lineDataSet.setCircleRadius(2f);
//            lineDataSet.setGradientColor(getGradientBackground().get(i).getStartColor(), getGradientBackground().get(i).getEndColor());
//        lineDataSet.setCircleColor(color);
        lineDataSet.setColor(color);
        lineDataSet.setDrawCircles(false);
//            lineDataSet.setDrawHighlightIndicators(true);
//            lineDataSet.setHighLightColor(Color.RED);

        return lineDataSet;
    }

    private ILineDataSet makeLineLkGizi(List<DataGizi> dataLk, String s, Integer color) {
        ArrayList<Entry> values2 = new ArrayList<>();
        for (int j=0;j<dataLk.size();j++) {
            if (s.equals("lk_anak")) {
                values2.add(new Entry((int) anak.getUsia(), (float) dataLk.get(j).getLk()));
            }
        }

        LineDataSet lineDataSet2 = new LineDataSet(values2, s);
//        lineDataSet.setDrawValues(false);
        lineDataSet2.setAxisDependency(YAxis.AxisDependency.LEFT);

        // setting tampilan
        lineDataSet2.setHighlightEnabled(true);
        lineDataSet2.setLineWidth(1.5f);
        lineDataSet2.setCircleRadius(5f);
//            lineDataSet.setGradientColor(getGradientBackground().get(i).getStartColor(), getGradientBackground().get(i).getEndColor());
        lineDataSet2.setCircleColor(color);
        lineDataSet2.setColor(color);
        lineDataSet2.setDrawCircles(true);
//            lineDataSet.setDrawHighlightIndicators(true);
//            lineDataSet.setHighLightColor(Color.RED);

        return lineDataSet2;
    }


    private void initViews(){
        toolbar.setTitle("Chart Lingkar Kepala");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void loadAnak(List<Anak> anakList) {

    }

    @Override
    public void showLoading() {
        mDialog = DialogBuilder.showLoadingDialog(getApplicationContext(), "Updating Data", "Please Wait", false);

    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();

    }

    @Override
    public void loadDataGizi(List<DataGizi> dataGiziList) {

    }

    @Override
    public void loadGizi(DataGizi dataGizi) {

    }
}
