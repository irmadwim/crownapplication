package com.example.crownapplication.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.BaseRespons;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.util.EmailValidator;
import com.example.crownapplication.ui.util.PasswordValidator;
import com.example.crownapplication.ui.util.PrefUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.jakewharton.threetenabp.AndroidThreeTen;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.crownapplication.ui.util.Util.isEempty;

public class SignUpActivity extends AppCompatActivity {

    @BindView(R.id.input_email_signup)
    TextInputEditText etEmail;
    @BindView(R.id.input_name_signup)
    TextInputEditText etNama;
    @BindView(R.id.text_login)
    TextView tvLogin;
    @BindView(R.id.input_password_signup)
    TextInputEditText etPassword;

    EmailValidator emailValidator;
    PasswordValidator passwordValidator;
    Context context;

    private String email;
    private String password;
    private String nama;
    private Api mApi;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isSessionLogin()){
            startActivity(new Intent(this, MainActivity.class));
            this.finish();
        }

        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
    }

    boolean isEmail(EditText text){
        emailValidator = new EmailValidator();
        String email = text.getText().toString();
        return emailValidator.isValid(email);
    }

    boolean isPassword(EditText text){
        passwordValidator = new PasswordValidator();
        String pass = text.getText().toString();
        return passwordValidator.isValid(pass);
    }

    @OnClick(R.id.text_login) void toLogin(){
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity (intent);
    }
    
    @OnClick(R.id.btn_signup) void onSignup(){
        if(isEempty(etNama)) {
            etNama.setError("Nama harus diisi");
        }else if(isEempty(etEmail)){
            etEmail.setError("Email harus diisi");
        }else if(isEempty(etPassword)){
            etPassword.setError("Password harus diisi");
        }else if(!isEmail(etEmail)){
            etEmail.setError("Email tidak valid");
        }else if(!isPassword(etPassword)){
            String str = passwordValidator.getString();
            Toast.makeText(SignUpActivity.this,str, Toast.LENGTH_SHORT).show();
        }else {
            signupAct();
        }
    }

    private void signupAct() {
        nama = etNama.getText().toString();
        email = etEmail.getText().toString();
        password = etPassword.getText().toString();
        mApi.registerUser(nama, email, password)
                .enqueue(new Callback<UserOrtu>() {
                    @Override
                    public void onResponse(Call<UserOrtu> call, Response<UserOrtu> response) {
                        UserOrtu user = response.body();
                        signUp();
                    }

                    @Override
                    public void onFailure(Call<UserOrtu> call, Throwable t) {

                    }
                });
    }

    private void signUp(){
        final MaterialDialog dialog = DialogBuilder.showLoadingDialog(SignUpActivity.this, "Updating Data", "Please wait..", false);
        mApi.loginUser(email, password).enqueue(new Callback<UserOrtu>() {
            @Override
            public void onResponse(Call<UserOrtu> call, Response<UserOrtu> response) {
                UserOrtu user = response.body();
                Log.i("USER_REGISTER", response.message());
                if (user != null){
                    //Masih error disini
                    //if (!user.getError()){
                    PrefUtil.putUser(getApplicationContext(), PrefUtil.USER_SESSION, user);
                    Intent intent = new Intent(SignUpActivity.this, AddDataForNewUserActivity.class);
                    startActivity(intent);
                    //this.finish();

                    //}
                    Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (response.code() == 403){
                    etPassword.requestFocus();
                    etPassword.setError(getString(R.string.error_password));
                }
                if (response.code() == 404){
                    etEmail.requestFocus();
                    etEmail.setError(getString(R.string.error_login));
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<UserOrtu> call, Throwable t) {
                //Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                Log.i("USER_REGISTER", t.getMessage());
                DialogBuilder.showErrorDialog(getApplicationContext(), "Gagal Login");
            }
        });
    }

    // this method to check is user logged in ?
    boolean isSessionLogin(){
        return PrefUtil.getUser(getApplicationContext(), PrefUtil.USER_SESSION) != null;
    }
}
