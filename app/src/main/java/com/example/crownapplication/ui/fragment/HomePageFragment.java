package com.example.crownapplication.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.helper.Db;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.User;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.activity.AddChildActivity;
import com.example.crownapplication.ui.adapter.AnakAdapter;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.presenter.AnakPresenter;
import com.example.crownapplication.ui.presenter.UserPresenter;
import com.example.crownapplication.ui.util.PrefUtil;
import com.example.crownapplication.ui.view.AnakView;
import com.example.crownapplication.ui.view.UserView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.jakewharton.threetenabp.AndroidThreeTen;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HomePageFragment extends Fragment implements AnakView{

    View myFragment;
    private int id_user;
    private UserPresenter userPresenter;
    private User user;
    private Anak anak;
    private UserOrtu userOrtu;
    private Api mApi;

    @BindView(R.id.rv_anak)
    RecyclerView rvAnak;

    //Activity context;

    private List<Anak> anakList = new ArrayList<>();
    private AnakPresenter anakPresenter;
    private AnakAdapter anakAdapter;
    private MaterialDialog dialog;


    public HomePageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragment = inflater.inflate(R.layout.fragment_home_page, container, false);
        ButterKnife.bind(this, myFragment);
        Db.getInstance().init(getActivity());
        AndroidThreeTen.init(getActivity());
        mApi = RetrofitBuilder.builder(getActivity()).create(Api.class);
        init();
        return myFragment;

    }

    private void init(){
        userOrtu = PrefUtil.getUser(getActivity(), PrefUtil.USER_SESSION);
        anakPresenter = new AnakPresenter(getContext(), this);
        anakList = anakPresenter.getByOrtu(userOrtu.getIdUser());
        anakAdapter =  new AnakAdapter(getContext(), anakList);

        rvAnak.setHasFixedSize(true);
        rvAnak.setLayoutManager(new LinearLayoutManager(getContext()));
    }


    @OnClick(R.id.fab_tambahanak_btn) void goAddAnak(){
        Intent intent = new Intent(getContext(), AddChildActivity.class);
        intent.putExtra("id_user", userOrtu.getIdUser());
        startActivity (intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadAnak(anakPresenter.getByOrtu(userOrtu.getIdUser()));
    }

    @Override
    public void showLoading() {
        dialog = DialogBuilder.showLoadingDialog(getContext(), "Updating Data", "Please Wait", false);
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadAnak(List<Anak> anakList) {
        anakAdapter.generate(anakList);
        anakAdapter.notifyDataSetChanged();
        rvAnak.setAdapter(anakAdapter);

    }
}
