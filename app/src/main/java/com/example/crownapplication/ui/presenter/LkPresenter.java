package com.example.crownapplication.ui.presenter;

import android.content.Context;

import com.example.crownapplication.bl.db.dao.LkDao;
import com.example.crownapplication.bl.db.model.Lk;
import com.example.crownapplication.ui.view.LkView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LkPresenter {
    private LkView lkView;

    public LkPresenter(Context context, LkView lkView) {
        this.lkView = lkView;
        AndroidThreeTen.init(context);
    }

    public void load(){
        lkView.showLoading();
        List<Lk> list = new ArrayList<>();
        try {
            list = LkDao.getLkDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        lkView.loadLk(list);
        lkView.hideLoading();
    }

    public List<Lk> getByJk(String jk){
        List<Lk> list = new ArrayList<>();
        try {
            list = LkDao.getLkDao().getByJk(jk);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Lk> getByUsiaJk(int usia, String jk){
        List<Lk> list = new ArrayList<>();
        try {
            list = LkDao.getLkDao().getByUsiaJk(usia, jk);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }


    public Lk getByID(int id){
        Lk lk = null;
        try {
            lk = LkDao.getLkDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lk;
    }

}
