package com.example.crownapplication.ui.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.DataGizi;
import com.example.crownapplication.bl.db.model.User;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.DataBbs;
import com.example.crownapplication.bl.network.model.DataTbs;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.presenter.AnakPresenter;
import com.example.crownapplication.ui.presenter.BbPresenter;
import com.example.crownapplication.ui.presenter.DataGiziPresenter;
import com.example.crownapplication.ui.view.AnakView;
import com.example.crownapplication.ui.view.DataGiziView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HeightChartActivity extends AppCompatActivity implements DataGiziView, AnakView {

    private int id_anak;
    private BbPresenter bbPresenter;
    private DataGiziPresenter dataGiziPresenter;
    private AnakPresenter anakPresenter;
    private User user;
    private UserOrtu userOrtu;
    private Anak anak;
    private DataGizi dataGizi;
    private Api mApi;
    private MaterialDialog mDialog;
    private Context context;

    @BindView(R.id.view_chart_height)
    LineChart chartTinggi;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private List<DataGizi> dataGiziList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_height_chart);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        initData();
        getDataTb();
        initViews();
    }

    private void initData() {

        id_anak = getIntent().getIntExtra("id", 0);
        anakPresenter = new AnakPresenter(getApplicationContext(), this);
        anak = anakPresenter.getByID(id_anak);

        dataGiziPresenter = new DataGiziPresenter(getApplicationContext(), this);
        //dataGizi = dataGiziPresenter.getLastIdByAnak(anak.getId_anak());

        //anakList = anakPresenter.getByOrtu(userOrtu.getIdUser());
        dataGiziList = dataGiziPresenter.getByAnak(id_anak);

    }

    private void getDataTb(){
        mApi.getDataTb(anak.getGender_anak())
                .enqueue(new Callback<List<DataTbs>>() {
                    @Override
                    public void onResponse(Call<List<DataTbs>> call, Response<List<DataTbs>> response) {
                        if (response.isSuccessful()){
                            List<DataTbs> dataTbs = response.body();
                            Log.i("DATATBCHART_GET", response.message());
                            setLineChartTb(response.body(), dataGiziList);

                        }
                    }

                    @Override
                    public void onFailure(Call<List<DataTbs>> call, Throwable t) {
                        DialogBuilder.showErrorDialog(context, t.getMessage());
                        Log.i("DATATBCHART_GET", t.getMessage());
                    }
                });


    }

    private void setLineChartTb(List<DataTbs> dataTbs, List<DataGizi> dataGiziTb){

        chartTinggi.getLegend().setWordWrapEnabled(true);
        chartTinggi.getLegend().setEnabled(true);
        chartTinggi.setDrawGridBackground(false);
        chartTinggi.getDescription().setEnabled(false);
        chartTinggi.setDrawBorders(false);

        chartTinggi.getAxisLeft().setEnabled(false);
        chartTinggi.getAxisRight().setDrawAxisLine(false);
        chartTinggi.getAxisRight().setDrawGridLines(false);
        chartTinggi.getXAxis().setDrawAxisLine(false);
        chartTinggi.getXAxis().setDrawGridLines(false);

        // enable touch gestures
        chartTinggi.setTouchEnabled(true);

        // enable scaling and dragging
        chartTinggi.setDragEnabled(true);
        chartTinggi.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        chartTinggi.setPinchZoom(false);

        ArrayList<String> xaxis = new ArrayList<>();
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        //ArrayList<ILineDataSet> dataSet2s = new ArrayList<>();

        dataSets.add(makeLineTb(dataTbs,"m3sd",Color.BLUE));
        dataSets.add(makeLineTb(dataTbs,"m2sd",Color.RED));
        dataSets.add(makeLineTb(dataTbs,"m1sd",Color.YELLOW));
        dataSets.add(makeLineTb(dataTbs,"medium",Color.GREEN));
        dataSets.add(makeLineTb(dataTbs,"p1sd",Color.YELLOW));
        dataSets.add(makeLineTb(dataTbs,"p2sd",Color.RED));
        dataSets.add(makeLineTb(dataTbs,"p3sd",Color.BLUE));

        dataSets.add(makeLineTbGizi(dataGiziTb,"tb_anak",Color.BLACK));

        chartTinggi.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        chartTinggi.getXAxis().setGranularity(1f);
        chartTinggi.getAxisLeft().setGranularity(1f);

        LineData datas = new LineData(dataSets);
        //LineData datas2 = new LineData(dataSet2s);
        chartTinggi.setData(datas);
        //chartTinggi.setData(datas2);
        chartTinggi.invalidate();

    }

    private ILineDataSet makeLineTb(List<DataTbs> dataTbs, String s, Integer color) {
        ArrayList<Entry> values = new ArrayList<>();
        for (int j=0;j<dataTbs.size();j++) {
            if (s.equals("m3sd")) {
                values.add(new Entry((float) dataTbs.get(j).getUsia(), (float) dataTbs.get(j).getM3sd()));
            } else if (s.equals("m2sd")) {
                values.add(new Entry((float) dataTbs.get(j).getUsia(), (float) dataTbs.get(j).getM2sd()));
            } else if (s.equals("m1sd")) {
                values.add(new Entry((float) dataTbs.get(j).getUsia(), (float) dataTbs.get(j).getM1sd()));
            } else if (s.equals("medium")) {
                values.add(new Entry((float) dataTbs.get(j).getUsia(), (float) dataTbs.get(j).getMedium()));
            } else if (s.equals("p1sd")) {
                values.add(new Entry((float) dataTbs.get(j).getUsia(), (float) dataTbs.get(j).getP1sd()));
            } else if (s.equals("p2sd")) {
                values.add(new Entry((float) dataTbs.get(j).getUsia(), (float) dataTbs.get(j).getP2sd()));
            } else if (s.equals("p3sd")) {
                values.add(new Entry((float) dataTbs.get(j).getUsia(), (float) dataTbs.get(j).getP3sd()));
            }
        }

        LineDataSet lineDataSet = new LineDataSet(values, s);
//        lineDataSet.setDrawValues(false);
        lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);

        // setting tampilan
        lineDataSet.setHighlightEnabled(true);
        lineDataSet.setLineWidth(1.5f);
//        lineDataSet.setCircleRadius(2f);
//            lineDataSet.setGradientColor(getGradientBackground().get(i).getStartColor(), getGradientBackground().get(i).getEndColor());
//        lineDataSet.setCircleColor(color);
        lineDataSet.setColor(color);
        lineDataSet.setDrawCircles(false);
//            lineDataSet.setDrawHighlightIndicators(true);
//            lineDataSet.setHighLightColor(Color.RED);

        return lineDataSet;
    }

    private ILineDataSet makeLineTbGizi(List<DataGizi> dataTb, String s, Integer color) {
        ArrayList<Entry> values2 = new ArrayList<>();
        for (int j=0;j<dataTb.size();j++) {
            if (s.equals("tb_anak")) {
                values2.add(new Entry((int) anak.getUsia(), (float) dataTb.get(j).getTb()));
            }
        }

        LineDataSet lineDataSet2 = new LineDataSet(values2, s);
//        lineDataSet.setDrawValues(false);
        lineDataSet2.setAxisDependency(YAxis.AxisDependency.LEFT);

        // setting tampilan
        lineDataSet2.setHighlightEnabled(true);
        lineDataSet2.setLineWidth(1.5f);
        lineDataSet2.setCircleRadius(5f);
//            lineDataSet.setGradientColor(getGradientBackground().get(i).getStartColor(), getGradientBackground().get(i).getEndColor());
        lineDataSet2.setCircleColor(color);
        lineDataSet2.setColor(color);
        lineDataSet2.setDrawCircles(true);
//            lineDataSet.setDrawHighlightIndicators(true);
//            lineDataSet.setHighLightColor(Color.RED);

        return lineDataSet2;
    }


    private void initViews(){
        toolbar.setTitle("Chart Tinggi");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void loadAnak(List<Anak> anakList) {

    }

    @Override
    public void showLoading() {
        mDialog = DialogBuilder.showLoadingDialog(getApplicationContext(), "Updating Data", "Please Wait", false);

    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();

    }

    @Override
    public void loadDataGizi(List<DataGizi> dataGiziList) {

    }

    @Override
    public void loadGizi(DataGizi dataGizi) {

    }
}
