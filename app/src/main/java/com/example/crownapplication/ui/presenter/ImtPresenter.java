package com.example.crownapplication.ui.presenter;

import android.content.Context;

import com.example.crownapplication.bl.db.dao.ImtDao;
import com.example.crownapplication.bl.db.model.Imt;
import com.example.crownapplication.ui.view.ImtView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ImtPresenter {

    private ImtView imtView;

    public ImtPresenter(Context context, ImtView imtView) {
        this.imtView = imtView;
        AndroidThreeTen.init(context);
    }

    public void load(){
        imtView.showLoading();
        List<Imt> list = new ArrayList<>();
        try {
            list = ImtDao.getImtDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        imtView.loadImt(list);
        imtView.hideLoading();
    }

    public List<Imt> getByJk(String jk){
        List<Imt> list = new ArrayList<>();
        try {
            list = ImtDao.getImtDao().getByJk(jk);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Imt getByID(int id){
        Imt imt = null;
        try {
            imt = ImtDao.getImtDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return imt;
    }

    public List<Imt> getByUsiaJk(int usia, String jk){
        List<Imt> list = new ArrayList<>();
        try {
            list = ImtDao.getImtDao().getByUsiaJk(usia, jk);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }


}
