package com.example.crownapplication.ui.presenter;

import android.content.Context;
import android.util.Log;

import com.example.crownapplication.bl.db.dao.PertanyaanDdstDao;
import com.example.crownapplication.bl.db.model.PertanyaanDdst;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.PertanyaanDdsts;
import com.example.crownapplication.ui.activity.TestActivity;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.view.PertanyaanView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.j256.ormlite.stmt.QueryBuilder.JoinWhereOperation.AND;

public class PertanyaanPresenter {

    private PertanyaanView pertanyaanView;
    private Api mApi;
    private Context context;

    public PertanyaanPresenter(Context context, PertanyaanView pertanyaanView) {
        this.pertanyaanView = pertanyaanView;
        this.context = context;
        AndroidThreeTen.init(context);
        mApi = RetrofitBuilder.builder(context).create(Api.class);

    }

//    public void load() {
//        pertanyaanView.showLoading();
//        List<PertanyaanDdst> list = new ArrayList<>();
//        try {
//            list = PertanyaanDdstDao.getPertanyaanddstDao().read();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        pertanyaanView.loadPertanyaan(list);
//        pertanyaanView.hideLoading();
//    }


    public PertanyaanDdst getByID(int id) {
        PertanyaanDdst pertanyaanDdst = null;
        try {
            pertanyaanDdst = PertanyaanDdstDao.getPertanyaanddstDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pertanyaanDdst;
    }

    public List<PertanyaanDdst> getByAspek(int aspek) {
        List<PertanyaanDdst> list = new ArrayList<>();
        try {
            list = PertanyaanDdstDao.getPertanyaanddstDao().getByAspek(aspek);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<PertanyaanDdst> getByUsia(int aspek, int usia) {
        List<PertanyaanDdst> list = new ArrayList<>();
        try {
            list = PertanyaanDdstDao.getPertanyaanddstDao().getByUsia(aspek, usia);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public int getUsia (int usia){
        if (usia > 24 && usia < 27){
            return  27;
        } else if (usia > 27 && usia < 30){
            return  30;
        } else if (usia > 30 && usia < 33){
            return  33;
        } else if (usia > 33 && usia < 36){
            return 36;
        } else if (usia > 36 && usia < 39){
            return 39;
        } else if (usia > 39 && usia < 42){
            return 42;
        } else if (usia > 42 && usia < 45){
            return 45;
        } else if (usia > 45 && usia < 48){
            return 48;
        } else if (usia > 48 && usia < 51){
            return 51;
        } else if (usia > 51 && usia < 54){
            return 54;
        } else if (usia > 54 && usia < 57){
            return 57;
        } else if (usia > 57 && usia < 60){
            return 60;
        } else if (usia > 63 && usia < 66){
            return 63;
        } else if (usia > 66 && usia < 69){
            return 66;
        } else if (usia > 69 && usia < 72){
            return 72;
        } else {
            return usia;
        }

    }

    public void initPertanyaan(int idAspek, int usia) {
        usia = getUsia(usia);
        mApi.getDataPertanyaan(idAspek, usia).enqueue(new Callback<PertanyaanDdsts>() {
            @Override
            public void onResponse(Call<PertanyaanDdsts> call, Response<PertanyaanDdsts> response) {
                Log.i("PERTANYAAN_GET", response.message());
                PertanyaanDdsts list = response.body();
                pertanyaanView.loadPertanyaan(list.getPertanyaanddst());

                for (PertanyaanDdsts.PertanyaanDdst pertanyaan : list.getPertanyaanddst()) {
                    PertanyaanDdst obj = new PertanyaanDdst();
                    obj.setId_pertanyaan(pertanyaan.getIdPertanyaan());
                    obj.setId_aspek(pertanyaan.getIdAspek());
                    obj.setUsia(pertanyaan.getUsia());
                    obj.setPertanyaan(pertanyaan.getPertanyaan());

                    try {
                        PertanyaanDdstDao.getPertanyaanddstDao().add(obj);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PertanyaanDdsts> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("PERTANYAAN_GET", t.getMessage());
            }
        });

    }


}
