package com.example.crownapplication.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.PertanyaanDdst;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.DataPertanyaans;
import com.example.crownapplication.bl.network.model.PertanyaanDdsts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PertanyaanAdapter extends RecyclerView.Adapter<PertanyaanAdapter.ViewHolder> {
    Context context;
    List<PertanyaanDdsts.PertanyaanDdst> list = new ArrayList<>();
    PertanyaanDdst selected = new PertanyaanDdst();
    int mCheckedPostion = -1;
    private boolean isChoosen = false;
    int countP = 0;
    int countF = 0;
    int countR = 0;
    //private CompoundButton lastCheckedRB = null;
    private String selectedRbText;
    private RadioGroup lastCheckedRG = null;
    private RadioButton lastCheckedRB = null;

    private Api mApi;

    private ArrayList<Integer> listP = new ArrayList<>();
    private ArrayList<Integer> listF = new ArrayList<>();
    private ArrayList<Integer> listR = new ArrayList<>();

    public PertanyaanAdapter(Context context) {
        this.context = context;
    }

    public PertanyaanAdapter(Context context, List<PertanyaanDdsts.PertanyaanDdst> list) {
        this.context = context;
        this.list = list;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_test, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //holder.setIsRecyclable(false);
        PertanyaanDdsts.PertanyaanDdst pertanyaanDdst = list.get(position);
        holder.id = pertanyaanDdst.getIdPertanyaan();
        holder.tvTest.setText(pertanyaanDdst.getPertanyaan());
        //holder.rbOrtu.setChecked(position == mCheckedPostion);
        holder.rgTest.setTag(position);

        holder.rgTest.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checked_rb = (RadioButton) group.findViewById(checkedId);
//                if (lastCheckedRB != null) {
//                    lastCheckedRB.setChecked(false);
//                }
//                //store the clicked radiobutton
//                lastCheckedRB = checked_rb;

                if(checked_rb.getText().toString().equalsIgnoreCase("Bisa")){
                    countP++;
                }else if (checked_rb.getText().toString().equalsIgnoreCase("Gagal")){
                    countF++;
                }else{
                    countR++;
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tv_test) TextView tvTest;
        @BindView(R.id.radioGrup_test)
        RadioGroup rgTest;
        @BindView(R.id.rb_bisa_test) RadioButton rbBisa;
        @BindView(R.id.rb_gagal_test) RadioButton rbGagal;
        @BindView(R.id.rb_tolak_test) RadioButton rbTolak;
        private int id;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    public void generate(List<PertanyaanDdsts.PertanyaanDdst> pertanyaanList){
        clear();
        list = pertanyaanList;
        notifyDataSetChanged();
    }

    public void clear(){
        list.clear();
        notifyDataSetChanged();
    }

    public boolean getPertanyaanChecked(){
        return isChoosen;
    }

    public int getSelectedF(){
        return countF;
    }
    public int getSelectedR(){
        return countR;
    }
    public int getSelectedP(){
        return countP;
    }
}

