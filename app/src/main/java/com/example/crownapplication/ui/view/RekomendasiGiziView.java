package com.example.crownapplication.ui.view;

import com.example.crownapplication.bl.db.model.RekomendasiGizi;

import java.util.List;

public interface RekomendasiGiziView {
    void showLoading();
    void hideLoading();
    void loadRekomGizi(List<RekomendasiGizi> rekomGiziList);
}

