package com.example.crownapplication.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.User;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.api.SyncWorker;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.BaseRespons;
import com.example.crownapplication.bl.network.model.DataAnaks;
import com.example.crownapplication.bl.network.model.DataJawabans;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.presenter.AnakPresenter;
import com.example.crownapplication.ui.presenter.UserPresenter;
import com.example.crownapplication.ui.util.PrefUtil;
import com.example.crownapplication.ui.view.AnakView;
import com.example.crownapplication.ui.view.UserView;
import com.google.android.material.textfield.TextInputEditText;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMeasureNewUserActivity extends AppCompatActivity implements UserView {

    private int id_anak, id_user;
    private AnakPresenter anakPresenter;
    private UserPresenter userPresenter;
    private User user;
    private UserOrtu userOrtu;
    private Anak anak;
    private Api mApi;
    private MaterialDialog mDialog;
    private Context ctx;

    @BindView(R.id.tedit_berat_addnew)
    TextInputEditText etBb;
    @BindView(R.id.tedit_tinggi_addnew)
    TextInputEditText etTb;
    @BindView(R.id.tedit_lkepala_addnew)
    TextInputEditText etLk;

    private HashMap<String, Integer> idAnak = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_measure_new_user);

        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        initData();
        getDataAnak();
        //initViews();
    }

    private void initData(){
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        userOrtu = PrefUtil.getUser(this, PrefUtil.USER_SESSION);
//        anakPresenter = new AnakPresenter(getApplicationContext(), this);
//        anak = anakPresenter.getByID(id_anak);
        //user = userPresenter.getByID(id_user);
    }

    private void getDataAnak(){
        mApi.getDataAnak(userOrtu.getIdUser())
                .enqueue(new Callback<DataAnaks>() {
                    @Override
                    public void onResponse(Call<DataAnaks> call, Response<DataAnaks> response) {
                        if (response.isSuccessful()){
                            DataAnaks dataanak = response.body();
                            Log.i("DATAANAK_GET", response.message());
                            id_anak = dataanak.getIdAnak();
                            //idAnak.put("anak_id", anak.getIdAnak());

                        }
                    }

                    @Override
                    public void onFailure(Call<DataAnaks> call, Throwable t) {
                        DialogBuilder.showErrorDialog(ctx, t.getMessage());
                        Log.i("DATAANAK_GET", t.getMessage());
                    }
                });
    }


    @OnClick(R.id.btn_simpan_addnew) void onAdd(){
        MaterialDialog dialog = DialogBuilder.showLoadingDialog(AddMeasureNewUserActivity.this, "Add Data", "Please Wait", false);
        String strTb = etTb.getText().toString();
        String strBb = etBb.getText().toString();
        float tb = Float.parseFloat(strTb);
        float bb = Float.parseFloat(strBb);
        float imt = bb / (( tb / 100) * (tb / 100));
        String strImt=String.valueOf(imt);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        mApi.addDataGizi(formatter.format(date),etTb.getText().toString(),
                etBb.getText().toString(), etLk.getText().toString(),
                strImt, id_anak)
                .enqueue(new Callback<BaseRespons>() {
                    @Override
                    public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                        BaseRespons baseRespons = response.body();
                        String message = "";
                        if (baseRespons!=null){
                            if(!baseRespons.getError()){
                                message = "Data berhasil ditambahkan";
                                //SyncWorker.getSyncWorker().syncAnak(getApplicationContext(), mApi.getAnak(), false);
                                //SyncWorker.getSyncWorker().syncDataGizi(getApplicationContext(), mApi.getDataGizi(), false);
                            }
                        }
                        if (response.code() == 400){
                            message = "Invalid parameter!";
                        }
                        if (response.code() == 502){
                            message = "Gagal menyimpan ke database.";
                        }
                        Toast.makeText(AddMeasureNewUserActivity.this, message, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        Intent intent = new Intent(AddMeasureNewUserActivity.this, MainActivity.class);
                        startActivity(intent);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        onSupportNavigateUp();
                    }

                    @Override
                    public void onFailure(Call<BaseRespons> call, Throwable t) {

                    }
                });
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadUser(List<User> userList) {

    }

//    @Override
//    public void loadAnak(List<Anak> anakList) {
//
//    }
}
