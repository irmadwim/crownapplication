package com.example.crownapplication.ui.view;

import com.example.crownapplication.bl.db.model.User;

import java.util.List;

public interface UserView {
    void showLoading();
    void hideLoading();
    void loadUser(List<User> userList);
}
