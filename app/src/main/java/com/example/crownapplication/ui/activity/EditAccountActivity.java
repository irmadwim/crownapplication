package com.example.crownapplication.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.User;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.api.SyncWorker;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.BaseRespons;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.presenter.UserPresenter;
import com.example.crownapplication.ui.util.PrefUtil;
import com.example.crownapplication.ui.view.UserView;
import com.google.android.material.textfield.TextInputEditText;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditAccountActivity extends AppCompatActivity implements UserView {

    private int id_user;
    private Api mApi;
    private MaterialDialog mDialog;
    private UserPresenter userPresenter;
    private User user;
    private UserOrtu userOrtu;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tedit_nama_editprofile)
    TextInputEditText etNama;
    @BindView(R.id.tedit_email_editprofile)
    TextInputEditText etEmail;
    @BindView(R.id.profile_picture)
    ImageView ivAvatar;
    private Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        initData();
        initViews();
    }

    private void initData(){
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        id_user = getIntent().getIntExtra("id", 0);
        userOrtu = PrefUtil.getUser(this, PrefUtil.USER_SESSION);
        userPresenter = new UserPresenter(getApplicationContext(), this);
        user = userPresenter.getByID(userOrtu.getIdUser());
    }

    private void initViews(){
        toolbar.setTitle("Edit Profil");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        etNama.setText(String.valueOf(user.getNama()));
        etEmail.setText(String.valueOf(user.getEmail()));
        //etPassword.setText(String.valueOf(guru.getPassword()));
        ivAvatar.setImageResource(R.drawable.profile);

    }

    @OnClick(R.id.btn_save_edit) void onUpdate(){
        MaterialDialog dialog = DialogBuilder.showLoadingDialog(EditAccountActivity.this, "Saving Data", "Please Wait", false);
        mApi.updateUser(userOrtu.getIdUser(), etNama.getText().toString(),
                etEmail.getText().toString())
                .enqueue(new Callback<BaseRespons>() {
                    @Override
                    public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                        BaseRespons baseRespons = response.body();
                        String message = "";
                        if (baseRespons!=null){
                            if (!baseRespons.getError()){
                                message = "Data berhasil disimpan.";
                                SyncWorker.getSyncWorker().syncUser(ctx, mApi.getUser(), false);
                            }
                        }
                        if (response.code() == 400){
                            message = "Invalid parameter!";
                        }
                        if (response.code() == 502){
                            message = "Gagal menyimpan ke database.";
                        }
                        Toast.makeText(EditAccountActivity.this, message, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        onSupportNavigateUp();
                    }

                    @Override
                    public void onFailure(Call<BaseRespons> call, Throwable t) {

                    }
                });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadUser(List<User> userList) {

    }
}
