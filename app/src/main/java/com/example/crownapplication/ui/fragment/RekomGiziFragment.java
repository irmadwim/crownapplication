package com.example.crownapplication.ui.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.RekomendasiGizi;
import com.example.crownapplication.event.RekomendasiGiziTabEvent;
import com.example.crownapplication.ui.adapter.RekomendasiGiziAdapter;
import com.example.crownapplication.ui.adapter.UmurRekomGiziAdapter;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.presenter.RekomendasiGiziPresenter;
import com.example.crownapplication.ui.view.RekomendasiGiziView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RekomGiziFragment extends Fragment implements RekomendasiGiziView {

    @BindView(R.id.rvUmur_rekomGizi)
    RecyclerView rvUmur;
    @BindView(R.id.rvGizi_rekomGizi)
    RecyclerView rvGizi;

    private MaterialDialog dialog;
    private int tab;
    private RekomendasiGiziPresenter rekomGiziPresenter;
    private RekomendasiGiziAdapter rekomGiziAdapter;
    private UmurRekomGiziAdapter umurAdapter;
    private List<String> listUmur = new ArrayList<>();
    private List<RekomendasiGizi> listRekomGizi = new ArrayList<>();

    public RekomGiziFragment() {
        // Required empty public constructor
    }

    @Subscribe(threadMode = ThreadMode.MAIN) public void setUmur(RekomendasiGiziTabEvent event){
        tab = event.idTab;
        //loadUmur(tab);
        loadRekomGizi(tab);
        //Log.i("USIA_MODE", "setMode: " + tab);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rekom_gizi, container, false);
        ButterKnife.bind(this, view);
        init();
        EventBus.getDefault().register(this);
        return view;
    }

    private void init(){
        rekomGiziPresenter = new RekomendasiGiziPresenter(getContext(),this);
        rekomGiziAdapter = new RekomendasiGiziAdapter(getContext());
        umurAdapter = new UmurRekomGiziAdapter(getContext());

        rvUmur.setHasFixedSize(true);
        rvUmur.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        rvUmur.setAdapter(umurAdapter);

        rvGizi.setHasFixedSize(true);
        rvGizi.setLayoutManager(new LinearLayoutManager(getContext()));
        rvGizi.setAdapter(rekomGiziAdapter);

    }

    private void loadRekomGizi(int i){
        listRekomGizi.clear();
        if (i == 0){
            listRekomGizi = rekomGiziPresenter.loadByAge(0, 5);
        }else if (i == 1){
            listRekomGizi = rekomGiziPresenter.loadByAge(6, 8);
        }else if (i == 2){
            listRekomGizi = rekomGiziPresenter.loadByAge(9, 12);
        }else if (i == 3){
            listRekomGizi = rekomGiziPresenter.loadByAge(13, 23);
        }else if (i == 4){
            listRekomGizi = rekomGiziPresenter.loadByAge(24, 36);
        }else {
            listRekomGizi = rekomGiziPresenter.loadByAge(37, 60);
        }
        rekomGiziAdapter.generate(listRekomGizi);
        rekomGiziAdapter.notifyDataSetChanged();
    }

    private void loadUmur(int i){
        listUmur.clear();
        String[] array = null;
        if (i == 0)
            array = getResources().getStringArray(R.array.umur1);
        else if (i == 1)
            array = getResources().getStringArray(R.array.umur2);
        else if (i == 2)
            array = getResources().getStringArray(R.array.umur3);
        else if (i == 3)
            array = getResources().getStringArray(R.array.umur4);
        else if (i == 4)
            array = getResources().getStringArray(R.array.umur5);
        else
            array = getResources().getStringArray(R.array.umur6);

        for (String item :array) {
            listUmur.add(item);
        }
        umurAdapter.generate(listUmur);
        umurAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        //loadUmur(tab);
        loadRekomGizi(tab);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void showLoading() {
        dialog = DialogBuilder.showLoadingDialog(getContext(), "Updating Data", "Please Wait", false);

    }

    @Override
    public void hideLoading() {
        dialog.dismiss();

    }

    @Override
    public void loadRekomGizi(List<RekomendasiGizi> rekomGiziList) {

    }
}
