package com.example.crownapplication.ui.presenter;

import android.content.Context;

import com.example.crownapplication.bl.db.dao.AnakDao;
import com.example.crownapplication.bl.db.dao.BbDao;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.Bb;
import com.example.crownapplication.ui.view.AnakView;
import com.example.crownapplication.ui.view.BbView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BbPresenter {

    private BbView bbView;

    public BbPresenter(Context context, BbView bbView) {
        this.bbView = bbView;
        AndroidThreeTen.init(context);
    }

    public void load(){
        bbView.showLoading();
        List<Bb> list = new ArrayList<>();
        try {
            list = BbDao.getBbDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        bbView.loadBb(list);
        bbView.hideLoading();
    }

    public List<Bb> getByJk(String jk){
        List<Bb> list = new ArrayList<>();
        try {
            list = BbDao.getBbDao().getByJk(jk);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Bb> getByUsiaJk(int usia, String jk){
        List<Bb> list = new ArrayList<>();
        try {
            list = BbDao.getBbDao().getByUsiaJk(usia, jk);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }


    public Bb getByID(int id){
        Bb bb = null;
        try {
            bb = BbDao.getBbDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bb;
    }

}
