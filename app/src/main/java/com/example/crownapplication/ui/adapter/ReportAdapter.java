package com.example.crownapplication.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.ReportAkhir;
import com.example.crownapplication.bl.network.model.ReportAkhirs;
import com.example.crownapplication.ui.activity.DetailChildActivity;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ViewHolder> {

    Context context;
    List<ReportAkhirs.ReportAkhir> list = new ArrayList<>();

    public ReportAdapter(Context context) {
        this.context = context;
        AndroidThreeTen.init(context);
    }

    public ReportAdapter(Context context, List<ReportAkhirs.ReportAkhir> list) {
        this.context = context;
        this.list = list;
        AndroidThreeTen.init(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_report, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ReportAkhirs.ReportAkhir reportAkhir = list.get(position);
        holder.id = reportAkhir.getIdReportAkhir();
        holder.tvHasil.setText(reportAkhir.getHasilAkhir());
        holder.tvTanggal.setText(reportAkhir.getTanggal());

        if (reportAkhir.getHasilAkhir().equals("Normal")) {
            holder.ivAvatar.setImageResource(R.drawable.ic_smile);
        } else if (reportAkhir.getHasilAkhir().equals("Abnormal")) {
            holder.ivAvatar.setImageResource(R.drawable.ic_cry);
        } else if (reportAkhir.getHasilAkhir().equals("Meragukan")) {
            holder.ivAvatar.setImageResource(R.drawable.ic_sad);
        } else {
            holder.ivAvatar.setImageResource(R.drawable.ic_reject);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_hasil_report)
        TextView tvHasil;
        @BindView(R.id.im_hasil_report)
        ImageView ivAvatar;
        @BindView(R.id.tv_tanggal_report)
        TextView tvTanggal;
        int id;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public void generate(List<ReportAkhirs.ReportAkhir> list) {
        clear();
        this.list = list;
        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }
}