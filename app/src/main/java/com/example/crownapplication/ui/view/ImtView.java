package com.example.crownapplication.ui.view;

import com.example.crownapplication.bl.db.model.Imt;

import java.util.List;

public interface ImtView {
    void showLoading();
    void hideLoading();
    void loadImt(List<Imt> imtList);
}
