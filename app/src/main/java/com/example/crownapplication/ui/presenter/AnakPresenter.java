package com.example.crownapplication.ui.presenter;

import android.content.Context;

import com.example.crownapplication.bl.db.dao.AnakDao;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.ui.view.AnakView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AnakPresenter {
    private AnakView anakView;

    public AnakPresenter(Context context, AnakView anakView) {
        this.anakView = anakView;
        AndroidThreeTen.init(context);
    }

    public void load(){
        anakView.showLoading();
        List<Anak> list = new ArrayList<>();
        try {
            list = AnakDao.getAnakDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        anakView.loadAnak(list);
        anakView.hideLoading();
    }

    public List<Anak> loadByOrtu(int ortu){
        anakView.showLoading();
        List<Anak> list = new ArrayList<>();
        List<Anak> selected = new ArrayList<>();
        try {
            list = AnakDao.getAnakDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (Anak anak : list){
            if (ortu <= anak.getId_user() ){
                selected.add(anak);
            }
        }
        //muridView.loadTppa(selected);
        anakView.hideLoading();
        return selected;
    }


    public List<Anak> getByOrtu(int ortu){
        List<Anak> list = new ArrayList<>();
        try {
            list = AnakDao.getAnakDao().getByOrtu(ortu);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Anak getByID(int id){
        Anak anak = null;
        try {
            anak = AnakDao.getAnakDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return anak;
    }

    public Anak getFirstId(){
        Anak anak = null;
        try {
            anak = AnakDao.getAnakDao().getFirstId();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return anak;
    }

    public Anak getFirstIdByOrtu(int ortu){
        Anak anak = null;
        try {
            anak = AnakDao.getAnakDao().getFirstIdByOrtu(ortu);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return anak;
    }
}

