package com.example.crownapplication.ui.presenter;

import android.content.Context;

import com.example.crownapplication.bl.db.dao.AspekDao;
import com.example.crownapplication.bl.db.model.Aspek;
import com.example.crownapplication.ui.view.AspekView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AspekPresenter {
    private AspekView aspekView;

    public AspekPresenter(Context context, AspekView aspekView) {
        this.aspekView = aspekView;
        AndroidThreeTen.init(context);
    }

    public void load(){
        aspekView.showLoading();
        List<Aspek> list = new ArrayList<>();
        try {
            list = AspekDao.getAspekDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        aspekView.loadAspek(list);
        aspekView.hideLoading();
    }


    public Aspek getByID(int id){
        Aspek aspek = null;
        try {
            aspek = AspekDao.getAspekDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return aspek;
    }


}
