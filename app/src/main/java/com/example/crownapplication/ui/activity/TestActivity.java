package com.example.crownapplication.ui.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.dao.PertanyaanDdstDao;
import com.example.crownapplication.bl.db.helper.Db;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.Aspek;
import com.example.crownapplication.bl.db.model.JawabanResponden;
import com.example.crownapplication.bl.db.model.PertanyaanDdst;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.api.SyncWorker;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.BaseRespons;
import com.example.crownapplication.bl.network.model.DataJawabans;
import com.example.crownapplication.bl.network.model.DataPertanyaans;
import com.example.crownapplication.bl.network.model.JawabanRespondens;
import com.example.crownapplication.bl.network.model.PertanyaanDdsts;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.adapter.AnakAdapter;
import com.example.crownapplication.ui.adapter.PertanyaanAdapter;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.presenter.AnakPresenter;
import com.example.crownapplication.ui.presenter.AspekPresenter;
import com.example.crownapplication.ui.presenter.PertanyaanPresenter;
import com.example.crownapplication.ui.presenter.UserPresenter;
import com.example.crownapplication.ui.util.PrefUtil;
import com.example.crownapplication.ui.view.AnakView;
import com.example.crownapplication.ui.view.AspekView;
import com.example.crownapplication.ui.view.PertanyaanView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestActivity extends AppCompatActivity implements AspekView, AnakView, PertanyaanView {

    private Api mApi;
    private UserPresenter userPresenter;
    private AnakPresenter anakPresenter;
    private AspekPresenter aspekPresenter;
    private PertanyaanPresenter pertanyaanPresenter;
    private PertanyaanAdapter pertanyaanAdapter;
    private int id_anak, usia;
    private Anak anak;
    private Aspek aspek;
    private UserOrtu userOrtu;
    private Dialog mDialog;
    private int aspekID;
    private String hasil;
    private JawabanRespondens.JawabanResponden jawabanResponden;
    private Context context;

    private List<Aspek> aspekList = new ArrayList<>();
    private List<DataPertanyaans> pertanyaanList = new ArrayList<>();
    private List<DataJawabans> jawabanList = new ArrayList<>();


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_usia_test)
    TextView tvUsia;
    @BindView(R.id.tv_namaanak_test)
    TextView tvNama;
    @BindView(R.id.tv_namaaspek)
    TextView tvAspek;
    @BindView(R.id.rv_test)
    RecyclerView rvTest;
    @BindView(R.id.im_anak_test)
    ImageView ivAvatar;
    @BindView(R.id.btn_back)
    Button btnBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ButterKnife.bind(this);
        Db.getInstance().init(this);
        AndroidThreeTen.init(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        initData();
        initView();

    }

    private void initData() {
        id_anak = getIntent().getIntExtra("id", 0);
        anakPresenter = new AnakPresenter(getApplicationContext(), this);
        anak = anakPresenter.getByID(id_anak);
        aspekPresenter = new AspekPresenter(getApplicationContext(), this);
        pertanyaanPresenter = new PertanyaanPresenter(getApplicationContext(),this);

        aspekID = PrefUtil.getIDAspek(getApplicationContext());
        //getDataPertanyaan();
        pertanyaanPresenter.initPertanyaan(aspekID, anak.getUsia());

    }

//    private void getDataPertanyaan(){
//        mApi.getDataPertanyaan(aspekID, anak.getUsia())
//                .enqueue(new Callback<List<DataPertanyaans>>() {
//                    @Override
//                    public void onResponse(Call<List<DataPertanyaans>> call, Response<List<DataPertanyaans>> response) {
//                        if (response.isSuccessful()){
//                            List<DataPertanyaans> pertanyaan = response.body();
//                            Log.i("DATAJAWABAN_GET", response.message());
//                            pertanyaanList = pertanyaan;
//
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<List<DataPertanyaans>> call, Throwable t) {
//                        DialogBuilder.showErrorDialog(context, t.getMessage());
//                        Log.i("DATAJAWABAN_GET", t.getMessage());
//                    }
//                });
//    }



    private void initView() {

        toolbar.setTitle("Kuesioner");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (aspekID > 1){

            btnBack.setVisibility(View.VISIBLE);
        }

        if (aspekID == 1){
            tvAspek.setText("Personal Sosial");
        } else if (aspekID == 2){
            tvAspek.setText("Motorik Halus");
        } else if (aspekID == 3) {
            tvAspek.setText("Bahasa");
        } else {
            tvAspek.setText("Motorik Kasar");
        }

        tvNama.setText(String.valueOf(anak.getNama_anak()));

        if (anak.getUsia() >= 12)
            tvUsia.setText(anak.getUsia()/12 + " tahun " + anak.getUsia()%12 + " bulan");
        else
            tvUsia.setText(anak.getUsia() + " bulan");

        // Set Avatar sesuai jenis kelamin dan umur
        if (anak.getGender_anak().equals("L")){
            ivAvatar.setImageResource(R.drawable.ic_baby_boy);
        }else {
            ivAvatar.setImageResource(R.drawable.ic_baby_girl);

        }

    }


    @OnClick(R.id.btn_next) void goNext(){
        saveDataJawaban();

    }

    public void saveDataJawaban(){
        mApi.addJawaban(aspekID, anak.getId_anak(),
                pertanyaanAdapter.getSelectedP(), pertanyaanAdapter.getSelectedF(),
                pertanyaanAdapter.getSelectedR())
                .enqueue(new Callback<BaseRespons>() {
                    @Override
                    public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                        BaseRespons baseRespons = response.body();
                        aspekID++;
                        PrefUtil.setIDAspek(getApplicationContext(),aspekID);
                        if (aspekID < 5){
                            Intent intent = getIntent();
                            finish();
                            startActivity(intent);
                        } else {
                            PrefUtil.setIDAspek(getApplicationContext(),1);
                            getDataJawaban();
                        }
                        //jawabanList.add(response.body());

                    }

                    @Override
                    public void onFailure(Call<BaseRespons> call, Throwable t) {

                    }
                });
    }

    private void getDataJawaban(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        mApi.getDataJawaban(anak.getId_anak(), formatter.format(date))
                .enqueue(new Callback<List<DataJawabans>>() {
                    @Override
                    public void onResponse(Call<List<DataJawabans>> call, Response<List<DataJawabans>> response) {
                        if (response.isSuccessful()){
                            List<DataJawabans> report = response.body();
                            Log.i("DATAJAWABAN_GET", response.message());
                            jawabanList = report;
                            hasil = chainingJawaban();
                            addReport();

                        }
                    }

                    @Override
                    public void onFailure(Call<List<DataJawabans>> call, Throwable t) {
                        DialogBuilder.showErrorDialog(context, t.getMessage());
                        Log.i("DATAJAWABAN_GET", t.getMessage());
                    }
                });
    }

    public String chainingJawaban(){
        int count1 = 0;
        int count2 = 0;
        for (int i=0;i<jawabanList.size()-1;i++){
            DataJawabans item = jawabanList.get(i);
            if (item.getJawabanF() >= 1) count1++;
            if (item.getJawabanF() >= 2) count2++;
            for (int j=i+1;j<jawabanList.size();j++){
                DataJawabans temp = jawabanList.get(j);
                if (item.getIdAspek() != temp.getIdAspek()){
                    if (temp.getJawabanF() >= 1) count1++;
                    if (temp.getJawabanF() >= 2) count2++;
                    if (count2 >= 2)
                       return "Abnormal";
                    else if (item.getJawabanF() >= 2 && count1 >= 2 || temp.getJawabanF() >= 2 && count1 >= 2)
                       return "Meragukan";
                    else if (item.getJawabanR() >= 1 || temp.getJawabanR() >= 1)
                        return "Tidak Dapat Dites";

                }
            }
        }
        return "Normal";
    }




    public void addReport(){
        mApi.addReport(anak.getId_anak(), hasil)
                .enqueue(new Callback<BaseRespons>() {
                    @Override
                    public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                        BaseRespons baseRespons = response.body();
                        updateReported();
                    }

                    @Override
                    public void onFailure(Call<BaseRespons> call, Throwable t) {

                    }
                });
    }

    public void updateReported(){
        mApi.updateReported(anak.getId_anak())
                .enqueue(new Callback<BaseRespons>() {
                    @Override
                    public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                        BaseRespons baseRespons = response.body();

                        //SyncWorker.getSyncWorker().syncReportAkhir(context, mApi.getReport(), false);
                        Intent intent = new Intent(TestActivity.this, HasilTestActivity.class);
                        intent.putExtra("id", anak.getId_anak());
                        intent.putExtra("hasil", hasil);
                        startActivity (intent);
                    }

                    @Override
                    public void onFailure(Call<BaseRespons> call, Throwable t) {

                    }
                });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
       // loadPertanyaan(pertanyaanPresenter.getByUsia(aspek.getId_aspek(), usia));
    }


    @Override
    public void loadAnak(List<Anak> anakList) {

    }

    @Override
    public void showLoading() {
        mDialog = DialogBuilder.showLoadingDialog(getApplicationContext(), "Updating Data", "Please Wait", false);

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadPertanyaan(List<PertanyaanDdsts.PertanyaanDdst> pertanyaanList) {
        pertanyaanAdapter =  new PertanyaanAdapter(getApplicationContext(), pertanyaanList);
        rvTest.setHasFixedSize(true);
        rvTest.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvTest.setAdapter(pertanyaanAdapter);
    }


    @Override
    public void loadAspek(List<Aspek> aspekList) {

    }
}
