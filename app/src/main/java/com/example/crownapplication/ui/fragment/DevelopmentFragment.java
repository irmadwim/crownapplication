package com.example.crownapplication.ui.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.dao.PertanyaanDdstDao;
import com.example.crownapplication.bl.db.helper.Db;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.PertanyaanDdst;
import com.example.crownapplication.bl.db.model.ReportAkhir;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.PertanyaanDdsts;
import com.example.crownapplication.bl.network.model.ReportAkhirs;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.activity.TestActivity;
import com.example.crownapplication.ui.adapter.AnakAdapter;
import com.example.crownapplication.ui.adapter.DialogListAdapter;
import com.example.crownapplication.ui.adapter.ReportAdapter;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.presenter.AnakPresenter;
import com.example.crownapplication.ui.presenter.ReportAkhirPresenter;
import com.example.crownapplication.ui.presenter.UserPresenter;
import com.example.crownapplication.ui.util.PrefUtil;
import com.example.crownapplication.ui.view.AnakView;
import com.example.crownapplication.ui.view.ReportAkhirView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DevelopmentFragment extends Fragment implements AnakView, ReportAkhirView {

    Calendar calendar;
    int year;
    int month;
    int date;
    private View view;
    public int id_anak;
    private Api mApi;
    private UserPresenter userPresenter;
    private AnakPresenter anakPresenter;
    private AnakAdapter anakAdapter;
    private ReportAdapter reportAdapter;
    private ReportAkhirPresenter reportAkhirPresenter;
    private DialogListAdapter dialogListAdapter;
    private UserOrtu userOrtu;
    private Anak anak;
    private Context context;
    private String monthYearStr;

    private List<Anak> anakList = new ArrayList<>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
    SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");

    @BindView(R.id.tv_namaanak_dev)
    TextView tvNama;
    @BindView(R.id.tv_usia_dev)
    TextView tvUsia;
    @BindView(R.id.tv_gagal_report)
    TextView tvGagal;
    @BindView(R.id.im_anak_dev)
    ImageView ivAvatar;
    @BindView(R.id.rv_report)
    RecyclerView rvReport;
    @BindView(R.id.etDatepicker)
    EditText etTgl;

    public DevelopmentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_development_test, container, false);
        ButterKnife.bind(this, view);
        Db.getInstance().init(getActivity());
        AndroidThreeTen.init(getActivity());
        mApi = RetrofitBuilder.builder(getActivity()).create(Api.class);
        initData();
        initView();
        return view;
    }

    private void initData() {
        userOrtu = PrefUtil.getUser(getActivity(), PrefUtil.USER_SESSION);
        anakPresenter = new AnakPresenter(getContext(), this);
        anak = anakPresenter.getFirstIdByOrtu(userOrtu.getIdUser());
        reportAkhirPresenter = new ReportAkhirPresenter(getContext(), this);

        anakList = anakPresenter.getByOrtu(userOrtu.getIdUser());
        //dialogListAdapter =  new DialogListAdapter(getContext(), anakList);

    }

    private void initView() {
        tvNama.setText(String.valueOf(anak.getNama_anak()));

        if (anak.getUsia() >= 12)
            tvUsia.setText(anak.getUsia()/12 + " tahun " + anak.getUsia()%12 + " bulan");
        else
            tvUsia.setText(anak.getUsia() + " bulan");

        // Set Avatar sesuai jenis kelamin dan umur
        if (anak.getGender_anak().equals("L")){
            ivAvatar.setImageResource(R.drawable.ic_baby_boy);
        }else {
            ivAvatar.setImageResource(R.drawable.ic_baby_girl);

        }
    }

    @OnClick(R.id.etDatepicker) void showTimeDialog(){
        MonthYearPickerDialog pickerDialog = new MonthYearPickerDialog();
        pickerDialog.setListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int i2) {
                monthYearStr = year + "-" + (month + 1) + "-" + i2;
                etTgl.setText(formatMonthYear(monthYearStr));
//                if (anak == null){
//                    Toast.makeText(view.getContext(), "testing", Toast.LENGTH_SHORT).show();
//                }else {
//                    Toast.makeText(view.getContext(), String.valueOf(anak.getId_anak()), Toast.LENGTH_SHORT).show();
//                }
                reportAkhirPresenter.getReport(anak.getId_anak(), etTgl.getText().toString());
            }
        });
        pickerDialog.show(getFragmentManager().beginTransaction(), "MonthYearPickerDialog");
    }

    String formatMonthYear(String str) {
        Date date = null;
        try {
            date = input.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(date);
    }


//    private void getReport(){
//        mApi.getDataReport(anak.getId_anak(), etTgl.getText().toString())
//                .enqueue(new Callback<DataReports>() {
//                    @Override
//                    public void onResponse(Call<DataReports> call, Response<DataReports> response) {
//                        if (response.isSuccessful()){
//                            Log.i("DATAREPORT_GET", response.message());
//
//                            if (response.body().getHasilAkhir() != null){
//                                DataReports report = response.body();
//
//                                tvGagal.setVisibility(View.GONE);
//                                tvHasil.setVisibility(View.VISIBLE);
//                                ivHasil.setVisibility(View.VISIBLE);
//                                tvHasil.setText(report.getHasilAkhir());
//                                if (report.getHasilAkhir().equals("Normal")){
//                                    ivHasil.setImageResource(R.drawable.ic_smile);
//                                }else if (report.getHasilAkhir().equals("Abnormal")){
//                                    ivHasil.setImageResource(R.drawable.ic_cry);
//                                } else if (report.getHasilAkhir().equals("Meragukan")){
//                                    ivHasil.setImageResource(R.drawable.ic_sad);
//                                } else {
//                                    ivHasil.setImageResource(R.drawable.ic_reject);
//                                }
//                            } else {
//                                tvGagal.setVisibility(View.VISIBLE);
//                                tvHasil.setVisibility(View.GONE);
//                                ivHasil.setVisibility(View.GONE);
//                            }
//
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<DataReports> call, Throwable t) {
//                        tvGagal.setVisibility(View.VISIBLE);
//                        tvHasil.setVisibility(View.GONE);
//                        ivHasil.setVisibility(View.GONE);
//                        DialogBuilder.showErrorDialog(context, t.getMessage());
//                        Log.i("DATAREPORT_GET", t.getMessage());
//                    }
//                });
//    }

    @OnClick(R.id.im_detail_dev) void goDialog(){
        AlertDialog.Builder mDialog = new AlertDialog.Builder(view.getContext())
                .setTitle("Pilih anak");
        LayoutInflater inflater = getLayoutInflater();
        View convertView = inflater.inflate(R.layout.fragment_list_anak_dialog, null);
        RecyclerView list = convertView.findViewById(R.id.rv_list_anak_dialog);
        DialogListAdapter dialogListAdapter = new DialogListAdapter(convertView.getContext(), R.layout.item_list_dialog, anakList);
        list.setLayoutManager(new LinearLayoutManager(convertView.getContext()));
        list.setAdapter(dialogListAdapter);
        dialogListAdapter.setOnItemClickListener(new DialogListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                id_anak = anakList.get(position).getId_anak();
                tvNama.setText(anakList.get(position).getNama_anak());
                tvUsia.setText(anakList.get(position).getUsia() + " bulan");
                if (anakList.get(position).getGender_anak().equals("L")){
                    ivAvatar.setImageResource(R.drawable.ic_baby_boy);
                }else {
                    ivAvatar.setImageResource(R.drawable.ic_baby_girl);
                }
            }
        });
        mDialog.setView(convertView);
        mDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                anak = anakPresenter.getByID(id_anak);
            }
        });
        mDialog.show();

    }

    @OnClick(R.id.btn_test) void goTest(){
        Intent intent = new Intent(getContext(), TestActivity.class);
        intent.putExtra("id", anak.getId_anak());
        startActivity (intent);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadReportAkhir(List<ReportAkhirs.ReportAkhir> reportAkhirList) {
        reportAdapter = new ReportAdapter(view.getContext(), reportAkhirList);
        rvReport.setHasFixedSize(true);
        rvReport.setLayoutManager(new LinearLayoutManager(getContext()));
        rvReport.setAdapter(reportAdapter);
    }

    @Override
    public void loadAnak(List<Anak> anakList) {

    }
}
