package com.example.crownapplication.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.crownapplication.R;
import com.example.crownapplication.ui.util.PreferenceManager;

public class SplashScreenActivity extends AppCompatActivity {

    private PreferenceManager mPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        // Checking for first time launch - before calling setContentView()

        // For automatic move to next activity
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreenActivity.this, SignInActivity.class));
                finish();
            }
        }, 3000);
    }
}
