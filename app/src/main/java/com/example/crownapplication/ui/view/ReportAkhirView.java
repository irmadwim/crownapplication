package com.example.crownapplication.ui.view;

import com.example.crownapplication.bl.db.model.ReportAkhir;
import com.example.crownapplication.bl.network.model.ReportAkhirs;

import java.util.List;

public interface ReportAkhirView {
    void showLoading();
    void hideLoading();
    void loadReportAkhir(List<ReportAkhirs.ReportAkhir> reportAkhirList);
}
