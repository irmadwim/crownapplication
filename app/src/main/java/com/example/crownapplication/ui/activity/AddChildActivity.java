package com.example.crownapplication.ui.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.User;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.api.SyncWorker;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.BaseRespons;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.presenter.AnakPresenter;
import com.example.crownapplication.ui.presenter.UserPresenter;
import com.example.crownapplication.ui.util.PrefUtil;
import com.example.crownapplication.ui.view.AnakView;
import com.example.crownapplication.ui.view.UserView;
import com.google.android.material.textfield.TextInputEditText;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddChildActivity extends AppCompatActivity implements UserView, AnakView {


    Calendar calendar;
    int year;
    int month;
    int date;
    private String selected_jk;
    private int id_anak, id_user;
    private AnakPresenter anakPresenter;
    private UserPresenter userPresenter;
    private User user;
    private UserOrtu userOrtu;
    private Anak anak;
    private Api mApi;
    private MaterialDialog mDialog;
    private Context ctx;

    @BindView(R.id.tedit_name_addchild)
    TextInputEditText etNama;
    @BindView(R.id.tedit_tgllahir_addchild)
    TextInputEditText etTgl;
    @BindView(R.id.sp_daftarJk_addAnak)
    Spinner spJk;
    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_child);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        initData();
        initViews();

    }

    private void initData(){
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        id_user = getIntent().getIntExtra("id_user", 0);
        userOrtu = PrefUtil.getUser(this, PrefUtil.USER_SESSION);
        userPresenter = new UserPresenter(getApplicationContext(), this);
        user = userPresenter.getByID(userOrtu.getIdUser());
        //user = userPresenter.getByID(id_user);
    }


    private void initViews(){
        toolbar.setTitle("Tambah Anak");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnItemSelected(R.id.sp_daftarJk_addAnak) void jkSelected(int position){
        selected_jk = (String) spJk.getAdapter().getItem(position);
    }

    @OnItemSelected(value = R.id.sp_daftarJk_addAnak, callback = OnItemSelected.Callback.NOTHING_SELECTED)
    void onNothingSelected() {

    }

    @OnClick(R.id.tedit_tgllahir_addchild) void showTimeDialog(){
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        date = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddChildActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        etTgl.setText(String.format("%02d/%02d/%02d", year, month+1, dayOfMonth));
                    }

                }, year, month, date);

        datePickerDialog.show();
    }

    @OnClick(R.id.btn_simpan_addchild) void onAdd(){
        MaterialDialog dialog = DialogBuilder.showLoadingDialog(AddChildActivity.this, "Add Data", "Please Wait", false);
        mApi.addAnak(etNama.getText().toString(),etTgl.getText().toString(),
                selected_jk, user.getId_user())
                .enqueue(new Callback<BaseRespons>() {
                    @Override
                    public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                        BaseRespons baseRespons = response.body();
                        String message = "";
                        if (baseRespons!=null){
                            if(!baseRespons.getError()){
                                message = "Data berhasil ditambahkan";
                                SyncWorker.getSyncWorker().syncAnak(getApplicationContext(), mApi.getAnak(), false);
                            }
                        }
                        if (response.code() == 400){
                            message = "Invalid parameter!";
                        }
                        if (response.code() == 502){
                            message = "Gagal menyimpan ke database.";
                        }
                        Toast.makeText(AddChildActivity.this, message, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        Intent intent = new Intent(AddChildActivity.this, MainActivity.class);
                        startActivity(intent);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        onSupportNavigateUp();
                    }

                    @Override
                    public void onFailure(Call<BaseRespons> call, Throwable t) {

                    }
                });
    }


    @Override
    public void loadAnak(List<Anak> anakList) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadUser(List<User> userList) {

    }
}
