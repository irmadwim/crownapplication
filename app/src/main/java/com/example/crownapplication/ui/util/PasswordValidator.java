package com.example.crownapplication.ui.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator {
    final String PASSWORD_PATTERN = "[a-zA-Z0-9\\!\\@\\#\\$]{6,90}";
    Pattern pattern;
    Matcher matcher;

    private String str;
    public boolean isValid(String password) {
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        if (password.length() < 6) {
            setString("Password harus terdiri minimal 6 karakter");
            return false;
        }
        if (password.length() > 90) {
            setString("Password tidak boleh lebih dari 90 karakter");
            return false;
        }
        if (!matcher.matches()) {
            setString("Password harus terdiri dari huruf dan angka");
            return false;
        }
        return true;
    }

    public void setString(String str){
        this.str = str;
    }

    public String getString(){
        return str;
    }
}

