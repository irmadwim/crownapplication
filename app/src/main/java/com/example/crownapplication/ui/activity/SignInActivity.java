package com.example.crownapplication.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.util.EmailValidator;
import com.example.crownapplication.ui.util.PasswordValidator;
import com.example.crownapplication.ui.util.PrefUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.jakewharton.threetenabp.AndroidThreeTen;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.crownapplication.ui.util.Util.isEempty;

public class SignInActivity extends AppCompatActivity {

    @BindView(R.id.input_email_signin)
    TextInputEditText etEmail;
    @BindView(R.id.text_register)
    TextView tvRegister;
    @BindView(R.id.input_password_signin)
    TextInputEditText etPassword;

    EmailValidator emailValidator;
    PasswordValidator passwordValidator;
    Context context;

    private String email;
    private String password;
    private Api mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isSessionLogin()){
            startActivity(new Intent(this, MainActivity.class));
            this.finish();
        }

        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);

    }

    boolean isEmail(EditText text){
        emailValidator = new EmailValidator();
        String email = text.getText().toString();
        return emailValidator.isValid(email);
    }

    boolean isPassword(EditText text){
        passwordValidator = new PasswordValidator();
        String pass = text.getText().toString();
        return passwordValidator.isValid(pass);
    }

    @OnClick(R.id.text_register) void toRegister(){
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity (intent);
    }

    @OnClick(R.id.btn_signin) void onLogin(){
        if(isEempty(etEmail)){
            etEmail.setError("Email harus diisi");
        }else if(isEempty(etPassword)){
            etPassword.setError("Password harus diisi");
        }else if(!isEmail(etEmail)){
            etEmail.setError("Email tidak valid");
        }else if(!isPassword(etPassword)){
            String str = passwordValidator.getString();
            Toast.makeText(getApplicationContext(),str, Toast.LENGTH_SHORT).show();
        }else {
            loginAct();
        }
    }
    void loginAct(){
        email = etEmail.getText().toString();
        password = etPassword.getText().toString();
        final MaterialDialog dialog = DialogBuilder.showLoadingDialog(SignInActivity.this, "Updating Data", "Please wait..", false);
        mApi.loginUser(email, password).enqueue(new Callback<UserOrtu>() {
            @Override
            public void onResponse(Call<UserOrtu> call, Response<UserOrtu> response) {
                UserOrtu user = response.body();
                Log.i("USER_LOGIN", response.message());
                if (user != null){
                    //Masih error disini
                    //if (!user.getError()){
                        PrefUtil.putUser(getApplicationContext(), PrefUtil.USER_SESSION, user);
                        Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                        startActivity(intent);
                        //this.finish();

                    //}
                    Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (response.code() == 403){
                    etPassword.requestFocus();
                    etPassword.setError(getString(R.string.error_password));
                }
                if (response.code() == 404){
                    etEmail.requestFocus();
                    etEmail.setError(getString(R.string.error_login));
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<UserOrtu> call, Throwable t) {
                //Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                Log.i("USER_LOGIN", t.getMessage());
                DialogBuilder.showErrorDialog(SignInActivity.this, "Gagal Login");
            }
        });
    }

    // this method to check is user logged in ?
    boolean isSessionLogin(){
        return PrefUtil.getUser(getApplicationContext(), PrefUtil.USER_SESSION) != null;
    }

}
