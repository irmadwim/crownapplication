package com.example.crownapplication.ui.view;

import com.example.crownapplication.bl.db.model.Lk;

import java.util.List;

public interface LkView {
    void showLoading();
    void hideLoading();
    void loadLk(List<Lk> lkList);
}
