package com.example.crownapplication.ui.presenter;

import android.content.Context;

import com.example.crownapplication.bl.db.dao.UserDao;
import com.example.crownapplication.bl.db.model.User;
import com.example.crownapplication.ui.view.UserView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserPresenter {
    Context context;
    private UserView userView;

    public UserPresenter(Context context, UserView userView) {
        this.context = context;
        this.userView = userView;
    }

    public void load(){
        userView.showLoading();
        List<User> list = new ArrayList<>();
        try {
            list = UserDao.getUserDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        userView.loadUser(list);
        userView.hideLoading();
    }

    public User getByID(int id){
        User user = null;
        try {
            user = UserDao.getUserDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }
}

