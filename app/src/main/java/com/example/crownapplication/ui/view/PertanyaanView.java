package com.example.crownapplication.ui.view;

import com.example.crownapplication.bl.db.model.PertanyaanDdst;
import com.example.crownapplication.bl.network.model.DataPertanyaans;
import com.example.crownapplication.bl.network.model.PertanyaanDdsts;

import java.util.List;

public interface PertanyaanView {
    void showLoading();
    void hideLoading();
    void loadPertanyaan(List<PertanyaanDdsts.PertanyaanDdst> pertanyaanList);
}
