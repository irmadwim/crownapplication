package com.example.crownapplication.ui.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.helper.Db;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.ReportAkhir;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.ReportAkhirs;
import com.example.crownapplication.ui.presenter.AnakPresenter;
import com.example.crownapplication.ui.presenter.ReportAkhirPresenter;
import com.example.crownapplication.ui.presenter.UserPresenter;
import com.example.crownapplication.ui.util.PrefUtil;
import com.example.crownapplication.ui.view.AnakView;
import com.example.crownapplication.ui.view.ReportAkhirView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HasilTestActivity extends AppCompatActivity implements ReportAkhirView, AnakView {

    private Api mApi;
    private UserPresenter userPresenter;
    private AnakPresenter anakPresenter;
    private int id_anak;
    private String hasil;
    private Dialog mDialog;
    private Anak anak;
    private ReportAkhir reportAkhir;
    private ReportAkhirPresenter reportAkhirPresenter;

    @BindView(R.id.tv_usia_Hasiltest)
    TextView tvUsia;
    @BindView(R.id.tv_namaanak_Hasiltest)
    TextView tvNama;
    @BindView(R.id.tv_Hasiltest)
    TextView tvHasil;
    @BindView(R.id.tv_penjelasan)
    TextView tvPenjelasan;
    @BindView(R.id.iv_Hasiltest)
    ImageView ivHasil;
    @BindView(R.id.im_anak_Hasiltest)
    ImageView ivAvatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_test);

        ButterKnife.bind(this);
        Db.getInstance().init(this);
        AndroidThreeTen.init(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        initData();
        initView();
    }

    private void initData() {
        id_anak = getIntent().getIntExtra("id", 0);
        hasil = getIntent().getStringExtra("hasil");
        anakPresenter = new AnakPresenter(getApplicationContext(), this);
        anak = anakPresenter.getByID(id_anak);

//        reportAkhirPresenter = new ReportAkhirPresenter(getApplicationContext(), this);
//        reportAkhir = reportAkhirPresenter.getByLastID(id_anak);
    }

    private void initView() {

        tvNama.setText(String.valueOf(anak.getNama_anak()));

        if (anak.getUsia() >= 12)
            tvUsia.setText(anak.getUsia()/12 + " tahun " + anak.getUsia()%12 + " bulan");
        else
            tvUsia.setText(anak.getUsia() + " bulan");

        // Set Avatar sesuai jenis kelamin dan umur
        if (anak.getGender_anak().equals("L")){
            ivAvatar.setImageResource(R.drawable.ic_baby_boy);
        }else {
            ivAvatar.setImageResource(R.drawable.ic_baby_girl);

        }

        tvHasil.setText(String.valueOf(hasil));

        if (hasil.equals("Normal")){
            ivHasil.setImageResource(R.drawable.ic_smile);
        }else if (hasil.equals("Abnormal")){
            ivHasil.setImageResource(R.drawable.ic_cry);
        } else if (hasil.equals("Meragukan")){
            ivHasil.setImageResource(R.drawable.ic_sad);
        } else {
            ivHasil.setImageResource(R.drawable.ic_reject);
        }

        if (hasil.equals("Normal")){
           tvHasil.setTextColor(Color.GREEN);
        }else if (hasil.equals("Abnormal")){
            tvHasil.setTextColor(Color.RED);
        } else if (hasil.equals("Meragukan")){
            tvHasil.setTextColor(Color.parseColor("#ff7c17"));
        } else {
            tvHasil.setTextColor(Color.BLUE);
        }

        if (hasil.equals("Normal")){
            tvPenjelasan.setText("Status perkembangan anak Anda normal tidak perlu khawatir, " +
                    "tetap lakukan stimulasi secara rutin");
        }else if (hasil.equals("Abnormal")){
            tvPenjelasan.setText("Status perkembangan anak Anda abnormal, segera periksakan ke dokter");
        } else if (hasil.equals("Meragukan")){
            tvPenjelasan.setText("Status perkembangan anak Anda meragukan, " +
                    "lakukan tes ulang dalam kurun waktu 3 Bulan");
        } else {
            tvPenjelasan.setText("Status perkembangan anak Anda tidak dapat dites, silahkan lakukan tes ulang " +
                    "dalam waktu satu bulan kedepan");
        }

    }

    @OnClick(R.id.btn_kembali) void goNext(){
        Intent intent = new Intent(HasilTestActivity.this, MainActivity.class);
        startActivity (intent);
    }



    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadAnak(List<Anak> anakList) {

    }

    @Override
    public void loadReportAkhir(List<ReportAkhirs.ReportAkhir> reportAkhirList) {

    }
}
