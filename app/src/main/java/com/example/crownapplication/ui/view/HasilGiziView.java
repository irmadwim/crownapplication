package com.example.crownapplication.ui.view;

import com.example.crownapplication.bl.db.model.DataGizi;
import com.example.crownapplication.bl.db.model.HasilGizi;

import java.util.List;

public interface HasilGiziView {
    void showLoading();
    void hideLoading();
    void loadHasilGizi(List<HasilGizi> hasilGiziList);
}
