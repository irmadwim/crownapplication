package com.example.crownapplication.ui.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.User;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.api.SyncWorker;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.BaseRespons;
import com.example.crownapplication.bl.network.model.UserOrtu;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.presenter.AnakPresenter;
import com.example.crownapplication.ui.presenter.UserPresenter;
import com.example.crownapplication.ui.util.PrefUtil;
import com.example.crownapplication.ui.view.AnakView;
import com.google.android.material.textfield.TextInputEditText;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMeasurementActivity extends AppCompatActivity implements AnakView {

    Calendar calendar;
    int year;
    int month;
    int date;
    private int id_anak, id_user;
    private AnakPresenter anakPresenter;
    private UserPresenter userPresenter;
    private User user;
    private UserOrtu userOrtu;
    private Anak anak;
    private Api mApi;
    private MaterialDialog mDialog;
    private Context ctx;

    @BindView(R.id.tedit_tgl_addmeasure)
    TextInputEditText etTgl;
    @BindView(R.id.tedit_berat_addmeasure)
    TextInputEditText etBb;
    @BindView(R.id.tedit_tinggi_addmeasure)
    TextInputEditText etTb;
    @BindView(R.id.tedit_lkepala_addmeasure)
    TextInputEditText etLk;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_measurement);
        ButterKnife.bind(this);
        AndroidThreeTen.init(this);
        initData();
        initViews();
    }

    private void initData(){
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        id_anak = getIntent().getIntExtra("id", 0);
        anakPresenter = new AnakPresenter(getApplicationContext(), this);
        anak = anakPresenter.getByID(id_anak);
        //user = userPresenter.getByID(id_user);
    }

    private void initViews(){
        toolbar.setTitle("Tambah Data Anak");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick(R.id.tedit_tgl_addmeasure) void showTimeDialog(){
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        date = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddMeasurementActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        etTgl.setText(String.format("%02d/%02d/%02d", year, month+1, dayOfMonth));
                    }

                }, year, month, date);

        datePickerDialog.show();
    }

    @OnClick(R.id.btn_simpan_addmeasure) void onAdd(){
        MaterialDialog dialog = DialogBuilder.showLoadingDialog(AddMeasurementActivity.this, "Add Data", "Please Wait", false);
        String strTb = etTb.getText().toString();
        String strBb = etBb.getText().toString();
        float tb = Float.parseFloat(strTb);
        float bb = Float.parseFloat(strBb);
        float imt = bb / (( tb / 100) * (tb / 100));
        String strImt=String.valueOf(imt);
        mApi.addDataGizi(etTgl.getText().toString(),etTb.getText().toString(),
                etBb.getText().toString(), etLk.getText().toString(),
                strImt, anak.getId_anak())
                .enqueue(new Callback<BaseRespons>() {
                    @Override
                    public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                        BaseRespons baseRespons = response.body();
                        String message = "";
                        if (baseRespons!=null){
                            if(!baseRespons.getError()){
                                message = "Data berhasil ditambahkan";
                                SyncWorker.getSyncWorker().syncDataGizi(getApplicationContext(), mApi.getDataGizi(), false);
                            }
                        }
                        if (response.code() == 400){
                            message = "Invalid parameter!";
                        }
                        if (response.code() == 502){
                            message = "Gagal menyimpan ke database.";
                        }
                        Toast.makeText(AddMeasurementActivity.this, message, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
//                        Intent intent = new Intent(AddMeasurementActivity.this, DetailChildActivity.class);
//                        startActivity(intent);
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                        onSupportNavigateUp();
                    }

                    @Override
                    public void onFailure(Call<BaseRespons> call, Throwable t) {

                    }
                });
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadAnak(List<Anak> anakList) {

    }
}
