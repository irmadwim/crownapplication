package com.example.crownapplication.ui.util;

import android.util.Patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {
    public boolean isValid(String emailStr){
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        Matcher matcher = pattern.matcher(emailStr);
        return matcher.matches();
    }
}
