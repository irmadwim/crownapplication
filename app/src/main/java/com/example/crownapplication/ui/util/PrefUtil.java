package com.example.crownapplication.ui.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.crownapplication.bl.network.model.UserOrtu;
import com.google.gson.Gson;
import com.example.crownapplication.bl.network.config.Config;

public class PrefUtil {
    public static final String USER_SESSION = "user_session";
    public static final String USER_STORAGE = "user_storage";

    public static SharedPreferences getSharedPreferences(Context ctx){
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void putUser(Context ctx, String key, UserOrtu user){
        Gson gson = new Gson();
        String json = gson.toJson(user);
        putString(ctx, key, json);
    }

    public static UserOrtu getUser(Context ctx, String key){
        Gson gson = new Gson();
        String json = getString(ctx, key);
        UserOrtu user = gson.fromJson(json, UserOrtu.class);
        return user;
    }

    public static void putString(Context ctx, String key, String value){
        getSharedPreferences(ctx).edit().putString(key, value).apply();
    }

    public static void setIDAspek(Context ctx, int id){
        getSharedPreferences(ctx).edit().putInt("id_aspek",id).apply();
    }

    public static int getIDAspek(Context ctx){
        return getSharedPreferences(ctx).getInt("id_aspek", 1);

    }

    public static String getString(Context ctx, String key){
        return getSharedPreferences(ctx).getString(key, null);
    }

    public static void clear(Context ctx) {
        getSharedPreferences(ctx).edit().clear().apply();
    }
}
