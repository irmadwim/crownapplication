package com.example.crownapplication.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.helper.Db;
import com.example.crownapplication.event.RekomendasiGiziTabEvent;
import com.example.crownapplication.ui.adapter.TabNutritionAdapter;
import com.example.crownapplication.ui.presenter.RekomendasiGiziPresenter;
import com.google.android.material.tabs.TabLayout;
import com.jakewharton.threetenabp.AndroidThreeTen;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;


public class NutritionFragment extends Fragment {

    View myFragment;
    ViewPager viewPager;
    TabLayout tabLayout;
    private RekomendasiGiziPresenter rekomgiziPresenter;

    public NutritionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragment = inflater.inflate(R.layout.fragment_nutrition, container, false);
        ButterKnife.bind(this, myFragment);
        Db.getInstance().init(getActivity());
        AndroidThreeTen.init(getActivity());
        viewPager = myFragment.findViewById(R.id.listgizi_vp);
        tabLayout = myFragment.findViewById(R.id.listgizi_tablayout);

        return myFragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        init();
        initListener();
    }

    private void init(){
        TabNutritionAdapter adapter = new TabNutritionAdapter(getChildFragmentManager());

        adapter.addFragment(new RekomGiziFragment(), "0-5");
        adapter.addFragment(new RekomGiziFragment(), "6-8");
        adapter.addFragment(new RekomGiziFragment(), "9-12");
        adapter.addFragment(new RekomGiziFragment(), "13-23");
        adapter.addFragment(new RekomGiziFragment(), "24-36");
        adapter.addFragment(new RekomGiziFragment(), "37-60");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initListener(){
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                EventBus.getDefault().postSticky(new RekomendasiGiziTabEvent(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                EventBus.getDefault().postSticky(new RekomendasiGiziTabEvent(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

}
