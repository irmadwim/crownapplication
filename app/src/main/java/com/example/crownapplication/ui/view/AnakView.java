package com.example.crownapplication.ui.view;

import com.example.crownapplication.bl.db.model.Anak;

import java.util.List;

public interface AnakView {
    void showLoading();
    void hideLoading();
    void loadAnak(List<Anak> anakList);
}

