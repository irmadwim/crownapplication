package com.example.crownapplication.ui.presenter;

import android.content.Context;

import com.example.crownapplication.bl.db.dao.RekomendasiGiziDao;
import com.example.crownapplication.bl.db.model.RekomendasiGizi;
import com.example.crownapplication.ui.view.RekomendasiGiziView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RekomendasiGiziPresenter {
    private RekomendasiGiziView rekomGiziView;

    public RekomendasiGiziPresenter(Context context, RekomendasiGiziView rekomGiziView) {
        this.rekomGiziView = rekomGiziView;
        AndroidThreeTen.init(context);
    }

    public void load() {
        rekomGiziView.showLoading();
        List<RekomendasiGizi> list = new ArrayList<>();
        try {
            list = RekomendasiGiziDao.getRekomendasigiziDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        rekomGiziView.loadRekomGizi(list);
        rekomGiziView.hideLoading();
    }

    public List<RekomendasiGizi> loadByAge(int age, int interval) {
        rekomGiziView.showLoading();
        List<RekomendasiGizi> list = new ArrayList<>();
        List<RekomendasiGizi> selected = new ArrayList<>();
        try {
            list = RekomendasiGiziDao.getRekomendasigiziDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (RekomendasiGizi rekomendasiGizi : list) {
            if (age <= rekomendasiGizi.getUsia() && rekomendasiGizi.getUsia() <= interval) {
                selected.add(rekomendasiGizi);
            }
        }
        //muridView.loadTppa(selected);
        rekomGiziView.hideLoading();
        return selected;
    }

    public RekomendasiGizi getByID(int id){
        RekomendasiGizi rekomendasiGizi = null;
        try {
            rekomendasiGizi = RekomendasiGiziDao.getRekomendasigiziDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rekomendasiGizi;
    }
}