package com.example.crownapplication.ui.presenter;

import android.content.Context;

import com.example.crownapplication.bl.db.dao.BbDao;
import com.example.crownapplication.bl.db.dao.TbDao;
import com.example.crownapplication.bl.db.model.Bb;
import com.example.crownapplication.bl.db.model.Tb;
import com.example.crownapplication.ui.view.BbView;
import com.example.crownapplication.ui.view.TbView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TbPresenter {
    private TbView tbView;

    public TbPresenter(Context context, TbView tbView) {
        this.tbView = tbView;
        AndroidThreeTen.init(context);
    }

    public void load(){
        tbView.showLoading();
        List<Tb> list = new ArrayList<>();
        try {
            list = TbDao.getTbDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        tbView.loadTb(list);
        tbView.hideLoading();
    }

    public List<Tb> getByJk(String jk){
        List<Tb> list = new ArrayList<>();
        try {
            list = TbDao.getTbDao().getByJk(jk);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Tb> getByUsiaJk(int usia, String jk){
        List<Tb> list = new ArrayList<>();
        try {
            list = TbDao.getTbDao().getByUsiaJk(usia, jk);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }


    public Tb getByID(int id){
        Tb tb = null;
        try {
            tb = TbDao.getTbDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tb;
    }

}
