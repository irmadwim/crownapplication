package com.example.crownapplication.ui.view;

import com.example.crownapplication.bl.db.model.Bb;

import java.util.List;

public interface BbView {
    void showLoading();
    void hideLoading();
    void loadBb(List<Bb> bbList);
}
