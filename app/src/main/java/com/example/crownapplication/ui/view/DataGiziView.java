package com.example.crownapplication.ui.view;

import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.db.model.DataGizi;

import java.util.List;

public interface DataGiziView {
    void showLoading();
    void hideLoading();
    void loadDataGizi(List<DataGizi> dataGiziList);
    void loadGizi(DataGizi dataGizi);
}
