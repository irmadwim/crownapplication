package com.example.crownapplication.ui.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.model.Anak;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.ui.activity.DetailChildActivity;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AnakAdapter extends RecyclerView.Adapter<AnakAdapter.ViewHolder> {

    Context context;
    List<Anak> list = new ArrayList<>();

    public AnakAdapter(Context context) {
        this.context = context;
        AndroidThreeTen.init(context);
    }

    public AnakAdapter(Context context, List<Anak> list) {
        this.context = context;
        this.list = list;
        AndroidThreeTen.init(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_card_home, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Anak anak = list.get(position);
        holder.id = anak.getId_anak();
        holder.tvNama.setText(anak.getNama_anak());
        holder.tvUsia.setText(anak.getUsia() + " bulan");

        if (anak.getGender_anak().equals("L")){
            holder.ivAvatar.setImageResource(R.drawable.ic_baby_boy);
        }else {
            holder.ivAvatar.setImageResource(R.drawable.ic_baby_girl);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tv_nama_anak)
        TextView tvNama;
        @BindView(R.id.avatar_anak)
        ImageView ivAvatar;
        @BindView(R.id.tv_usia_anak)
        TextView tvUsia;
        int id;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.cv_anak) void onClick(){
            Intent intent = new Intent(context, DetailChildActivity.class);
            intent.putExtra("id", id);
            context.startActivity(intent);
        }
    }

    public void generate(List<Anak> list) {
        clear();
        this.list = list;
        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }

}
