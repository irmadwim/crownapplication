package com.example.crownapplication.ui.presenter;

import android.content.Context;

import com.example.crownapplication.bl.db.dao.DataGiziDao;
import com.example.crownapplication.bl.db.model.DataGizi;
import com.example.crownapplication.ui.view.DataGiziView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DataGiziPresenter {
    private DataGiziView dataGiziView;

    public DataGiziPresenter(Context context, DataGiziView dataGiziView) {
        this.dataGiziView = dataGiziView;
        AndroidThreeTen.init(context);
    }

    public void load(){
        dataGiziView.showLoading();
        List<DataGizi> list = new ArrayList<>();
        try {
            list = DataGiziDao.getDatagiziDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        dataGiziView.loadDataGizi(list);
        dataGiziView.hideLoading();
    }


    public List<DataGizi> getByAnak(int anak){
        List<DataGizi> list = new ArrayList<>();
        try {
            list = DataGiziDao.getDatagiziDao().getByAnak(anak);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public DataGizi getByID(int id){
        DataGizi dataGizi = null;
        try {
            dataGizi = DataGiziDao.getDatagiziDao().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dataGizi;
    }

    public DataGizi getLastIdByAnak(int anak){
        DataGizi dataGizi = null;
        try {
            dataGizi = DataGiziDao.getDatagiziDao().getLastIdByAnak(anak);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dataGizi;
    }
}


