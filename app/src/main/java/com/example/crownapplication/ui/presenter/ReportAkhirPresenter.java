package com.example.crownapplication.ui.presenter;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.example.crownapplication.R;
import com.example.crownapplication.bl.db.dao.PertanyaanDdstDao;
import com.example.crownapplication.bl.db.dao.ReportAkhirDao;
import com.example.crownapplication.bl.db.model.PertanyaanDdst;
import com.example.crownapplication.bl.db.model.ReportAkhir;
import com.example.crownapplication.bl.network.api.Api;
import com.example.crownapplication.bl.network.config.RetrofitBuilder;
import com.example.crownapplication.bl.network.model.DataReports;
import com.example.crownapplication.bl.network.model.PertanyaanDdsts;
import com.example.crownapplication.bl.network.model.ReportAkhirs;
import com.example.crownapplication.ui.dialog.DialogBuilder;
import com.example.crownapplication.ui.view.AnakView;
import com.example.crownapplication.ui.view.ReportAkhirView;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportAkhirPresenter {
    Context context;
    private ReportAkhirView mReportView;
    private Api mApi;

    public ReportAkhirPresenter(Context context, ReportAkhirView mReportView) {
        this.mReportView = mReportView;
        AndroidThreeTen.init(context);
        this.context = context;
        mApi = RetrofitBuilder.builder(context).create(Api.class);
    }

    public ReportAkhirPresenter(ReportAkhirView mReportView) {
        this.mReportView = mReportView;
    }

//    public void load(){
//        mReportView.showLoading();
//        List<ReportAkhir> list = new ArrayList<>();
//        try {
//            list = ReportAkhirDao.getReportakhirDao().read();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        mReportView.loadReportAkhir(list);
//        mReportView.hideLoading();
//    }


    public List<ReportAkhir> getByDate(String tgl, int anak_id){
        List<ReportAkhir> list = new ArrayList<>();
        mReportView.showLoading();
        try {
            list = ReportAkhirDao.getReportakhirDao().getByTgl(tgl, anak_id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        mReportView.hideLoading();
        return list;
    }

    public ReportAkhir getByLastID(int anak){
        ReportAkhir reportAkhir = null;
        try {
            reportAkhir = ReportAkhirDao.getReportakhirDao().getByLastID(anak);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reportAkhir;
    }

    public void getReport(int idAnak, String tanggal) {
        mApi.getReport(idAnak, tanggal).enqueue(new Callback<ReportAkhirs>() {
            @Override
            public void onResponse(Call<ReportAkhirs> call, Response<ReportAkhirs> response) {
                Log.i("REPORT_GET", response.message());
                ReportAkhirs list = response.body();
                mReportView.loadReportAkhir(list.getReportakhir());

            }

            @Override
            public void onFailure(Call<ReportAkhirs> call, Throwable t) {
                DialogBuilder.showErrorDialog(context, t.getMessage());
                Log.i("REPORT_GET", t.getMessage());
            }
        });

    }


}
